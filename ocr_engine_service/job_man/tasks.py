from ocr_engine_service.celery import app
from src.ocr_extraction.views import convert_ocr_to_json
from billiard.exceptions import WorkerLostError
@app.task
def s3_file_processor(sFileUrl):
    print("dummy s3_file_processor")

@app.task
def a_sample_nlp_task(dParams={}):
    print(dParams)
@app.task()
def ocr_img_to_json(dParams):
     try:
          convert_ocr_to_json(dParams)
     except WorkerLostError as e :
          print("Worker lost   *******  ",str(e))

@app.task()
def ocr_img_to_json_system_mapping(dParams):
     try:
          convert_ocr_to_json(dParams)
     except WorkerLostError as e :
          print("Worker lost   *******  ",str(e))
     

    

@app.task
def get_ocr_response(dParams):
     print("dummy get_ocr_response")
@app.task
def split_and_process_data(dMappedJson, dFileInfo, lDbJsonSet=None, bNeedNlpConv=True, bExecuteAsTask=True):
     print("dummy s3_file_processor")
