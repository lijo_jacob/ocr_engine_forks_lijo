"""rcm_ai_service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('brightree_html/', include('src.brightree_html.urls')),
    path('pdf/', include('src.pdf.urls')),
    path('ocr/', include('src.ocr.urls')),
    path('model/', include('src.model.urls')),
    path('conversion/', include('src.conversion.urls')),
    path('doc_pred/', include('src.document_prediction.urls')),
]
