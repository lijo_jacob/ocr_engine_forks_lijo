#!/bin/sh

# install requirements
# pip install -r requirements.txt

# install nltk dependencies
python3 -m nltk.downloader stopwords
python3 -m nltk.downloader punkt
python3 -m nltk.downloader averaged_perceptron_tagger
python3 -m nltk.downloader maxent_ne_chunker
python3 -m nltk.downloader words
python3 -m spacy download en

# train and save model
# python generate_model.python

# imagemagik
apt-get install libmagickwand-dev -y


# install phantomJS
apt-get update
apt-get install -y build-essential chrpath libssl-dev libxft-dev
apt-get install -y libfreetype6 libfreetype6-dev
apt-get install -y libfontconfig1 libfontconfig1-dev
apt-get install -y wget
apt-get install -y libhunspell-dev

# cd ~
# wget http://nlp.stanford.edu/software/stanford-corenlp-full-2018-01-31.zip
# unzip stanford-corenlp-full-2018-01-31.zip
# cd stanford-corenlp-full-2018-01-31
# chmod +x /root/OCR_Engine/ocr_engine_service/src/ner/start_corenlp_server.sh
# bash /root/OCR_Engine/ocr_engine_service/src/ner/start_corenlp_server.sh &
