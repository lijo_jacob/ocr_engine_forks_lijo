#!/usr/bin/env python3

import os

import time
import datetime
import cv2
import numpy as np
import uuid
import json

import functools
import logging
import collections

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


# @functools.lru_cache(maxsize=1)
# def get_host_info():
#     ret = {}
#     with open('/proc/cpuinfo') as f:
#         ret['cpuinfo'] = f.read()
#
#     with open('/proc/meminfo') as f:
#         ret['meminfo'] = f.read()
#
#     with open('/proc/loadavg') as f:
#         ret['loadavg'] = f.read()
#
#     return ret


@functools.lru_cache(maxsize=100)
def get_predictor(checkpoint_path):
    print('Checkpoint path', checkpoint_path)
    logger.info('loading model')
    import tensorflow as tf
    import EAST.model
    from EAST.icdar import restore_rectangle
    import EAST.lanms
    from EAST.eval import resize_image, sort_poly, detect

    input_images = tf.placeholder(
        tf.float32, shape=[None, None, None, 3], name='input_images')
    global_step = tf.get_variable(
        'global_step', [], initializer=tf.constant_initializer(0), trainable=False)

    f_score, f_geometry = EAST.model.model(input_images, is_training=False)

    variable_averages = tf.train.ExponentialMovingAverage(0.997, global_step)
    saver = tf.train.Saver(variable_averages.variables_to_restore())

    sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))

    ckpt_state = tf.train.get_checkpoint_state(checkpoint_path)
    model_path = os.path.join(checkpoint_path, os.path.basename(
        ckpt_state.model_checkpoint_path))
    print('****************', model_path)
    logger.info('Restore from {}'.format(model_path))
    saver.restore(sess, model_path)
    print('Model restored***************')

    def predictor(img):
        """
        :return: {
            'text_lines': [
                {
                    'score': ,
                    'x0': ,
                    'y0': ,
                    'x1': ,
                    ...
                    'y3': ,
                }
            ],
            'rtparams': {  # runtime parameters
                'image_size': ,
                'working_size': ,
            },
            'timing': {
                'net': ,
                'restore': ,
                'nms': ,
                'cpuinfo': ,
                'meminfo': ,
                'uptime': ,
            }
        }
        """
        start_time = time.time()
        rtparams = collections.OrderedDict()
        rtparams['start_time'] = datetime.datetime.now().isoformat()
        rtparams['image_size'] = '{}x{}'.format(img.shape[1], img.shape[0])
        timer = collections.OrderedDict([
            ('net', 0),
            ('restore', 0),
            ('nms', 0)
        ])

        im_resized, (ratio_h, ratio_w) = resize_image(img)
        rtparams['working_size'] = '{}x{}'.format(
            im_resized.shape[1], im_resized.shape[0])
        start = time.time()
        score, geometry = sess.run(
            [f_score, f_geometry],
            feed_dict={input_images: [im_resized[:, :, ::-1]]})
        timer['net'] = time.time() - start

        boxes, timer = detect(score_map=score, geo_map=geometry, timer=timer)
        logger.info('net {:.0f}ms, restore {:.0f}ms, nms {:.0f}ms'.format(
            timer['net'] * 1000, timer['restore'] * 1000, timer['nms'] * 1000))

        if boxes is not None:
            scores = boxes[:, 8].reshape(-1)
            boxes = boxes[:, :8].reshape((-1, 4, 2))
            boxes[:, :, 0] /= ratio_w
            boxes[:, :, 1] /= ratio_h

        duration = time.time() - start_time
        timer['overall'] = duration
        logger.info('[timing] {}'.format(duration))

        text_lines = []
        if boxes is not None:
            text_lines = []
            for box, score in zip(boxes, scores):
                box = sort_poly(box.astype(np.int32))
                if np.linalg.norm(box[0] - box[1]) < 5 or np.linalg.norm(box[3] - box[0]) < 5:
                    continue
                tl = collections.OrderedDict(zip(
                    ['x0', 'y0', 'x1', 'y1', 'x2', 'y2', 'x3', 'y3'],
                    map(float, box.flatten())))
                tl['score'] = float(score)
                text_lines.append(tl)
        ret = {
            'text_lines': text_lines,
            # 'rtparams': rtparams,
            # 'timing': timer,
        }
        # ret.update(get_host_info())
        return ret

    return predictor


# the webserver
# from flask import Flask, request, render_template
import argparse


class Config:
    SAVE_DIR = 'static/results'


config = Config()


# app = Flask(__name__)


def index():
    return render_template('index.html', session_id='dummy_session_id')


def draw_illu(illu, rst):
    for t in rst['text_lines']:
        d = np.array([t['x0'], t['y0'], t['x1'], t['y1'], t['x2'],
                      t['y2'], t['x3'], t['y3']], dtype='int32')
        d = d.reshape(-1, 2)
        cv2.polylines(illu, [d], isClosed=True, color=(255, 255, 0))
    return illu


def save_result(img, rst):
    session_id = str(uuid.uuid1())
    dirpath = os.path.join(config.SAVE_DIR, session_id)
    os.makedirs(dirpath)

    # save input image
    output_path = os.path.join(dirpath, 'input.png')
    cv2.imwrite(output_path, img)

    # save illustration
    output_path = os.path.join(dirpath, 'output.png')
    cv2.imwrite(output_path, draw_illu(img.copy(), rst))

    # save json data
    output_path = os.path.join(dirpath, 'result.json')
    with open(output_path, 'w') as f:
        json.dump(rst, f)

    rst['session_id'] = session_id
    return rst


checkpoint_path = os.getcwd() + '/EAST/east_icdar2015_resnet_v1_50_rbox/'


def index_post():
    global predictor
    import io
    bio = io.BytesIO()
    request.files['image'].save(bio)
    img = cv2.imdecode(np.frombuffer(bio.getvalue(), dtype='uint8'), 1)
    rst = get_predictor(checkpoint_path)(img)

    save_result(img, rst)
    return render_template('index.html', session_id=rst['session_id'])


import sys


def main(img_path):
    print(img_path)
    final_path = img_path.replace('.jpg', '_output.jpg')
    global checkpoint_path
    img = cv2.imread(img_path)
    # print('&&&&&&&&&&&&&', img.shape)
    img = cv2.pyrUp(img)
    # print('&&&&&&&&&&&&&', img.shape)

    # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 1))
    # dilated = cv2.dilate(gray, kernel, iterations=3)
    # eroded=cv2.dilate(dilated, kernel, iterations=3)
    # cv2.imshow('Img',eroded)
    # cv2.waitKey(0)
    rst = get_predictor(checkpoint_path)(img)
    coor_list = []
    for line in rst['text_lines']:
        # print(line)

        coor_list.append([line['x0'], line['y0'], line['x2'], line['y2']])
    # for coor in coor_list:
    #     # print(coor)
    #     cv2.rectangle(img,(int(coor[0]),int(coor[1])),(int(coor[2]),int(coor[3])),(0,255,0),3)
    # cv2.imwrite(final_path,img)
    return final_path, coor_list

    # print(rst['text_lines'])


import time
if __name__ == '__main__':
    now = time.time()
    img_path = '/home/user2/PycharmProjects/RCMBrain/data/data_as_images/060118-1/0.jpg'
    main(img_path)
    print(time.time() - now)
