#!/bin/bash
# activate settings base on ENV
#!/bin/bash
# activate settings base on ENV

if [ "${NODE_ENV}" = "prod" ] ; then
    echo "Starting the OCR Engine PROD instance"
    if [ "${RUN_MODE}" = "uwsgi" ]
     then
        
        python ./manage.py runscript page_rotation_init_train --settings=settings &
        NEW_RELIC_CONFIG_FILE=newrelic_prod.ini newrelic-admin run-program uwsgi --check-static rest_framework --enable-threads --processes 4 --threads 2 --ignore-sigpipe --ignore-write-errors --disable-write-exception --wsgi-file wsgi.py --http-timeout 4000  --env DJANGO_SETTINGS_MODULE=settings --master --http 0.0.0.0:8000
    else
        python ./manage.py runscript page_rotation_init_train --settings=settings &
        celery worker -A ocr_engine_service -n ocr_worker[%i]@%h$(cat /sys/class/net/$(ip route show default | awk '/default/ {print $5}')/address)  -l INFO -c2 -Qocr_queue &
        celery worker -A ocr_engine_service -n sys_mapping_ocr[%i]@%h$(cat /sys/class/net/$(ip route show default | awk '/default/ {print $5}')/address) -l INFO   -c1 -Qsystem_mapping_ocr_queue & 
        python3 ./manage.py runserver 0.0.0.0:8000 &
    fi

elif [ "${NODE_ENV}" = "staging" ] ; then
    echo "Starting the OCR Engine STAGING instance"
    if [ "${RUN_MODE}" = "uwsgi" ]
    then
        python ./manage.py runscript page_rotation_init_train --settings=settings &
        NEW_RELIC_CONFIG_FILE=newrelic_stag.ini newrelic-admin run-program uwsgi --check-static rest_framework --enable-threads  --processes 4 --threads 2 --ignore-sigpipe --ignore-write-errors --disable-write-exception --wsgi-file wsgi.py --http-timeout 4000  --env DJANGO_SETTINGS_MODULE=settings --master --http 0.0.0.0:8000
    else
        python ./manage.py runscript page_rotation_init_train --settings=settings &
        celery worker -A ocr_engine_service -n ocr_worker[%i]@%h$(cat /sys/class/net/$(ip route show default | awk '/default/ {print $5}')/address) -l INFO   -c2 -Qocr_queue  &
        celery worker -A ocr_engine_service -n sys_mapping_ocr[%i]@%h$(cat /sys/class/net/$(ip route show default | awk '/default/ {print $5}')/address) -l INFO   -c1 -Qsystem_mapping_ocr_queue & 
        python3 ./manage.py runserver 0.0.0.0:8000 &
    fi   
elif [ "${NODE_ENV}" = "qa" ] ; then
    echo "Starting the OCR Engine QA instance"
    if [ "${RUN_MODE}" = "uwsgi" ]
     then
        python ./manage.py runscript page_rotation_init_train --settings=settings &
        NEW_RELIC_CONFIG_FILE=newrelic_qa.ini newrelic-admin run-program uwsgi --check-static rest_framework --enable-threads --processes 4 --threads 2 --ignore-sigpipe --ignore-write-errors --disable-write-exception --wsgi-file wsgi.py --http-timeout 4000  --env DJANGO_SETTINGS_MODULE=settings --master --http 0.0.0.0:8000
    else
        python ./manage.py runscript page_rotation_init_train --settings=settings &
        celery worker -A ocr_engine_service -n ocr_worker[%i]@%h$(cat /sys/class/net/$(ip route show default | awk '/default/ {print $5}')/address) -l DEBUG   -c2 -Qocr_queue  &
        celery worker -A ocr_engine_service -n sys_mapping_ocr[%i]@%h$(cat /sys/class/net/$(ip route show default | awk '/default/ {print $5}')/address) -l INFO   -c1 -Qsystem_mapping_ocr_queue & 
        python3 ./manage.py runserver 0.0.0.0:8000 &
    fi
elif [ "${NODE_ENV}" = "dev" ] ; then
    echo "Starting the OCR Engine DEV instance"
    if [ "${RUN_MODE}" = "uwsgi" ]
     then
        python ./manage.py runscript page_rotation_init_train --settings=settings &
        NEW_RELIC_CONFIG_FILE=newrelic_dev.ini newrelic-admin run-program uwsgi --check-static rest_framework --enable-threads --processes 4 --threads 2 --ignore-sigpipe --ignore-write-errors --disable-write-exception --wsgi-file wsgi.py --http-timeout 4000  --env DJANGO_SETTINGS_MODULE=settings --master --http 0.0.0.0:8000
    else
        python ./manage.py runscript page_rotation_init_train --settings=settings &
        celery worker -A ocr_engine_service -n ocr_worker[%i]@%h$(cat /sys/class/net/$(ip route show default | awk '/default/ {print $5}')/address) -l INFO  -c2 -Qocr_queue  &
        celery worker -A ocr_engine_service -n sys_mapping_ocr[%i]@%h$(cat /sys/class/net/$(ip route show default | awk '/default/ {print $5}')/address) -l INFO  -c1 -Qsystem_mapping_ocr_queue & 
        python3 ./manage.py runserver 0.0.0.0:8000 &
    fi
elif [ "${NODE_ENV}" = "dev2" ] ; then
    echo "Starting the OCR Engine DEV2 instance"
    if [ "${RUN_MODE}" = "uwsgi" ]
    then
        
        python ./manage.py runscript page_rotation_init_train --settings=settings &
        NEW_RELIC_CONFIG_FILE=newrelic_dev2.ini newrelic-admin run-program uwsgi --check-static rest_framework --enable-threads --processes 4 --threads 2 --ignore-sigpipe --ignore-write-errors --disable-write-exception --wsgi-file wsgi.py  --http-timeout 4000 --env DJANGO_SETTINGS_MODULE=settings --master --http 0.0.0.0:8000
    else
        python3 ./manage.py runscript page_rotation_init_train --settings=settings &
        celery worker -A ocr_engine_service -n ocr_worker[%i]@%h$(cat /sys/class/net/$(ip route show default | awk '/default/ {print $5}')/address) -l INFO  -c2 -Qocr_queue & 
        celery worker -A ocr_engine_service -n sys_mapping_ocr[%i]@%h$(cat /sys/class/net/$(ip route show default | awk '/default/ {print $5}')/address) -l INFO  -c1 -Qsystem_mapping_ocr_queue & 
        python3 ./manage.py runserver 0.0.0.0:8000 &
    fi
 else   
    echo "No Environment defined for this build."
    exit 1
fi
