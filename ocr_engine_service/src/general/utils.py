import os


def check_dir_exist(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)
