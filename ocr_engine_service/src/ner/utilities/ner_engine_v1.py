#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue July 25 16:11:41 2018

@author: jain mohit
"""

# imports

import os
from time import time
import json
import spacy
import pandas as pd
import numpy as np
from sklearn.externals import joblib
import nltk
from nltk.tree import Tree
from nltk.parse import CoreNLPParser
from concurrent.futures import ThreadPoolExecutor, as_completed
from ._utils import (
    check_dir_exist
)
from .variables import (
    MODEL_PATH
)

nlp = spacy.load('en')
tagger = CoreNLPParser(url='http://127.0.0.1:9004', tagtype='ner')

# pool = ThreadPoolExecutor(3)
stan_pool = ThreadPoolExecutor(15)


def run_spacy_ner(text):
    """
        Spacy NER        
    """
    doc = nlp(text)
    for ent in doc.ents:
        return ent.label_

def _processTextNltk(itemArray):
    """
        NLTK NER Helper function
    """
    entityList = []
    entityList.append(itemArray)
    for item in entityList:
        tokenized = nltk.word_tokenize(item)
        tagged = nltk.pos_tag(tokenized)
        namedEnt = nltk.ne_chunk(tagged)
    return namedEnt

def run_nltk_ner(text):
    """
       NLTK NER  
    """
    tagged_token = _processTextNltk(text)
    for i in tagged_token:
        if len(i) == 1:
            return i.label()
        else:
            return "None"



def run_stanford_corenlp_ner(text):
    """
        Stanford CoreNLP NER
    """
    textList = []; text = text.strip()
    if text != '':
        textList.append(text)
        tagged_token = tagger.tag(textList)
        if tagged_token[0][1] == 'O':
            return 'None'
    else:
        return 'None'
    return tagged_token[0][1]


def process_spacy_pred(text):        
    """
        Further processing of Spacy NER output 
    """
    if text == 'ORG':
        return str.replace(text,'ORG','ORGANIZATION')
    elif text == 'CARDINAL':
        return str.replace(text,'CARDINAL','NUMBER')
    elif text == None:
        text = 'None'
        return text


def process_stanford_pred(text):
    """
        Further processing of Stanford CoreNLP NER output 
    """
    if text == 'STATE_OR_PROVINCE':
        return str.replace(text,'STATE_OR_PROVINCE','GPE')
    elif text == 'CITY':
        return str.replace(text,'CITY','GPE')



def generate_metafeatures(df):
    """
        Generates metafeatures that will be used by RF models for NER prediction
        df : dataframe
    """
    
    spacy_futures = []
    stan_temp_futures = []; stan_futures = []
    text_list = df['Text'].values
    
    # Spacy Multi-threading to speed up the computation using Pipe
    t2 = time()
    docs = nlp.pipe(text_list)
    for i in docs:
        for ent in i.ents:
            spacy_futures.append(ent.label_)
    spacy_series = pd.Series(spacy_futures)
    print("TIME Spacy",time()-t2)

    df['pred_label_Spacy'] = spacy_series
    # t3 = time()
    df['pred_label_NLTK'] = df.apply(lambda x: run_nltk_ner(x['Text']), axis=1)
    # print("TIME NLTK",time()-t3)
    
    t1 = time()
    for i in text_list:
        stan_temp_futures.append(stan_pool.submit(run_stanford_corenlp_ner,i))
    for x in as_completed(stan_temp_futures):
        stan_futures.append(x.result())
    stan_series = pd.Series(stan_futures)
    print("TIME Stan",time() - t1)
    df['pred_label_Stanford'] = stan_series
    
    df2 = df.copy()
    df2['pred_label_Spacy'] = df2.apply(lambda x: process_spacy_pred(x['pred_label_Spacy']), axis=1)
    df2['pred_label_Stanford'] = df2.apply(lambda x: process_stanford_pred(x['pred_label_Stanford']), axis=1)
    return df2


def prepare_data(df,mf):
    """
        Stacking Spacy, NLTK, Stanford CoreNLP predictions

        INPUT
        ------------------
        df : raw dataframe
        mf : metafeatures
    """
    
    pred_spacy_nltk = np.column_stack((df['pred_label_Spacy'],df['pred_label_NLTK']))
    pred_all = np.column_stack((pred_spacy_nltk,mf['pred_label_Stanford']))

    pred_all_df = pd.DataFrame(data=pred_all,columns=['pred_Spacy','pred_NLTK','pred_Stanford'])

    pred_all_df['pred_Spacy'] = pred_all_df['pred_Spacy'].apply(str)
    pred_all_df['pred_NLTK'] = pred_all_df['pred_NLTK'].apply(str)
    pred_all_df['pred_Stanford'] = pred_all_df['pred_Stanford'].apply(str)

    pred_all_df['pred_combined'] = pred_all_df['pred_Spacy'] + ' ' + pred_all_df['pred_NLTK'] + ' ' + pred_all_df['pred_Stanford']
    return pred_all_df['pred_combined']


def inverse_transform_label(data):
    """
        Tranform the numeric encoding back to the NER label

        INPUT
        ----------------
        data : np.array,
    """
    ref_dict = {'0' : 'GPE', '1' : 'NUMBER', '2' : 'None', '3' : 'ORGANIZATION', '4' : 'PERSON'}
    
    final_label = []
    for i in data:
        for j in ref_dict.keys():
            if i == int(j):
                final_label.append(ref_dict[j])
    return pd.Series(final_label)

# Encoding the target
# 0 : GPE, 1 : Number, 2 : None, 3 : Org, 4 : Person
# Predict on test data
def predict_entity(test_data):
    """
        test_data : test_data
    """
    filename = MODEL_PATH
    loaded_model = joblib.load(filename) # load the model from disk
    predictions = loaded_model.predict(test_data)
    return predictions

def run_ner_engine(df):
    """
        df : input dataframe
    """
    
    # print(os.path.dirname(os.path.abspath(__file__)))
    mf = generate_metafeatures(df)
    
    test_data = prepare_data(df,mf)

    predictions = predict_entity(test_data)
    # print(predictions)
    # final_pred_label = inverse_transform_label(predictions)
    return predictions


# if __name__ == '__main__':
#     # check_dir_exist('./ai_data/ner/ocr_json/')
#     ocr_json = './ai_data/ner/ocr_json/'
#     print("Running NER \n {}".format(run_ner_engine(ocr_json)))