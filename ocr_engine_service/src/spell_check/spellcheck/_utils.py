import os, string

# GAZETTER_DIR_PATH = 'src/spell_check/static_data/gazetters/'
# US_CITIES_PATH = os.path.join(GAZETTER_DIR_PATH,'us_cities.txt')

def get_current_path():
    return os.path.dirname(os.path.realpath(__file__))

def clean_string(text):
    return text.translate(str.maketrans('','',string.punctuation))

def load_text_data(city_file_path):
    """
        INPUT
        -------
            city_file_path : path to text file
        RETURNS
        -------
            text_cleaned : list of cleaned city
    """
    with open(city_file_path, 'r') as f:
        textList = f.read().split('\n')
    text_cleaned = list(set([clean_string(x) for x in textList if clean_string(x) != '']))
    return text_cleaned

def check_dir_exist(directory):
    '''
    Function to check whether a directory exists
    If not exists, creates a new one
    '''
    if not os.path.exists(directory):
        os.makedirs(directory)
