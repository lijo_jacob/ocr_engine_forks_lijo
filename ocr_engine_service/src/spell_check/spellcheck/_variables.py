import re
import os
from django.conf import settings

# img_path_local = 'ai_data/spellCheck/img'
# json_path_local = 'ai_data/spellCheck/json'
# HUNSPELL_PATH = os.path.join(settings.MEDIA_ROOT, 'nlp','spellCheck','hunspell')
HUNSPELL_PATH = 'src/spell_check/static_data/hunspell'
# GAZETTER_DIR_PATH = os.path.join(settings.MEDIA_ROOT, 'nlp','spellCheck','gazetters')
GAZETTER_DIR_PATH = 'src/spell_check/static_data/gazetters/'
TEMP_IMG_PATH = 'ai_data/spellCheck/temp_img'
# US_CITIES_PATH = 'src/spell_check/static_data/gazetters/us_cities.txt'
US_CITIES_PATH = os.path.join(GAZETTER_DIR_PATH, 'us_cities.txt')

flags_collection_name = 'rcmb_ai_flags'

# match all whitespace except newlines
NORMALIZE_WHITESPACE_REGEX = re.compile(r'[^\S\n]+', re.UNICODE)

RE_DASH_FILTER = re.compile(r'[\-\˗\֊\‐\‑\‒\–\—\⁻\₋\−\﹣\－]', re.UNICODE)

RE_APOSTROPHE_FILTER = re.compile(r'&#39;|[ʼ՚＇‘’‛❛❜ߴߵ`‵´ˊˋ{}{}{}{}{}{}{}{}{}]'.format(
    chr(768), chr(769), chr(832), chr(833), chr(2387), chr(5151),
    chr(5152), chr(65344), chr(8242)),
    re.UNICODE)

RE_LEFT_PARENTH_FILTER = re.compile(r'[\(\[\{\⁽\₍\❨\❪\﹙\（]', re.UNICODE)

RE_RIGHT_PARENTH_FILTER = re.compile(r'[\)\]\}\⁾\₎\❩\❫\﹚\）]', re.UNICODE)

ALLOWED_CURRENCIES = """¥£₪$€฿₨"""

# ALLOWED_PUNCTUATION = """-!?/;"'%&<>.()[]{}@#:,|=*"""

ALLOWED_PUNCTUATION = """-?/"'%&<>.{}@#,="""  # Removed (){}|:;!*

RE_BASIC_CLEANER = re.compile(r'[^\w\s{}{}]'.format(
    re.escape(ALLOWED_CURRENCIES), re.escape(ALLOWED_PUNCTUATION)),
    re.UNICODE)

# date = "12/11/1995"
RE_DATE_FILTER = re.compile(
    "^(([1-9]|0[1-9])|1[0-2])(.|-\/)(([1-9]|0[1-9])|1[0-9]|2[0-9]|3[0-1])(.|-|\/)(19|20)[0-9][0-9]$")

# find_date('12/31/1995')SVILLE,NY' SUGGESTION ['EVANSVILLE'] FLAG FalseF RATIO 73 ITEM DANSVILLE,NY SUGG EVANSF RATIO 73 ITEM DANSVILLE,NY SUGG EVANS
RE_SSN_FILTER = re.compile("^(X{3})(-)(X{2})(-)[0-9]{4}$")

RE_AMRN_FILTER = re.compile("^U[1-9]{1}[0-9]{6}$")

RE_AGE_FILTER = re.compile(
    "^(([1-9])|[1-9][0-9]|[1][0-9][0-9]) Y$")  # 1 Y to 199 Y

RE_PHONE_US_FILTER = re.compile(
    "\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4}")
