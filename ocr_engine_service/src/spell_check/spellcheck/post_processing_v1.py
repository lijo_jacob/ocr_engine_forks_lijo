import os
import shutil
import re
import ast
import json
import cv2
import base64
import numpy as np
import pandas as pd
import ast
import hunspell
from textblob import Word
from nltk.corpus import stopwords
from time import time
from pymongo import MongoClient
from common.utils import upload_to_s3, fpath_from_s3
from ._utils import check_dir_exist
from ._variables import (
    GAZETTER_DIR_PATH, HUNSPELL_PATH,
    US_CITIES_PATH, flags_collection_name
)


master_db = os.environ["MASTER_COLLECTION"]
mongo = MongoClient(os.environ["RCMB_DB_URI"])

# # Path of ocr json and image
# ocr_file = 'spell_check/response.json'
# ocr_img = 'spell_check/sample.jpg'

# Path of medical , non medical and us-cities gazzeters
# medical_words = os.path.join("spellcheck","spell_check_util","gazetters",'medical.txt')
# nonmedical_words = os.path.join("spellcheck","spell_check_util","gazetters",'non_medical.txt')
# uscities_words = os.path.join("spellcheck","spell_check_util","gazetters",'us_cities.txt')
s3_gazzeters_path = 'ai_data/new_training/hunspell_dic'


def load_keyword(HUNOBJ, fpaths):
    for paths in fpaths:
        with open(paths, 'r') as f:
            MED_TOKEN_LIST = f.read().splitlines()
            for keyword in MED_TOKEN_LIST:
                HUNOBJ.add(keyword)
    return HUNOBJ


def sync_gazzeters():

    # gazetters we need
    gazzet = ['medical.txt', 'non_medical.txt', 'us_cities.txt']
    pred_files_condition = {"key": "hunspell_files"}
    status_pred_files = mongo[master_db][flags_collection_name].find_one(
        pred_files_condition)
    if status_pred_files.get('updated_timestamp'):
         db_latest_val = str(status_pred_files.get('updated_timestamp'))
    else:
        return None
    check_dir_exist(os.path.join(GAZETTER_DIR_PATH, db_latest_val))
    fpaths = [os.path.join(GAZETTER_DIR_PATH, db_latest_val + '/', i)
              for i in gazzet]
    print ('fpathsss are', fpaths)
    HUNOBJ = load_hunspell_object()
    try:

        HUNOBJ = load_keyword(HUNOBJ, fpaths)
    except Exception as e:
        for elements in fpaths:
            #print ('basename', os.basename(elements))
            s3_file_path = os.path.join(
                s3_gazzeters_path, os.path.basename(elements))
            print('Downloading Gazetters files from S3...', s3_file_path)
            __ = fpath_from_s3(s3_file_path, fpath=elements)
        HUNOBJ = load_keyword(HUNOBJ, fpaths)
    return HUNOBJ

    # if status_pred_files.get('pending_update') == True:
    #     for i in gazzet:
    #         fpath = os.path.join(GAZETTER_DIR_PATH, i)
    #         if not os.path.exists(fpath):
    #             os.mknod(fpath)

    #         if force_download:
    #             s3_file_path = os.path.join(s3_gazzeters_path, i)
    #             print('Downloading Gazetters files from S3...')
    #             __ = fpath_from_s3(s3_file_path, fpath=fpath)
    #     __ = mongo[master_db][flags_collection_name].update(
    #         pred_files_condition, {"$set": {"pending_update": False}})
    # else:
    #     print('Model files are up-to-date!')


def load_json_df(ocr_file):
    # Loading the json file as dataframe
    with open(ocr_file) as f:
        ocr_data = json.load(f)

    df_img = ocr_data['data']
    df_data = pd.DataFrame(df_img)
    return df_data


def load_hunspell_object():
    # Loading hunspell objects and with its respective language dictionary
    en_US_dic = os.path.join(HUNSPELL_PATH, './en_US.dic')
    en_US_aff = os.path.join(HUNSPELL_PATH, './en_US.aff')
    # creating 4 hunspell objects using the dictionary and affilations
    HUNOBJ = hunspell.HunSpell(en_US_dic, en_US_aff)
    return HUNOBJ


# def load_keywords(HUNOBJ):

#     # adding medical words to hunspell objects
#     with open(GAZETTER_DIR_PATH + '/medical.txt', 'r') as f:
#         MED_TOKEN_LIST = f.read().splitlines()
#     for keyword in MED_TOKEN_LIST:
#         HUNOBJ.add(keyword)

#     # adding non-medical words to hunspell objects
#     with open(GAZETTER_DIR_PATH + '/non_medical.txt', 'r') as f:
#         NON_MED_TOKEN_LIST = f.read().splitlines()
#     for keyword in NON_MED_TOKEN_LIST:
#         HUNOBJ.add(keyword)

#     # adding uscities words to hunspell objects
#     with open(US_CITIES_PATH, 'r') as f:
#         US_CITIES_TOKEN_LIST = f.read().splitlines()
#     for keyword in US_CITIES_TOKEN_LIST:
#         HUNOBJ.add(keyword)


def hunspell_check(comment, hun_obj):
    if hun_obj:
        # check the word in the gazetters if its there then its a correct word
        spellFlag = hun_obj.spell(comment)

        if spellFlag == True:
            return ""

        else:
            return comment
    else:
        return comment


def hunspell_suggest(comment, hun_obj):
    if hun_obj:
        # suggest correct spelling words using hunspell
        suggestedToken = hun_obj.suggest(comment)

        if suggestedToken != []:
            return suggestedToken
        else:
            return []
    else:
        return []


def textblob_suggest(comment):
    # getting the list of correct words
    correct = []
    w = Word(comment)
    w = w.spellcheck()
    for i in w:
        if i[1] > 0.5 and i[0] != '':
            correct.append(i[0])
    if correct != []:
        return correct
    else:
        return []


def normalize_words(comment, lowercase, remove_stopwords):
    stops = stopwords.words("english")

    # checking whether it has number
    if bool(re.search(r'\d', comment)):
        return ""

    # removing non alphabets
    regex = re.compile('[^a-z\sA-Z]')
    comment = regex.sub('', comment)
    return comment


def run_post_processing_engine(ocr_json_file):

    # loading the ocr json as dataframe
    df_data = load_json_df(ocr_json_file)

    # initiate hunspell gazetters update
    # sync_gazzeters()
    HUNOBJ = sync_gazzeters()
    # loading the hunspell objects
    #HUNOBJ = load_hunspell_object()

    # Add medical , non-medical and us-cities keywords into the hunspell object
    # load_keywords(HUNOBJ)

    # Normalize the words and return the processed words
    df_processed_words = pd.DataFrame()
    df_processed_words['Text'] = df_data['Text']
    df_processed_words['Text_After_Clean'] = df_data['Text'].apply(
        normalize_words, lowercase=True, remove_stopwords=True)
    df_processed_words['Text_After_Hunspell'] = df_processed_words['Text_After_Clean'].apply(
        hunspell_check, hun_obj=HUNOBJ)
    df_processed_words['Suggested_Hunspell'] = df_processed_words['Text_After_Hunspell'].apply(
        hunspell_suggest, hun_obj=HUNOBJ)
    df_processed_words['Suggested_Textblob'] = df_processed_words['Text_After_Hunspell'].apply(
        textblob_suggest)
    df_data['Suggested_Hunspell'] = df_processed_words['Suggested_Hunspell']
    df_data['Suggested_Textblob'] = df_processed_words['Suggested_Textblob']
    df_data['New_text'] = df_data['Text']
    return df_data
