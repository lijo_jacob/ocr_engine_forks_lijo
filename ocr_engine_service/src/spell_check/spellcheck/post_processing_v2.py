#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 28 14:12:48 2018

@author: mohit
"""

import os
import shutil
import re
import ast
import json
import cv2
import base64
import numpy as np
import pandas as pd

import hunspell
import spacy
from geotext import GeoText
from fuzzywuzzy import fuzz
from time import time

from django.conf import settings
from common.error_constants import ERR_401
from common.utils import log_error, get_extn

from ._variables import (
    TEMP_IMG_PATH, GAZETTER_DIR_PATH, HUNSPELL_PATH,
    US_CITIES_PATH
)

from ._utils import (
    check_dir_exist, clean_string, load_text_data
)

from ._variables import (
    NORMALIZE_WHITESPACE_REGEX, RE_DASH_FILTER, RE_APOSTROPHE_FILTER,
    RE_LEFT_PARENTH_FILTER, RE_RIGHT_PARENTH_FILTER, ALLOWED_CURRENCIES,
    ALLOWED_PUNCTUATION, RE_BASIC_CLEANER
)

from ._variables import (
    RE_DATE_FILTER, RE_SSN_FILTER, RE_AMRN_FILTER,
    RE_AGE_FILTER, RE_PHONE_US_FILTER
)

nlp = spacy.load('en')


def generate_ocr_df(img_d, img_path):
    """
        img_d : image ocr JSON data
        img_path : path to the image
    """
    try:
        df_img = ast.literal_eval(img_d)
        df_data = pd.DataFrame(df_img['data'])
    except ValueError:
        try:
            df_img = img_d['data']
            df_data = pd.DataFrame(df_img)
        except ValueError:
            df_img = img_d['data']['data']
            df_data = pd.DataFrame(df_img)

    df_data['old_text'] = df_data['Text']
    df = df_data[['Text', 'old_text', 'br_x', 'br_y', 'tl_x',
                  'tl_y', 'index', 'width', 'height', 'word_length']]
    img = cv2.imread(img_path)

    check_dir_exist(TEMP_IMG_PATH)

    ocr_df = pd.DataFrame()
    for i in range(df.shape[0]):
        crop_img = img[np.int(df.loc[i]['tl_y']):np.int(df.loc[i]['br_y']), np.int(
            df.loc[i]['tl_x']):np.int(df.loc[i]['br_x'])]
        cv2.imwrite(TEMP_IMG_PATH + "/Image_" + str(int(df.loc[i]['tl_x'])) + "_" + str(
            int(df.loc[i]['br_x'])) + ".jpg", crop_img)
        with open(TEMP_IMG_PATH + "/Image_" + str(int(df.loc[i]['tl_x'])) + "_" + str(int(df.loc[i]['br_x'])) + ".jpg", "rb") as fid:
            data = fid.read()
        b64_bytes = base64.b64encode(data)
        ocr_df = ocr_df.append({'item_text_list': df.loc[i]['Text'], 'old_text': df.loc[i]['old_text'], 'br_x': df.loc[i]['br_x'], 'br_y': df.loc[i]['br_y'], 'tl_x': df.loc[i]['tl_x'], 'tl_y': df.loc[i]
                                ['tl_y'], 'base64': b64_bytes, 'index': int(df.loc[i]['index']), 'width': df.loc[i]['width'], 'height': df.loc[i]['height'], 'word_length': df.loc[i]['word_length']}, ignore_index=True)

    return ocr_df


def load_data(json_path, img_path):
    """
        file_path : path to JSON file
    """
    with open(json_path) as f:
        ocr_data = json.load(f)
    return ocr_data


def get_hunspell_dict(hunspell_path):
    """
        hunspell_path : path to the hunspell dictionaries
    """
    en_US_dic = os.path.join(hunspell_path, './en_US.dic')
    en_US_aff = os.path.join(hunspell_path, './en_US.aff')

    HUNOBJ = hunspell.HunSpell(os.path.join(
        HUNSPELL_PATH, './en_US_backup.dic'), en_US_aff)
    HUNOBJ_MED = hunspell.HunSpell(en_US_dic, en_US_aff)
    HUNOBJ_OTHER = hunspell.HunSpell(en_US_dic, en_US_aff)
    HUNOBJ_FINAL = hunspell.HunSpell(en_US_dic, en_US_aff)
    return HUNOBJ, HUNOBJ_MED, HUNOBJ_OTHER, HUNOBJ_FINAL


def generate_medical_tokens(HUNOBJ_MED):
    """
        HUNOBJ_MED : Hunspell object containing the medical terms
    """
    with open(GAZETTER_DIR_PATH + '/medical.txt', 'r') as f:
        MED_TOKEN_LIST = f.read().splitlines()

    for keyword in MED_TOKEN_LIST:
        HUNOBJ_MED.add(keyword)
    return MED_TOKEN_LIST


def generate_non_medical_token(HUNOBJ_OTHER):
    """
        HUNOBJ_OTHER : Hunspell object containing non-medical terms
    """
    with open(GAZETTER_DIR_PATH + '/non_medical.txt', 'r') as f:
        NON_MED_TOKEN_LIST = f.read().splitlines()

    for keyword in NON_MED_TOKEN_LIST:
        HUNOBJ_OTHER.add(keyword)
    return NON_MED_TOKEN_LIST


def add_image_vocab(HUNOBJ, HUNOBJ_OTHER, text_list):
    """
        HUNOBJ_1 : HUNOBJ
        HUNOBJ_2 : HUNOBJ_OTHER
        text_list : columns containing text
    """
    def add_true_token(spellchecker, text):

        x= [ c for c in text if ord(c) < 128 ]
        text=''.join(x)
        # print('^^^^^^^^^^^^^^^^', text)
        text = text.encode('latin-1')
        spellFlag = spellchecker.spell(text)
        if spellFlag:
            HUNOBJ_OTHER.add(text)
            return spellFlag
        return spellFlag

    count_incorrect_word = 0
    count_correct_word = 0
    for item in text_list:
        x = [c for c in item if ord(c) < 128]
        item = ''.join(x)
        spellFlag = add_true_token(HUNOBJ, item)
        if spellFlag:
            count_correct_word += 1
        elif spellFlag == False:
            count_incorrect_word += 1


def run_spacy_ner(text):
    doc = nlp(text)
    for ent in doc.ents:
        return ent.label_


def clean_text(text):
    """Clean text - remove unwanted chars, punctuation etc."""

    text = str(text).strip()
    text = NORMALIZE_WHITESPACE_REGEX.sub(' ', text)
    text = RE_DASH_FILTER.sub('-', text)
    text = RE_APOSTROPHE_FILTER.sub("'", text)
    text = RE_LEFT_PARENTH_FILTER.sub("(", text)
    text = RE_RIGHT_PARENTH_FILTER.sub(")", text)
    text = RE_BASIC_CLEANER.sub('', text)
    if text == '':
        return 'None'
    else:
        return text


def find_date(text):
    """
        date = "12/11/1995"
    """
    def matches(x): return x.group() if x else False
    if matches(RE_DATE_FILTER.search(text)):
        return True
    else:
        return False


def find_ssn(text):
    """
        XXX-XX-1234
    """
    def matches(x): return x.group() if x else False
    if matches(RE_SSN_FILTER.search(text)):
        return True
    else:
        return False


def find_amrn(text):
    """
        AMRN : U1234567
    """
    def matches(x): return x.group() if x else False
    if matches(RE_AMRN_FILTER.search(text)):
        return True
    else:
        return False


def find_age(text):
    """
        AGE : 5Y, 56 years
    """
    def matches(x): return x.group() if x else False
    if matches(RE_AGE_FILTER.search(text)):
        return True
    else:
        return False


def find_phone_US(text):
    def matches(x): return x.group() if x else False
    if matches(RE_PHONE_US_FILTER.search(text)):
        return True
    else:
        return False


def _binarySearch(l, target):
    """
        Binary Search on String

        INPUT
        ---------------------
        l : list of strings
        target : target string
    """
    start = 0
    end = len(l) - 1
    l = sorted(l)
    ml = list()  # list of midpoints
    while start <= end:
        middle = int((start + end) / 2)
        midpoint = l[middle]
        ml.append(midpoint)
        if midpoint > target:
            end = middle - 1
        elif midpoint < target:
            start = middle + 1
        else:
            ml.append(midpoint)
            return ml
    return ml


def find_location(text, CITY_LIST):
    """
        text : incorrect text
        CITY_LIST : list of city names
    """
    if isinstance(text, (bytes, bytearray)):
        text = text.decode('unicode_escape').strip()
    text = text.strip().title()

    temp_list = []
    temp_dict = {}
    bs_list = _binarySearch(CITY_LIST, text)

    for city in bs_list:
        fuzz_score = fuzz.ratio(text, city)
        temp_dict['fuzz_loc'] = fuzz_score
        temp_dict['text'] = text
        temp_dict['city'] = city
        temp_list.append(temp_dict.copy())

    new_temp = sorted(temp_list, key=lambda x: x['fuzz_loc'], reverse=True)

    if new_temp[0]['fuzz_loc'] > 80:
        return True, new_temp[0]['city']
    else:
        return False


def get_correct_med_text(spellchecker, text):

    x = [c for c in text if ord(c) < 128]
    # print('x',x)
    text = ''.join(x)

    """
        Get correct text related to medical vocabulary
        spellchecker : spell checker object
        text : text to be corrected
    """
    corrected_text = ''
    text = text.strip()
    if text == '':
        corrected_text = corrected_text + text
    else:
        text = clean_text(text)
        text = text.encode('latin-1')
#         text = text.encode('latin-1').title()
        spellFlag = spellchecker.spell(text)
        if spellFlag:
            suggestedToken = []
            return suggestedToken, spellFlag, None
        else:
            spacy_flag = run_spacy_ner(text.decode('latin-1'))
            suggestedToken = spellchecker.suggest(
                text.decode('unicode_escape'))
            text = text.decode('unicode_escape')
            # Find date
            if find_date(text):
                spellFlag = True
                return suggestedToken, spellFlag, spacy_flag
            # Find SSN
            if find_ssn(text.upper()):
                spellFlag = True
                return suggestedToken, spellFlag, spacy_flag
            if find_amrn(text.upper()):
                spellFlag = True
                return suggestedToken, spellFlag, spacy_flag
            if find_age(text.upper()):
                spellFlag = True
                return suggestedToken, spellFlag, spacy_flag
            if find_phone_US(text.upper()):
                spellFlag = True
                return suggestedToken, spellFlag, spacy_flag
            if find_location(text, US_CITY_LIST):
                spellFlag, suggestedToken = find_location(text, US_CITY_LIST)
                return suggestedToken, spellFlag, spacy_flag
            if len(suggestedToken) > 0:
                if fuzz.ratio(text.upper(), suggestedToken[0]) > 97:
                    spellFlag = True
                    return suggestedToken, spellFlag, spacy_flag

            return suggestedToken, spellFlag, spacy_flag


def run_autocorrect_v1(spellchecker, file_df, file_name):
    """
        spellchecker : Hunspell spell check object
        file_df : pd Dataframe containing brx, bry, tlx, tly, item_text_list
        file_name : name of the file
    """
    incorrect_counter = 0
    correct_counter = 0
    suggest_yes = 0
    suggest_no = 0
    f_df = pd.DataFrame(columns=('Text', 'old_text', 'index', 'isCorrect', 'Suggestion',
                                 'F-Ratio', 'Entity', 'base64', 'filename', 'br_x', 'tl_x', 'word_length', 'b64_value'))

    for index, item in zip(file_df['index'], file_df['item_text_clean']):
        brx_value = file_df[(file_df['item_text_clean'] == item) & (
            file_df['index'] == index)]['br_x'].iat[0]
        bry_value = file_df[(file_df['item_text_clean'] == item) & (
            file_df['index'] == index)]['br_y'].iat[0]
        tlx_value = file_df[(file_df['item_text_clean'] == item) & (
            file_df['index'] == index)]['tl_x'].iat[0]
        tly_value = file_df[(file_df['item_text_clean'] == item) & (
            file_df['index'] == index)]['tl_y'].iat[0]
        index_value = file_df[(file_df['item_text_clean'] == item) & (
            file_df['index'] == index)]['index'].iat[0]
        old_text_value = file_df[(file_df['item_text_clean'] == item) & (
            file_df['index'] == index)]['old_text'].iat[0]
        width = file_df[(file_df['item_text_clean'] == item) &
                        (file_df['index'] == index)]['width'].iat[0]
        height = file_df[(file_df['item_text_clean'] == item) & (
            file_df['index'] == index)]['height'].iat[0]
        word_length = file_df[(file_df['item_text_clean'] == item) & (
            file_df['index'] == index)]['word_length'].iat[0]
        b64_value = file_df[(file_df['item_text_clean'] == item) & (
            file_df['index'] == index)]['base64'].iat[0]
        # print('Item', item,type(item))
        x = [c for c in item if ord(c) < 128]
        # print('x',x)
        item = ''.join(x)
        if item.strip() != '':

            suggestedToken, spellFlag, entity = get_correct_med_text(
                spellchecker, item)
            if spellFlag:
                correct_counter += 1
                f_df = f_df.append([{'Text': item, 'old_text': old_text_value, 'index': index_value, 'isCorrect': spellFlag, 'Suggestion': None, 'F-Ratio': None, 'Entity': entity, 'base64': file_df['base64'],
                                     'filename':file_name, 'br_x':brx_value, 'br_y':bry_value, 'tl_x':tlx_value, 'tl_y':tly_value, 'width':width, 'height':height, 'word_length':word_length, 'b64_value':b64_value}], ignore_index=True)
            elif spellFlag == False:
                incorrect_counter += 1
                if len(suggestedToken) > 0:
                    f_ratio = fuzz.ratio(
                        item.lower(), suggestedToken[0].lower())
                    f_ratio_list = [{"ratio": fuzz.ratio(
                        item.lower(), i.lower()), "element": i} for i in suggestedToken]
                    sorted_list = sorted(
                        f_ratio_list, key=lambda k: k['ratio'], reverse=True)
                    sorted_list = [i['element'] for i in sorted_list]
                    f_df = f_df.append({'Text': item, 'old_text': old_text_value, 'index': index_value, 'isCorrect': spellFlag, 'Suggestion': sorted_list, 'F-Ratio': f_ratio, 'Entity': entity,
                                        'base64': file_df['base64'], 'filename': file_name, 'br_x': brx_value, 'br_y': bry_value, 'tl_x': tlx_value, 'tl_y': tly_value, 'width': width, 'height': height, 'word_length': word_length, 'b64_value': b64_value}, ignore_index=True)
                    suggest_yes += 1
                else:
                    f_df = f_df.append({'Text': item, 'old_text': old_text_value, 'index': index_value, 'isCorrect': spellFlag, 'Suggestion': None, 'F-Ratio': None, 'Entity': entity,
                                        'base64': file_df['base64'], 'filename': file_name, 'br_x': brx_value, 'br_y': bry_value, 'tl_x': tlx_value, 'tl_y': tly_value, 'width': width, 'height': height, 'word_length': word_length, 'b64_value': b64_value}, ignore_index=True)
                    suggest_no += 1

    f_df = f_df.sort_values('F-Ratio', ascending=False).sort_index()
    return f_df


US_CITY_LIST = load_text_data(US_CITIES_PATH)


@log_error(ERR_401)
def run_post_processing_engine(ocr_file, ocr_img):
    """
        ocr_file : json ocr path
        ocr_img : ocr image
    """
    file_name = ocr_file.rsplit('/', 1)[1].split('.')[0]

    raw_df = load_data(ocr_file, ocr_img)

    file_data = generate_ocr_df(raw_df, ocr_img)

    HUNOBJ, HUNOBJ_MED, HUNOBJ_OTHER, HUNOBJ_FINAL = get_hunspell_dict(
        HUNSPELL_PATH)
    MED_TOKEN_LIST = generate_medical_tokens(HUNOBJ_MED)
    NON_MED_TOKEN_LIST = generate_non_medical_token(HUNOBJ_OTHER)
    file_data['item_text_clean'] = file_data.apply(
        lambda row: clean_text(row['item_text_list']), axis=1)
    # Adds true tokens to HUNOBJ_OTHER
    add_image_vocab(HUNOBJ, HUNOBJ_OTHER, file_data['item_text_clean'])

    for keyword in MED_TOKEN_LIST:
        HUNOBJ_OTHER.add(keyword)
    for keyword in NON_MED_TOKEN_LIST:
        HUNOBJ_OTHER.add(keyword)

    ocr_df = run_autocorrect_v1(HUNOBJ_OTHER, file_data, file_name)
    ocr_df['Text_True'] = ocr_df['Text'][ocr_df['isCorrect'] == True]
    ocr_df['Text_False'] = ocr_df['Text'][ocr_df['isCorrect'] == False]
    ocr_df.rename(columns={'Text': 'New_text',
                           'old_text': 'Text'}, inplace=True)
    ocr_df = ocr_df[['New_text', 'isCorrect', 'Suggestion', 'Text_True', 'Text_False', 'index',
                     'tl_x', 'tl_y', 'width', 'height', 'br_x', 'br_y', 'Text', 'word_length', 'Entity']]

    return ocr_df


if __name__ == '__main__':
    print(os.path.dirname(os.path.realpath(__file__)))
    ocr_file = 'ai_data/spellCheck/json/data_2.json'
    ocr_img = 'ai_data/spellCheck/img/data_2.png'
    print("Correct DF \n {}".format(
        run_post_processing_engine(ocr_file, ocr_img)))
