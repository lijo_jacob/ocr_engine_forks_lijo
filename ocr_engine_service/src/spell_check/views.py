from rest_framework.views import APIView
from rest_framework.response import Response

from common.request_validators import (
    check_for_keys_in_data, get_formatted_response
)

import os
import sys
import json
import shutil
import posixpath
import urllib.request
import urllib.parse
import urllib.error

import json
import time
from os import environ

from src.spell_check.spellcheck.post_processing_v1 import run_post_processing_engine

from common.utils import (
    fpath_from_s3, save_file_locally, get_extn
)


from common.s3buc_download import s3_call
from common.rcm_connections import get_rcmb_constant_json
constant = get_rcmb_constant_json()

aws_s3 = constant["s3"]
aws_access_key_id = aws_s3["aws_key"]
aws_secret_access_key = aws_s3["aws_secret"]
bucket = aws_s3["bucket_name"]

from pymongo import MongoClient
flags_collection_name = 'rcmb_ai_flags'


class SpellCheckOcr(APIView):

    """
            Spell Correction for JSON data from OCR images
    """

    def post(self, request):
        print('0')
        try:
            mongo = MongoClient(constant['mongo_rcmb']['db_uri'])
            print('1')
            master_db = constant['mongo_rcmb']['db']
            pred_files_condition = {"key": "hunspell_files"}
            status_pred_files = mongo[master_db][flags_collection_name].find_one(
                pred_files_condition)
            if status_pred_files.get('pending_update') == True:
                print('2')
                s3_call(aws_access_key_id, aws_secret_access_key, bucket)
                print('Downloading hunspell files from S3...')
                mongo[master_db][flags_collection_name].update(
                    pred_files_condition, {"$set": {"pending_update": False}})
                print('3')
            else:
                print('hunspell files are up-to-date!')

            msg = 'Spell Check Done'
            status = 'SUCCESS'
            print('4')
            image_data = request.FILES['image']
            json_csv_data = request.FILES['data']
            print('5')
            # Local Storage
            image_path = save_file_locally(image_data)
            json_csv_path = save_file_locally(json_csv_data)

            # if not imgpath: return (False, "IMAGE NOT FOUND")

            ocr_df = run_post_processing_engine(json_csv_path, image_path)
            print('6')
            data = json.loads(ocr_df.to_json(orient='records'))
            print('7')
        except Exception as e:
            raise e
            msg = str(e)
            status = 'ERROR'
            data = None
        context = {
            'status': status,
            'msg': msg,
            'data': data
        }
        return Response(context)
