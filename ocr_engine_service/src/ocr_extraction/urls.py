from django.conf.urls import url, include
from rest_framework import routers

from django.urls import path

from .views import (img_to_json, status_view,
                    ner_check, tess_only, img_to_json_v2, ImgCeleryApi, GetConnectionStatus, GitStatus, ImgSkewcorrectApi)

router = routers.DefaultRouter()
router.register(r'tess_only', tess_only, 'tess_only')
router.register(r'ner_json', ner_check, 'ner_check')
router.register(r'img_to_json', img_to_json, 'img_to_json')
router.register(r'img_to_json_v2', img_to_json_v2, 'img_to_json_v2')
router.register(r'status', status_view, 'status_view')
router.register(r'img_invoke_celery', ImgCeleryApi, 'img_invoke_celery')
router.register(r'connection_status', GetConnectionStatus, 'connection_status')
router.register(r'git_status', GitStatus, 'git_status')
router.register(r'skew_correct', ImgSkewcorrectApi, 'skew_correct')
# urlpatterns = [url(r'^', include(router.urls)), ]
urlpatterns = [
    url(r'^', include(router.urls)),

]
