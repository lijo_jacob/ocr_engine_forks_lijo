import re
import os
import cgi
import sys
import cv2
import time
import json
import boto3
import shutil
import random
import hashlib
import os.path
import requests
import datetime
import mimetypes
import posixpath
import numpy as np
import http.server
import urllib.error
import urllib.parse
import urllib.request
from io import BytesIO
import urllib.parse as urlparse
import time
from django.conf import settings
from rest_framework import viewsets
from rest_framework.response import Response
from cgi import parse_header, parse_multipart
from common.dml_utils import update_status_code

# from urllib.parse import parse_qs

from common.variables import BASE_SYS_NAME
from django.core.files.base import ContentFile
from common.rcm_connections import RcmbDbConnection
from django.core.files.storage import default_storage
from src.ocr_extraction.utilities.ocr_multi_proc import getOCRText
from src.ocr_extraction.utilities.s3_upload import connect_to_s3, upload_to_s3
from src.ocr_extraction.utilities.ocr_utils_multi_proc import skew_correction_only
# __upload_folder__ = './direc/'
method_flag = ['tess_only', 'connected_only', 'combined_method']


def convert_ocr_to_json(dData):
    try:

        from scripts.page_rotation_init_train import download_img_fpath_from_s3, download_img_fpath_from_s3_system_mapping
        dInputData = dData
        image_path = dInputData.get('file_url')
        __ = update_status_code(
            dInputData, 1.41, 'AI_OCR_ENGINE_FILE_RECIEVED')
        print("image path is   **   " + image_path)
        system_mapping = dInputData.get('system_mapping', None)
        fpath = None
        if not system_mapping:
            fpath = download_img_fpath_from_s3(image_path)
        else:
            print("elseee")
            fpath = download_img_fpath_from_s3_system_mapping(
                image_path, common_bucket=system_mapping)
        print("fpath is ************   " + fpath)

        extension = os.path.splitext(fpath)[1]
        resp = {}
        if(".pdf" == extension):
            from .utilities.non_layered_pdf_util import convert_pdf_img
            filename_list = convert_pdf_img(fpath)
        else:
            filename_list = [fpath]

        page_count = len(filename_list)
        __ = update_status_code(
            dInputData, 1.42, 'AI_OCR_ENGINE_FILE_DOWNLOADED_FROM_S3', page_count=page_count)

        print('***Pagewise List***: ', filename_list)
        # initialize dictionaries
        dict_data = {}
        dict_new_text = {}
        dict_text = {}
        dict_box_image_url = {}
        dict_image_url = {}
        dict_image_height = {}
        dict_image_width = {}
        dict_image_skew_angle = {}

        dict_schema = {}
        ocr_start_time = time.time()
        for ind, fp in enumerate(filename_list):
            print('\n'*10)
            print('Processing>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>%s of %s' %(str(ind+1),str(len(filename_list))))
            print('\n' * 10)
            fpath = fp
            fn = os.path.join(fpath)
            # fn = os.path.join(fp)

            if not fn:
                return (False, "Can't find out file name...")

            # methods for extraction are tess_only and combined_method
            ind_resp = getOCRText(fn, post_processing_flag=True,
                                  method_flag=method_flag[2],write_flag=False)
            print('\n' * 5)
            str_ind = str(ind + 1)
            dict_data[str_ind] = ind_resp['data']
            dict_new_text[str_ind] = ind_resp['New_text']
            dict_text[str_ind] = ind_resp['Text']
            dict_box_image_url[str_ind] = ind_resp['box_image_url']
            dict_image_url[str_ind] = ind_resp['image_url']
            dict_image_height[str_ind] = ind_resp['image_height']
            dict_image_width[str_ind] = ind_resp['image_width']
            dict_image_skew_angle[str_ind] = ind_resp['skew_correction_angle']


            dict_schema[str_ind] = ind_resp['schema']
            # if os.path.isfile(fn):
            #     os.remove(fn)
            # if os.path.isfile(fpath):
            #     os.remove(fpath)
            # print(ind_resp.keys())
        ocr_end_time = time.time()
        ocr_duration = ocr_end_time - ocr_start_time
        print('Took %s to process file %s' % (str(ocr_duration),fpath))
        __ = update_status_code(
            dInputData, 1.43, 'AI_OCR_ENGINE_EXTRACTION_COMPLETED',ocr_start_time  = str(ocr_start_time),
            ocr_end_time =str(ocr_end_time), ocr_duration = str(ocr_duration))

        resp['data'] = dict_data
        resp['New_text'] = dict_new_text
        resp['Text'] = dict_text
        resp['box_image_url'] = dict_box_image_url
        resp['image_url'] = dict_image_url
        resp['schema'] = dict_schema
        resp['img_height'] = dict_image_height
        resp['img_width'] = dict_image_width
        resp['skew_correction_angle'] = dict_image_skew_angle

        resp['msg'] = ''

        from common.utils import get_file_hash, get_extn
        resp['file_hash'] = get_file_hash(fpath)
        resp['file_extn'] = get_extn(fpath)
        resp["file_id"] = dInputData.get('file_id')
        resp['status'] = 'success'
        resp['dInputData'] = dInputData

        if system_mapping:
            print("Setting system mapping flag in the response")
            resp['system_mapping'] = True
        # with open('data.json', 'w') as outfile:
        #     json.dump(resp, outfile)
    except Exception as e:
        resp = {}
        from common.utils import get_file_hash, get_extn

        resp["file_id"] = dInputData.get('file_id')
        resp['status'] = 'failed'
        resp['dInputData'] = dData
        if fpath:
            resp['msg'] = str(e)
            resp['file_hash'] = get_file_hash(fpath)
            resp['file_extn'] = get_extn(fpath)
        else:
            fpath = ''
            resp['msg'] = 'File download Error'
            resp['file_hash'] = get_file_hash(fpath)
            resp['file_extn'] = get_extn(fpath)

    from src.ocr_extraction.utilities.utils import call_ocr_response_task
    __ = update_status_code(
        dInputData, 1.44, 'AI_OCR_ENGINE_DATA_PASSED_TO_NLP_ENGINE')
    call_ocr_response_task(resp)


class tess_only(viewsets.ViewSet):
    # def list(self, request):
    #     return Response("Success with get request")

    def copyfile(self, source, outputfile):
        print(source, outputfile)
        shutil.copyfileobj(source, outputfile)

    def do_POST(self):
        r, json_resp = self.create()
        f = BytesIO()
        f.write(b"%s" % json_resp.encode())
        length = f.tell()
        f.seek(0)
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.send_header("Content-Length", str(length))
        self.end_headers()
        if f:
            self.copyfile(f, self.wfile)
            f.close()

    def create(self, request):
        data = request.FILES['file']
        uniq_filename = (str(datetime.datetime.now())).replace(' ', '_')
        path = default_storage.save(
            uniq_filename, ContentFile(data.read()))
        fn = os.path.join(settings.MEDIA_ROOT, path)
        print('Image loaded: ', fn)
        if not fn:
            return (False, "Can't find out file name...")
        resp = getOCRText(fn, post_processing_flag=True,
                          method_flag=method_flag[0])
        json_resp = json.dumps(resp)
        if os.path.isfile(fn):
            os.remove(fn)
        if os.path.isfile(path):
            os.remove(path)

        RESP = Response(resp)

        return RESP


class status_view(viewsets.ViewSet):
    def create(self, request):
        # for post
        return Response({"status": 200})

    def list(self, request):
        # for get
        return Response({"status": 200})


class img_to_json_v2(viewsets.ViewSet):

    def copyfile(self, source, outputfile):
        print(source, outputfile)
        shutil.copyfileobj(source, outputfile)

    def do_POST(self):
        r, json_resp = self.create()
        f = BytesIO()
        f.write(b"%s" % json_resp.encode())
        length = f.tell()
        f.seek(0)
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.send_header("Content-Length", str(length))
        self.end_headers()
        if f:
            self.copyfile(f, self.wfile)
            f.close()

    def create(self, request):
        data = request.FILES['file']
        uniq_filename = (str(datetime.datetime.now())).replace(' ', '_')
        path = default_storage.save(
            uniq_filename, ContentFile(data.read()))
        fn = os.path.join(settings.MEDIA_ROOT, path)
        print('Image loaded: ', fn)
        if not fn:
            return (False, "Can't find out file name...")
        resp = getOCRText(fn, False, method_flag=method_flag[1])
        json_resp = json.dumps(resp)
        if os.path.isfile(fn):
            os.remove(fn)
        if os.path.isfile(path):
            os.remove(path)
        RESP = Response(resp)
        return RESP


class ner_check(viewsets.ViewSet):
    # def list(self, request):
    #     return Response("Success with get request")

    def copyfile(self, source, outputfile):
        print(source, outputfile)
        shutil.copyfileobj(source, outputfile)

    def do_POST(self):
        r, json_resp = self.create()
        f = BytesIO()
        f.write(b"%s" % json_resp.encode())
        length = f.tell()
        f.seek(0)
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.send_header("Content-Length", str(length))
        self.end_headers()
        if f:
            self.copyfile(f, self.wfile)
            f.close()

    def create(self, request):
        data = request.FILES['file']
        uniq_filename = (str(datetime.datetime.now())).replace(' ', '_')
        path = default_storage.save(
            uniq_filename, ContentFile(data.read()))
        fn = os.path.join(settings.MEDIA_ROOT, path)
        print('Image loaded: ', fn)
        if not fn:
            return (False, "Can't find out file name...")

        # methods for extraction are tess_only and combined_method
        resp = getOCRText(fn, True, method_flag=method_flag[0])
        json_resp = json.dumps(resp)
        if os.path.isfile(fn):
            os.remove(fn)
        if os.path.isfile(path):
            os.remove(path)

        RESP = Response(resp)

        return RESP


class img_to_json(viewsets.ViewSet):

    def copyfile(self, source, outputfile):
        print(source, outputfile)
        shutil.copyfileobj(source, outputfile)

    def do_POST(self):
        r, json_resp = self.create()
        f = BytesIO()
        f.write(b"%s" % json_resp.encode())
        length = f.tell()
        f.seek(0)
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.send_header("Content-Length", str(length))
        self.end_headers()
        if f:
            self.copyfile(f, self.wfile)
            f.close()

    def create(self, request):
        data = request.FILES['file']
        uniq_filename = (str(datetime.datetime.now())).replace(' ', '_')
        path = default_storage.save(
            uniq_filename, ContentFile(data.read()))
        fn = os.path.join(settings.MEDIA_ROOT, path)
        print('Image loaded: ', fn)
        if not fn:
            return (False, "Can't find out file name...")

        # methods for extraction are tess_only and combined_method
        resp = getOCRText(fn, post_processing_flag=True,
                          method_flag=method_flag[2])
        json_resp = json.dumps(resp)
        if os.path.isfile(fn):
            os.remove(fn)
        if os.path.isfile(path):
            os.remove(path)

        RESP = Response(resp)

        return RESP


class ImgCeleryApi(viewsets.ViewSet):

    def copyfile(self, source, outputfile):
        print(source, outputfile)
        shutil.copyfileobj(source, outputfile)

    def do_POST(self):
        r, json_resp = self.create()
        f = BytesIO()
        f.write(b"%s" % json_resp.encode())
        length = f.tell()
        f.seek(0)
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.send_header("Content-Length", str(length))
        self.end_headers()
        if f:
            self.copyfile(f, self.wfile)
            f.close()

    def create(self, request):
        dict_out = {"status": "SUCCESS"}
        data = request.data
        from job_man.tasks import ocr_img_to_json
        dParam = data.dict()
        print('^^^^^', dParam)
        from job_man.tasks import ocr_img_to_json
        # dParams = dInputData.dict()
        ocr_img_to_json(dParam)
        resp = 'SUCCESS'
        RESP = Response(dict_out)
        return RESP


class GetConnectionStatus(viewsets.ViewSet):

    def copyfile(self, source, outputfile):
        print(source, outputfile)
        shutil.copyfileobj(source, outputfile)

    def do_POST(self):
        r, json_resp = self.create()
        f = BytesIO()
        f.write(b"%s" % json_resp.encode())
        length = f.tell()
        f.seek(0)
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.send_header("Content-Length", str(length))
        self.end_headers()
        if f:
            self.copyfile(f, self.wfile)
            f.close()

    def create(self, request):
        from pymongo import MongoClient
        response = {"status": "success"}
        try:
            s3 = boto3.client(
                's3', aws_access_key_id=os.environ["S3_AWS_KEY"], aws_secret_access_key=os.environ["S3_AWS_SECRET"])
            s3BucketLocationRes = s3.get_bucket_location(
                Bucket=os.environ["S3_BUCKET_NAME"])
            response["s3Response"] = s3BucketLocationRes
        except:
            response["s3Exception"] = "S3Error"
            response["status"] = "ERROR"
        try:
            obj = RcmbDbConnection()
            mresponse = obj.get_rcmb_schema()
            response["mongoSchemaLength"] = len(mresponse["resources"])
        except:
            response["mongoError"] = "MongoDB Error"
            response["status"] = "ERROR"
        try:
            res = json.loads(requests.get(settings.NLP_URL).text)
            response["NLP_status"] = res
        except Exception as e:
            response["NLP_status"] = {"status": "ERROR"}
        return Response(response)

    def get(self, request):
        d = {"status": "success"}
        with open(os.path.join(settings.BASE_DIR, 'rcm_ai_service', 'git_status.txt')) as f:
            print('2')
            data = f.read()
        d['data'] = data
        return Response(d)


class GitStatus(viewsets.ViewSet):
    def create(self, request):
        d = {"status": "success"}
        with open(os.path.join(settings.BASE_DIR, 'ocr_engine_service', 'git_status.txt')) as f:
            #             print('2')
            data = f.read()
        d['data'] = data
        return Response(d)

    def list(self, request):
        d = {"status": "success"}
        with open(os.path.join(settings.BASE_DIR, 'ocr_engine_service', 'git_status.txt')) as f:
            data = f.read()
        d['data'] = data
        return Response(d)


class ImgSkewcorrectApi(viewsets.ViewSet):
    # from scripts.page_rotation_init_train import download_img_fpath_from_s3

    def copyfile(self, source, outputfile):
        print(source, outputfile)
        shutil.copyfileobj(source, outputfile)

    def do_POST(self):
        r, json_resp = self.create()
        f = BytesIO()
        f.write(b"%s" % json_resp.encode())
        length = f.tell()
        f.seek(0)
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.send_header("Content-Length", str(length))
        self.end_headers()
        if f:
            self.copyfile(f, self.wfile)
            f.close()

    def create(self, request):

        data = request.data
        from scripts.page_rotation_init_train import download_img_fpath_from_s3
        dParam = data.dict()
        base_name = dParam['file_url']

        dict_out = {"status": "success"}

        # image_path = request.FILES('file_url')
        # # data = request.FILES['file']
        fpath = skew_correction_only(download_img_fpath_from_s3(base_name))

        print('Local File Path: ', fpath)
        extension = os.path.splitext((base_name))[-1]
        out_name = os.path.join(os.path.split(base_name)[0], os.path.splitext(
            os.path.basename(base_name))[0] + '_out' + extension)
        connect_to_s3(os.environ['S3_AWS_KEY'], os.environ['S3_AWS_SECRET'],
                      settings.S3_BUCKET_NAME_IMG, subdirectory=os.path.split(base_name)[0])
        upload_to_s3(fpath, os.path.basename(out_name))

        dict_out['S3_url'] = os.path.join(
            settings.S3_BUCKET_NAME_IMG, out_name)
        return Response(dict_out)
