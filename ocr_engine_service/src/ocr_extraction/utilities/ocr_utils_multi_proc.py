import os
import gc
import re
import cv2
import sys
import csv
import glob
import time
import json
import uuid
import shutil
import random
import hashlib
import datetime
import functools
import subprocess
import pytesseract
import numpy as np
import pandas as pd
import multiprocessing

from scipy import ndimage
from ast import literal_eval
from os.path import basename
from dateutil.parser import parse
from sortedcontainers import SortedDict
from scipy.ndimage import interpolation as inter
from PIL import Image, ImageFilter, ImageEnhance

from .constants import dict_normal, model_json_path, model_path
from src.ocr_extraction.utilities.EAST.run_demo_server import main
from scipy.misc import imread, imresize, imshow
from keras.models import load_model, model_from_json
from tensorflow import Graph, Session

from .ufarray import UFarray
from .all_text_dict import get_final_json_v2,get_final_json
from .ocr_concurrent import (
    text_extract, text_extract_v2, text_extract_with_validation, ocr
)
from .connected_components_utils import (
    run, process_test_image, req_coods, get_bounding_box, box_with_tess
)
from cv2 import (
    boundingRect, countNonZero, cvtColor, drawContours, findContours,
    getStructuringElement, imread, morphologyEx, pyrDown, rectangle, threshold
)
from .utils import (
    get_ocr_response, dump_to_s3, upload_to_s3, s3_upload_model, upload_file_s3, get_s3_client
)
from src.spell_check.spellcheck.post_processing_v1 import run_post_processing_engine

# For cythonising script


type_dict = {'int': 10, 'float': 9, 'alpha': 8, 'list': 7, 'alpha_numeric': 6,
             'set': 5, 'dict': 4, 'bool': 3, 'date_time': 2, 'currency': 1, 'string': 0}
# Template matching
# t1 = cv2.imread('/home/vishnu/RCMB/DATA/templates/search_button.jpg')
# t2 = cv2.imread('/home/vishnu/RCMB/DATA/templates/calculator_button.jpg')
# t3 = cv2.imread('/home/vishnu/RCMB/DATA/templates/drop_down.jpg')


def check_dir_exist(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def is_date(string):
    try:
        parse(string)
        return True
    except ValueError:
        return False


def any_currency(s, curr="$"):
    return any(c in s for c in curr)


def check_custom_pattern(string):
    out = re.sub("[^a-zA-Z0-9_/-]", "", re.sub("\s+", "-", string))
    if out:
        return True


def get_type(s):
    try:
        k = literal_eval(s)
        val_type = type(k).__name__
    except Exception as e:
        if e.args[0] == 'invalid token':
            try:
                s = int(s)
            except:
                pass
        val_type = type(s).__name__

    if(val_type == 'str'):
        if(str(s).isalpha()):
            str_category = 'alpha'
        elif(is_date(str(s))):
            str_category = 'date_time'
        elif(str(s).isalnum()):
            str_category = 'alpha_numeric'
        elif(any_currency(s)):
            str_category = 'currency'
        elif check_custom_pattern(s):
            str_category = 'date_time'
        else:
            str_category = 'string'

        return str_category
    elif val_type == 'tuple':
        return 'string'
    else:
        return val_type


def md5Checksum(filePath):
    with open(filePath, 'rb') as fh:
        m = hashlib.md5()
        while True:
            data = fh.read(8192)
            if not data:
                break
            m.update(data)
        return m.hexdigest()


def get_type_old(word):
    word = word.replace('-', '')

    if word.isalpha():
        return 'str'
    elif word.isdecimal():
        return 'int'
    elif word.isalnum() or '$' in word:
        return 'alnum'

    else:
        try:
            __ = float(word)

            return 'float'
        except ValueError:
            return 'str'

    # Sort function based on the 2D - xy coordinate system


def convert_to_bw(img):
    bw = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    __, bw = cv2.threshold(bw, 127, 255, cv2.THRESH_BINARY)
    return bw


def cordSort_old(box1, box2):
    if abs(box1[1] - box2[1]) > 3:
        if box1[1] > box2[1]:
            return 1
        elif box1[1] < box2[1]:
            return -1
    if abs(box1[0] - box2[0]) > 3:
        if box1[0] > box2[0]:
            return 1
        elif box1[0] < box2[0]:
            return -1
        return 0
    return 0

# Sort function based on the 2D - xy coordinate system


def cordSort1(box1, box2):
    if abs(box1[5] - box2[5]) > 5:
        if box1[5] > box2[5]:
            return 1
        elif box1[5] < box2[5]:
            return -1
    # if abs(box1[4]-box2[4]):
    if box1[4] > box2[4]:
        return 1
    elif box1[4] < box2[4]:
        return -1
    return 0


def cordSort(box1, box2):
    if abs(box1[5] - box2[5]) < 5:
        if box1[4] > box2[4]:
            return 1
        elif box1[4] < box2[4]:
            return -1
    else:
        if box1[5] > box2[5]:
            return 1
        elif box1[5] < box2[5]:
            return -1
    return 0


def remove_buttons(img, t1, t2, t3):
    # t1: Search button template path
    # t2: Calculate button template path
    # t3: Drop down menu button path
    im = img.copy()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray_t_calc = cv2.cvtColor(t2, cv2.COLOR_BGR2GRAY)
    gray_t_drop_down = cv2.cvtColor(t3, cv2.COLOR_BGR2GRAY)
    primary_edges = cv2.Canny(gray, 32, 128, apertureSize=3)
    subimage_edges = cv2.Canny(t1, 32, 128, apertureSize=3)
    w1, h1, __ = t1.shape[::-1]
    w2, h2, __ = t2.shape[::-1]
    w3, h3, __ = t3.shape[::-1]
    res = cv2.matchTemplate(
        primary_edges, subimage_edges, cv2.TM_CCOEFF_NORMED)
    res2 = cv2.matchTemplate(gray, gray_t_calc, cv2.TM_CCOEFF_NORMED)
    res3 = cv2.matchTemplate(gray, gray_t_drop_down, cv2.TM_CCOEFF_NORMED)
    threshold = 0.5
    threshold_t_calc = 0.6
    threshold_t_drop_down = 0.7
    loc = np.where(res >= threshold)
    loc2 = np.where(res2 >= threshold_t_calc)
    loc3 = np.where(res3 >= threshold_t_drop_down)
    init_list = []
    init_list_2 = []
    init_list_3 = []
    white = [255, 255, 255]
    for pt in zip(*loc[::-1]):
        init_list.append([pt[0], pt[1], pt[0] + w1, pt[1] + h1])
    for pt in zip(*loc2[::-1]):
        init_list_2.append([pt[0], pt[1], pt[0] + w2, pt[1] + h2])
    for pt in zip(*loc3[::-1]):
        init_list_3.append([pt[0], pt[1], pt[0] + w2, pt[1] + h2])
    for b in init_list:
        x, y, w, h = b[0], b[1], b[2] - b[0], b[3] - b[1]
        avg_color_per_row = np.average(
            img[y:y + h - 2, x - 1:x + w + 50], axis=0)
        avg_color = np.average(avg_color_per_row, axis=0)
        cv2.rectangle(im, (x, y), (x + w1 + 14, y + h1), white, -2)
    for b in init_list_2:
        x, y, w, h = b[0], b[1], b[2] - b[0], b[3] - b[1]
        avg_color_per_row = np.average(
            img[y:y + h - 2, x - 1:x + w + 50], axis=0)
        avg_color = np.average(avg_color_per_row, axis=0)
        cv2.rectangle(im, (x, y), (x + w2 + 16, y + h2 - 5), white, -2)
    for b in init_list_3:
        x, y, w, h = b[0], b[1], b[2] - b[0], b[3] - b[1]
        avg_color_per_row = np.average(
            img[y:y + h - 2, x - 1:x + w + 50], axis=0)
        avg_color = np.average(avg_color_per_row, axis=0)
        cv2.rectangle(im, (x, y), (x + w3 + 16, y + h3 - 5), white, -2)
    return (im)


def undesired_objects(image):
    image = image.astype('uint8')
    nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(
        image, connectivity=4)
    sizes = stats[:, -1]

    max_label = 1
    max_size = sizes[1]
    for i in range(2, nb_components):
        if sizes[i] > max_size:
            max_label = i
            max_size = sizes[i]

    img2 = np.zeros(output.shape)
    img2[output == max_label] = 0
    return img2


def drawLines(img, line_size=5):
    bw = img.copy()
    rows, cols = bw.shape
    """
    for i in range(0,rows-1):
        for j in range(0,cols-1):
            if bw[i,j]==0:
                bw[i,j]=1
    """
    for i in range(0, rows - 1):
        k = 0
        for k in range(0, line_size):
            if j + k > cols - 1 or img[i, j + k] == 255:
                break
        # print(k)
        if k == line_size - 1:
            for k in range(0, line_size):
                if j + k < cols - 1 and img[i, j + k] != 255:
                    bw[i, j + k] = 254
                # elif j+k<cols-1:
                #    bw[i,j+k]=0

    cv2.imwrite('s1.png', bw)
    """
    for i in range(0,rows-1):
        for j in range(0,cols-1):
            k=0
            for k in range(0,line_size+20):
                if i+k>rows-1 or img[i+k,j]==255:
                    break
            # print(k)
            if k==line_size-1:
                for k in range(0,line_size+20):
                    if i+k<rows-1:
                        bw[i+k,j]=255

    cv2.imwrite('s2.png',bw)
     for i in range(0,rows-1):
        for j in range(0,cols-1):
            if bw[i,j]==254 and bw[i,j]!=0:
                bw[i,j]=255
            else:
                bw[i,j]=0

    cv2.imwrite('s2.png',bw)
    """
    for i in range(0, rows - 1):
        for j in range(0, cols - 1):
            if bw[i, j] == 254:
                bw[i, j] = 0
            else:
                bw[i, j] = 255

    cv2.imwrite('s2.png', bw)
    return bw

# def density(x,y,img):


def removeLines_old(img, line_size=20):
    bw = img.copy()
    rows, cols = bw.shape
    for i in range(0, rows - 1):
        for j in range(0, cols - 1):
            k = 0
            for k in range(0, line_size):
                if j + k > cols - 1 or img[i, j + k] == 0:
                    break
            # print(k)
            if k == line_size - 1:
                count = 0
                if i > 5 and i < rows - 5:
                    if img[i - 3, j] == 255:
                        count = count + 1
                    if img[i - 2, j] == 255:
                        count = count + 1
                    if img[i - 1, j] == 255:
                        count = count + 1
                    if img[i + 1, j] == 255:
                        count = count + 1
                    if img[i + 2, j] == 255:
                        count = count + 1
                    if img[i + 3, j] == 255:
                        count = count + 1
                if count < 3:
                    for k in range(0, line_size):
                        if j + k < cols - 1:
                            bw[i, j + k] = 0
    return bw


def connectHorNodes(img, gap=5):
    bw = img.copy()
    rows, cols = bw.shape
    for i in range(0, rows - 1):
        for j in range(5, cols - 1):
            k = 0
            for k in range(0, gap):
                if j + k < cols - 1 and img[i, j + k] == 255:
                    break
            # print(k)
            if k > 2 and k < gap - 1:
                for k in range(0, gap):
                    if j + k < cols - 1:
                        bw[i, j + k] = 255
    return bw


def connectVerNodes(img, gap=4):
    bw = img.copy()
    rows, cols = bw.shape
    for i in range(5, cols - 1):
        for j in range(5, rows - 1):
            k = 0
            c = 0
            for k in range(0, gap):
                if j + k < rows - 1 and img[j + k, i] == 255:
                    c = c + 1
            # print(k)
            if c >= 3:
                for k in range(0, gap):
                    if j + k < rows - 1:
                        bw[j + k, i] = 255
    return bw
# Detect skewness and deskew image


def detect_angle(filename):
    fpth = os.path.splitext(filename)[0] + '_de_skewed.jpg'
    im = cv2.imread(filename)
    angle = pytesseract.image_to_osd(im)

    n = [e.encode('utf-8') for e in angle.strip(':').split('\n')]

    rotate = str(n[1]).split(':')[1]
    rotate = int(rotate.rstrip("'"))
    confidence = str(n[3]).split(':')[1]
    confidence = confidence.replace("'", "")
    print('Rotation {} Degree'.format(rotate))
    print(confidence)
    if float(confidence) < 1.0:
        rotate = 0
    if rotate != 0:
        img_rotated = ndimage.rotate(im, rotate)
        cv2.imwrite(fpth, img_rotated)
        return fpth
    else:
        cv2.imwrite(fpth, im)
        print('FILEPATH', fpth)
        return fpth


def detect_angle_new(filename):
    print(' *** ', filename)
    if filename == '/tmp/dummy.jpg':
        return filename
    fpth = os.path.splitext(filename)[0] + '_de_skewed.jpg'
    im = cv2.imread(filename)
    angle = pytesseract.image_to_osd(im)

    n = [e.encode('utf-8') for e in angle.strip(':').split('\n')]

    rotate = str(n[1]).split(':')[1]
    rotate = int(rotate.rstrip("'"))
    confidence = str(n[3]).split(':')[1]
    confidence = confidence.replace("'", "")

    print(confidence)
    if float(confidence) < 1.0:

        print('\n' * 1, '*** Skew detection with CNN running ***', '\n' * 1)

        rotate = detect_angle_cnn(filename)
    else:
        print('\n' * 1, '*** Skew detection with Tesseract running ***', '\n' * 1)
        print('\n' * 1)
    print ('Rotation {} Degree'.format(rotate))
    img_rotated = ndimage.rotate(im, rotate)
    cv2.imwrite(fpth, img_rotated)
    print('\n' * 1, '*** Deskew done ***''\n' * 1)
    return fpth

# loaded_model=None


def detect_angle_cnn(filename):
    # global  loaded_model
    print('^^^^^^^^', filename)
    if filename == '/tmp/dummy.jpg':
        return filename
    graph1 = Graph()
    with graph1.as_default():
        session1 = Session()
        with session1.as_default():
            json_file = open(model_json_path, 'r')
            loaded_model_json = json_file.read()
            json_file.close()

            loaded_model = model_from_json(loaded_model_json)
            # load woeights into new model
            loaded_model.load_weights(model_path)
            print("*****Loaded Model from disk******")
            fpth = os.path.splitext(filename)[0] + '_de_skewed.jpg'

            im = cv2.imread(filename)

            x = imresize(im, (64, 64))
            x = x.reshape(1, 64, 64, 3)
            out = loaded_model.predict(x)
            angle = dict_normal[np.argmax(out, axis=1)[0]]
            del loaded_model
            gc.collect()
            print ('Rotation {} Degree'.format(angle))
    #         img_rotated = ndimage.rotate(im, int(angle))
    #         cv2.imwrite(fpth, img_rotated)
    # print('FPATH: ', fpth)
    # print('***Deskew done***')
    return angle


def getLists_new(filePath):
    # filePath = detect_angle(filePath)
    bw, bw_org = process_test_image(filePath)
    # cv2.imwrite('/home/vishnu/Desktop/bw.jpg', cv2.imread(bw))
    img_ = cv2.imread(bw_org)
    img = Image.open(bw)
    img = img.point(lambda p: p > 190 and 255)
    img = img.convert('1')
    t1 = time.time()
    (labels, output_img) = run(img)
    print('{} For run script'.format(time.time() - t1))
    # output_img.save('/home/vishnu/Desktop/00eeee.jpg')
    uniq_vals = list(set(labels.values()))
    # print('UNIQUE VALS LENGTH: ', len(uniq_vals))
    cood_vals = []
    # tnp_v = []
    t2 = time.time()

    # for k, v in labels.items():
    #     print(k, v)
    #     break
    dff = pd.DataFrame.from_dict(labels, orient='index').reset_index()
    dff = dff.groupby([0])['index'].apply(list).reset_index()
    for ind, row in dff.iterrows():
        min_x, min_y, max_x, max_y = req_coods(row['index'])
        area = (max_x - min_x) * (max_y - min_y)
        if area > 6 and area < 2000:
            cood_vals.append([min_x, min_y, max_x, max_y])
    # print(len(cood_vals), cood_vals[0:3])
    # for i in range(len(uniq_vals)):
    #     coods = [k for k, v in labels.items() if v == uniq_vals[i]]
    #     # print(coods)
    #     min_x, min_y, max_x, max_y = req_coods(coods)
    #     # tnp_v.append([min_x, min_y, max_x, max_y])
    #     area = (max_x - min_x) * (max_y - min_y)

    #     if area > 6 and area < 2000:
    #         cood_vals.append([min_x, min_y, max_x, max_y])
    # print(tnp_v)
    # print(coods)

    # with concurrent.futures.ProcessPoolExecutor(max_workers=4) as executor:
    #     ocr_ = []
    #     for img_path, out_file in zip(image_list, executor.map(ocr, image_list)):
    #         ocr_.append(out_file)
    # Write boxes

    # org_img = cv2.imread(filePath)
    # for b in cood_vals:
    #     tx, ty, bx, by = b
    #     cv2.rectangle(org_img, (tx, ty), (bx, by), [0, 0, 255], 3)
    # cv2.imwrite('/home/vishnu/projects/ip_ocr_engine/boxes2.jpg', org_img)

    print('{}For coods append'.format(time.time() - t2))
    img_ = cv2.imread(filePath)
    org = img_.copy()
    mask = np.zeros(img_.shape[:2], dtype=np.uint8)
    mask_2 = box_with_tess(filePath)

    for b in cood_vals:
        tx, ty, bx, by = b
        if (by - ty) * (bx - tx) <= 300:
            cv2.rectangle(mask, (tx - 3, ty - 2), (bx + 5, by + 2),
                          [255, 255, 255], -1)
        else:

            cv2.rectangle(mask, (tx - 2, ty - 2), (bx + 5, by + 2),
                          [255, 255, 255], -1)
    # cv2.imwrite('/home/vishnu/Desktop/mask.jpg', mask)
    res = mask + mask_2
    out, boxes = get_bounding_box(res, img_)
    box_name = os.path.splitext(filePath)[0] + '_boxes.jpg'
    cv2.imwrite(box_name, out)
    # cv2.imwrite('/home/vishnu/Desktop/op2.jpg', out)
    box_grouped, __ = cv2.groupRectangles(np.array(boxes).tolist(), 0, 0)
    box_grouped = box_grouped.tolist()
    new_box = []
    for box in box_grouped:
        x, y, w, h = box
    #         w = w - x - 2
    #         h = h - y - 2
        mx = int((x + (w / 2)))
        my = int((y + (h / 2)))
        new_box.append([x, y, w, h, mx, my])
    sorted_box_grouped = sorted(new_box, key=functools.cmp_to_key(cordSort))
    os.remove(bw)
    return sorted_box_grouped, org

# Contour based approach


def getLists_old(filePath):
    rgb = cv2.imread(filePath)
    # rgb = remove_buttons(rgb, t1, t2, t3)
    # cv2.imwrite('/home/vishnu/RCMB/DATA/templates/image.jpg', rgb)
    im = rgb.copy()
    ih, iw, __ = rgb.shape
    hasText = 0
    gray = cv2.cvtColor(rgb, cv2.COLOR_BGR2GRAY)
    morphKernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    grad = cv2.morphologyEx(gray, cv2.MORPH_GRADIENT, morphKernel)
    __, bw = cv2.threshold(grad, 0.0, 255.0, cv2.THRESH_BINARY |
                           cv2.THRESH_OTSU)
    bw1 = removeLines(bw)
    morphKernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4, 4))
    bw1 = cv2.morphologyEx(np.array(bw1), cv2.MORPH_CLOSE, morphKernel)
    splitEelements(bw, bw1)
    bw1 = cv2.morphologyEx(bw1, cv2.MORPH_CLOSE, morphKernel)
    splitEelements(bw, bw1)
    kernel = np.ones((3, 3), np.uint8)
    connected = cv2.dilate(bw, kernel, iterations=1)  # dilate
    kernel = np.ones((1, 1), np.uint8)
    connected = cv2.morphologyEx(bw1, cv2.MORPH_OPEN, kernel)
    __, contours, hierarchy = cv2.findContours(
        connected, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    idx = 0
    i = 0
    ocrtext = ""
    finalText = ""
    bboxes_list = list()
    merge_box = []
    heights = list()
    while idx >= 0:
        x, y, w, h = cv2.boundingRect(contours[idx])
        if (h < ih - 5 and w < iw - 5):
            cv2.rectangle(im, (x, y), (x + w, y + h), (0, 255, 0), 1)
            mx = int((x + (w / 2)))
            my = int((y + (h / 2)))
            bboxes_list.append([x, y, w, h])  # ,mx,my])
            # ,mx,my])
            merge_box.append([x - 1, y - 1, x + w + 1, y + h + 1])
            # merge_box.append([x, y, x + w, y + h])
            heights.append(h)
            hasText = 1
        idx = hierarchy[0][idx][0]
    heights = sorted(heights)
    bboxes_list, __ = cv2.groupRectangles(
        np.array(merge_box).tolist(), 0, 0)
    new_box = []
    for box in bboxes_list:
        x, y, w, h = box
        # w = w - x - 2
        # h = h - y - 2
        mx = int((x + (w / 2)))
        my = int((y + (h / 2)))
        new_box.append([x, y, w, h, mx, my])
    bboxes_list = sorted(new_box, key=functools.cmp_to_key(cordSort))
    return bboxes_list, rgb


def process_ocr(box, rgb, i, crop_path):
    top, bottom, left, right = [50] * 4
    color = [255, 255, 255]
    # print(box)
    x, y = box[:2]
    w, h = box[2] - box[0], box[3] - box[1]
    mx, my = box[4:6]
    if y - 2 != 0 and x - 2 != 0:
        roi = rgb[y - 2:y + h + 2, x - 2:x + w + 2]
    else:
        roi = rgb[y:y + h, x:x + w]
    if roi.shape[0] != 0 and roi.shape[1] != 0:
        cr_name = str(i) + '_' + str(x) + '_' + \
            str(y) + '_' + str(w) + '_' + str(h)
        roi = cv2.pyrUp(roi)
        roi = cv2.copyMakeBorder(
            roi, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)
        roi = Image.fromarray(roi)
        roi = roi.filter(ImageFilter.SHARPEN)
        # roi = ImageEnhance.Brightness(roi).enhance(1.5)
        roi = change_contrast(roi, 25)

        # roi = roi.convert("LA")

        roi.save(crop_path +
                 cr_name + '.jpg', dpi=(300.0, 300.0))

# def group(sequence, chunk_size):
#     list_1 = list(zip(*[iter(sequence)] * chunk_size))
#     tmp = len(sequence)%chunk_size
#     list_2 = sequence[len(sequence)-tmp:len(sequence)]
#     tmp2 = []
#     tmp2.append(list_2)
#     return list_1 + tmp2


def group(alist, wanted_parts=1):
    length = len(alist)
    return [alist[i * length // wanted_parts: (i + 1) * length // wanted_parts]
            for i in range(wanted_parts)]


def change_contrast(img, level):
    factor = (259 * (level + 255)) / (255 * (259 - level))

    def contrast(c):
        return 128 + factor * (c - 128)
    return img.point(contrast)


def get_conf_score(DIR_PATH):

    parse_error_flag = False
    image = cv2.imread(DIR_PATH)
    OUTPUT_FILE = os.path.splitext(DIR_PATH)[0] + '_conf'
    func_tesseract = 'tesseract ' + DIR_PATH + " " + \
        OUTPUT_FILE + ' --oem 1 -l eng --psm 6 tsv'
    # os.system(func_tesseract)
    subprocess.check_output(func_tesseract, shell=True)
    f_df = pd.DataFrame()
    # tsv_file = OUTPUT_FILE + '.tsv'
    # csv_table = pd.read_table(tsv_file, sep='\t')
    # csv_table.to_csv(OUTPUT_FILE + '.csv')
    OUTPUT_FILE = os.path.join('./', OUTPUT_FILE)
    main_file = OUTPUT_FILE + '.tsv'
    try:
        # raise pd.errors.ParserError
        df = pd.read_csv(main_file, sep="\t", error_bad_lines=False)

    except pd.errors.ParserError:
        # elif 1:
        print('*******Handling parse error********')
        parse_error_flag = True
        with open(main_file, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            list_reader = list(reader)
            main_list = [list(dict(row).values())[0].split('\t')
                         for row in list_reader]
            columns = list(dict(list_reader[0]).keys())[0].split('\t')
            df = pd.DataFrame(main_list, columns=columns)
    # print('DF_EROOR', df.head())
    # print(df.head())
    # df = pd.read_csv(OUTPUT_FILE + '.tsv', sep='\t', error_bad_lines=False, engine = 'python')
    df1 = df.dropna()
    conf_df = pd.concat([df1, f_df], ignore_index=True)
    # os.remove(file)
    conf_df = conf_df.rename(
        index=str, columns={"left": "tl_x", "top": "tl_y", "text": "Text"})
    conf_df['br_x'] = conf_df['tl_x'] + conf_df['width']
    conf_df['br_y'] = conf_df['tl_y'] + conf_df['height']
    offset_features = ['tl_x', 'tl_y', 'br_x', 'br_y', 'width', 'height']
    req_cols = offset_features + ['Text']
    conf_df[offset_features] = conf_df[offset_features].astype(float)
    new_df = conf_df[req_cols]
    new_df['word_length'] = new_df['Text'].apply(lambda x: len(x))
    new_df['index'] = list(range(len(new_df)))
    finalText = ' '.join(list(new_df['Text'].values))
    json_out = json.loads(new_df.to_json(orient='table', index=False))
    json_out['raw_text'] = finalText
    # print(new_df.head())

    return json_out, new_df, parse_error_flag


def draw_boxes(df, img_path):
    coordinates = []
    img = cv2.imread(img_path)
    org = img.copy()
    out = os.path.splitext(img_path)[0] + '_boxes.jpg'
    for (i, row) in df.iterrows():

        tl_x = row['tl_x']
        tl_y = row['tl_y']
        br_x = row['br_x']
        br_y = row['br_y']
        mx = (int(br_x) - int(tl_x)) / 2
        my = (int(br_y) - int(tl_y)) / 2
        coordinates.append([int(tl_x), int(tl_y),
                            int(br_x), int(br_y), int(mx), int(my)])
        cv2.rectangle(img, (int(tl_x), int(tl_y)),
                      (int(br_x), int(br_y)), (0, 255, 0), 2)
    cv2.imwrite(out, img)
    return(out, coordinates, org)


def tesseract_only(filepath):
    # filepath = detect_angle(filepath)
    json_out, final_df, parse_error_flag = get_conf_score(filepath)
    s3_path_org = None
    # Write image boxes got from tesseract
    out, coordinates, org = draw_boxes(final_df, filepath)
    if parse_error_flag:
        uniq_org_name = (str(datetime.datetime.now())
                         ).replace(' ', '_') + '_orgImage'
        s3_path_org, local_path_org = dump_to_s3(org, uniq_org_name)
        print('Original Image_Uploaded to: ', s3_path_org)
    # print(coordinates)
    return json_out, final_df, coordinates, org, parse_error_flag, s3_path_org


def post_process(filepath, json_out):
    name = str(uuid.uuid4().fields[-1])[:5]
    crop_path = './crop_files/'
    # dir_path = crop_path + \
    #     str(name) + '/'
    check_dir_exist(crop_path)
    f_name = crop_path + 'img.jpg'
    json_path = crop_path + 'data.json'
    rgb = cv2.imread(filepath)
    cv2.imwrite(f_name, rgb)
    with open(json_path, 'w') as outfile:
        json.dump(json_out, outfile)

    ocr_df = run_post_processing_engine(json_path)
    # print('RESPONSE**********', ocr_df)
    # print(response.text)
    # res_data = json.loads(response.content)
    res_data = json.loads(ocr_df.to_json(orient='table'))
    with open(json_path, 'w') as outfile:
        json.dump(res_data, outfile)
    Text = []
    new_text = []
#    print (res_data['data'])
    for index in res_data['data']:
        Text.append(index['Text'])
        new_text.append(index['New_text'])
    res_data['Text'] = ' '.join(Text)
    res_data['New_text'] = ' '.join(new_text)
    return res_data

def process_ocr_v2(box, rgb):
    top, bottom, left, right = [50] * 4
    color = [255, 255, 255]
    # print(box)
    x, y = box[:2]
    w, h = box[2] - box[0], box[3] - box[1]
    mx, my = box[4:6]
    if y - 2 != 0 and x - 2 != 0:
        roi = rgb[y - 2:y + h + 2, x - 2:x + w + 2]
    else:
        roi = rgb[y:y + h, x:x + w]
    if roi.shape[0] != 0 and roi.shape[1] != 0:
        # cr_name = str(i) + '_' + str(x) + '_' + \
        #     str(y) + '_' + str(w) + '_' + str(h)
        roi = cv2.pyrUp(roi)
        roi = cv2.copyMakeBorder(
            roi, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)
        roi = Image.fromarray(roi)
        roi = roi.filter(ImageFilter.SHARPEN)
        # roi = ImageEnhance.Brightness(roi).enhance(1.5)
        roi = change_contrast(roi, 25)
    return np.array(roi)
        # roi = roi.convert("LA")

        # roi.save(crop_path +
        #          cr_name + '.jpg', dpi=(300.0, 300.0))


def process_all_patch(listt,rgb,dir_path):
    res_list = []
    for ind, val in enumerate(listt):
         patch_img = process_ocr_v2(val, rgb, ind, dir_path)
         res_list.append(patch_img)
    yield res_list

def extract_text_from_patch(listt, rgb, delete_flag=False, vallidation=False):

    if os.environ['NODE_ENV'] == 'dev2':
        delete_flag = False
    print('No of boxes', len(listt))
    name = str(uuid.uuid4().fields[-1])[:5]
    crop_path = './crop_files/'
    dir_path = crop_path + \
        str(name) + '/'
    check_dir_exist(dir_path)
    for ind, val in enumerate(listt):
        process_ocr(val, rgb, ind, dir_path)
    if vallidation == True:

        model_file = glob.glob(
            './model_files/*.hdf5')

        model = load_model(model_file[0])
        text_extract_with_validation(dir_path, model)
        del model
        gc.collect()
    else:
        text_extract(dir_path)
    final_json, df, local_file_path = get_final_json(dir_path, rgb)
    box_image = cv2.imread(local_file_path)
    uniq_box_name = (str(datetime.datetime.now())).replace(' ', '_')
    uniq_org_name = (str(datetime.datetime.now())
                     ).replace(' ', '_') + '_orgImage'
    s3_path_box, local_path_box = dump_to_s3(box_image, uniq_box_name)
    s3_path_org, local_path_org = dump_to_s3(rgb, uniq_org_name)
    print('Result Image_Uploaded to: ', s3_path_box)
    print('Original Image_Uploaded to: ', s3_path_org)
    if delete_flag is True:
        try:
            shutil.rmtree(crop_path)
        except OSError as e:
            print ("Error: %s - %s." % (e.filename, e.strerror))
    # shutil.rmtree(local_path)
    return final_json, df, s3_path_box, s3_path_org
    # return final_json, df




def extract_text_from_patch_all_in_memory(listt, rgb, delete_flag=False, vallidation=False,write_flag=False):

    if os.environ['NODE_ENV'] == 'dev2':
        delete_flag = False
    print('No of boxes', len(listt))
    name = str(uuid.uuid4().fields[-1])[:5]
    crop_path = './crop_files/'
    dir_path = crop_path + \
        str(name) + '/'
    check_dir_exist(dir_path)

    all_patch_list = text_extract_v2(listt, [rgb,dir_path])
    coords_pred = next(all_patch_list)
    final_json, df, local_file_path = get_final_json_v2(coords_pred,dir_path, rgb,write_flag)
    box_image = cv2.imread(local_file_path)
    uniq_box_name = (str(datetime.datetime.now())).replace(' ', '_')
    uniq_org_name = (str(datetime.datetime.now())
                     ).replace(' ', '_') + '_orgImage'
    s3_path_box, local_path_box = dump_to_s3(box_image, uniq_box_name)
    s3_path_org, local_path_org = dump_to_s3(rgb, uniq_org_name)
    print('Result Image_Uploaded to: ', s3_path_box)
    print('Original Image_Uploaded to: ', s3_path_org)
    if delete_flag is True:
        try:
            shutil.rmtree(crop_path)
        except OSError as e:
            print ("Error: %s - %s." % (e.filename, e.strerror))
    # shutil.rmtree(local_path)
    return final_json, df, s3_path_box, s3_path_org
    # return final_json, df





def combined_bounding_box_extraction_method(img_path):
    contour_ext_val = 6
    draw_flag = True
    all_tess_boxes = []
    curr_loc = os.getcwd()

    final_path, coor_list = main(img_path)

    img = cv2.imread(img_path)
    # img=cv2.resize(img,(480,640))
    # for c in coor_list:
    # cv2.rectangle(img,(int(c[0]),int(c[1])),(int(c[2]),int(c[3])),(0,255,0),3)
    # img_to_draw=cv2.imread(final_path)
    h, w, __ = img.shape
    if len(coor_list) > 0:

        letters = pytesseract.image_to_boxes(img, config=' --oem 1')
        letters = letters.split('\n')
        letters = [letter.split() for letter in letters]

        for letter in letters:
            all_tess_boxes.append(
                [int(letter[1]), h - int(letter[2]), int(letter[3]), h - int(letter[4])])
            # cv2.rectangle(img,(int(letter[1]),h-int(letter[2])),(int(letter[3]),h-int(letter[4])),(0,0,255),3)
        new_blacking = np.zeros((h, w), dtype=np.uint8)
        for coor in coor_list:
            coor[0] = int(coor[0])
            coor[1] = int(coor[1])
            coor[2] = int(coor[2])
            coor[3] = int(coor[3])
            area_crop = (coor[3] - coor[1]) * (coor[2] - coor[0])

            cv2.rectangle(
                new_blacking, (coor[0], coor[1]), (coor[2], coor[3]), (255, 255, 255), -1)
            for ind, t_box in enumerate(all_tess_boxes):
                cv2.rectangle(new_blacking, (t_box[0] - contour_ext_val, t_box[1]),
                              (t_box[2] + contour_ext_val, t_box[3]), (255, 255, 255), -1)

                # cv2.rectangle(new_blacking, (t_box[0], t_box[1]), (t_box[2], t_box[3]), (255,255,255), -1)

        # cv2.imwrite(final_path,new_blacking)
        # final_img = new_blacking
        # print('*****************', final_img.shape)
        kernel = kernel = np.ones((1, 5), np.uint8)
        # final_img = cv2.erode(new_blacking, kernel, iterations=3)
        final_img = cv2.dilate(new_blacking, kernel, iterations=2)

        __, contours, hierarchy = cv2.findContours(
            final_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        all_contour_boxes = []
        for idx in range(len(contours)):
            x, y, w, h = cv2.boundingRect(contours[idx])
            all_contour_boxes.append(
                [x, y, x + w, y + h, int((w + 2 * x) / 2), int((h + 2 * y) / 2)])
        sorted_box_grouped = sorted(
            all_contour_boxes, key=functools.cmp_to_key(cordSort))
        # sorted_box_grouped=sort_boxes(all_contour_boxes)
        cloned_img = img.copy()
        if draw_flag:
            for box in sorted_box_grouped:
                cv2.rectangle(
                    cloned_img, (box[0], box[1]), (box[2], box[3]), (0, 0, 255), 3)
            img_path_to_write = os.path.join(
                '/tmp', str(uuid.uuid4()) + '.jpg')
            cv2.imwrite(img_path_to_write, cloned_img)
        return sorted_box_grouped, img
    else:
        return None, None


def find_score(arr, angle):
    data = inter.rotate(arr, angle, reshape=False, order=0)
    hist = np.sum(data, axis=1)
    score = np.sum((hist[1:] - hist[:-1]) ** 2)
    return hist, score


def skew_correction(input_file):

    img_original = cv2.imread(input_file)
    print('input_filename', input_file)
    img = Image.open(input_file)

    # convert to binary
    wd, ht = img.size
    pix = np.array(img.convert('1').getdata(), np.uint8)
    bin_img = 1 - (pix.reshape((ht, wd)) / 255.0)

    # plt.imshow(bin_img, cmap='gray')
    # plt.savefig('binary.png')

    delta = 1
    limit = 30
    angles = np.arange(-limit, limit + delta, delta)
    scores = []
    # print(angles)
    for angle in angles:
        hist, score = find_score(bin_img, angle)
        scores.append(score)

    best_score = max(scores)
    best_angle = angles[scores.index(best_score)]
    print('Best angle: {}'.format(best_angle))

    # correct skew
    data = inter.rotate(bin_img, best_angle, reshape=False, order=0)
    img = Image.fromarray((255 * data).astype("uint8")).convert("RGB")
    img_new = np.array(img)
    img_new = 255 - img_new
    new_img = ndimage.rotate(img_original, best_angle, cval=255.0)
    cv2.imwrite(input_file, new_img)
    return input_file


def skew_correction_only(fpath):
    skew_corrected = skew_correction(detect_angle_new(fpath))
    return skew_corrected


# src_path = './data/images/'
if __name__ == '__main__':
    sorted_box_grouped, org = getLists_new(
        '/home/vishnu/RCMB/DATA/GEC_Screenshots/GEC_15_patient_list/ALLEN,MELISSA M/ALLEN,MELISSA M.jpg')