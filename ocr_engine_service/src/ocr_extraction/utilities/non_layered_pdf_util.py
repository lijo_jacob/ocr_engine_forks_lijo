import io
import os
import glob
import json
import random
import tempfile
import subprocess
import numpy as np
import pandas as pd
from wand.image import Image
from bs4 import BeautifulSoup
from django.conf import settings
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from sklearn.externals import joblib
from pdf2image import convert_from_path
from pdfminer.pdfinterp import resolve1
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import HTMLConverter, TextConverter, XMLConverter
from common.utils import (
    get_type, log_error, check_dir_exist
)
from common.error_constants import ERR_001

def convert_pdf_img(pdf_file):
    with Image(filename=pdf_file, resolution=300) as img:
        # img.compression_quality = 99
        images = img.sequence
        pages = len(images)
        print("page length is  ",pages)
        filename_list = []
        temp_path = get_random_path()
        for i in range(pages):
            filename = os.path.join(temp_path, str(i) + '.jpg')
            print("generate file name is  ", filename)
            Image(images[i]).save(filename=filename)
            filename_list.append(filename)
        print("file name list ",filename_list)
        return filename_list
def get_random_path():
    rand_name = "%0.12d" % random.randint(0, 999999999999)
    new_path = os.path.join(os.environ["MEDIA_ROOT"], 'tmp',
                            'pdf_image_tmp', rand_name)
    check_dir_exist(new_path)
    return new_path