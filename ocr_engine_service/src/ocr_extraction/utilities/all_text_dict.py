import os
import cv2
import glob
import time
import json
import datetime
import shutil
import numpy as np
import pandas as pd
from sortedcontainers import SortedDict
# from .ocr_utils_multi_proc import *
from .s3_upload import connect_to_s3, upload_to_s3
from django.conf import settings


# path = '/home/vishnu/projects/ip_ocr_engine/crop_files/803632107359659/'
offset_features = ['tl_x', 'tl_y', 'width', 'height', ]
all_columns = ['Text', 'index'] + offset_features


def sortKeyFunc(s):
    return float(os.path.splitext(os.path.basename(s))[0].split('_')[0])







def get_final_json_v2(coords,direc_path, rgb,write_flag = False):
    # subdirectory = direc_path.replace('.', ' ')
    # print('DIREC-PATH', os.path.basename(direc_path), direc_path)
    uniq_path = os.path.splitext(direc_path)[0].split('/')[2]
    unique_counter = 0
    # rgb = cv2.pyrUp(rgb)
    image = rgb.copy()
    # __, image = cv2.threshold(image, 0.0, 255.0, cv2.THRESH_BINARY_INV |
    #                           cv2.THRESH_OTSU)

    # ref = cv2.copyMakeBorder(image, 15, 15, 15, 15,
    #                          cv2.BORDER_CONSTANT, value=[0, 0, 0])
    ref = image
    img_file_name = ''
    txt_list = coords #sorted(glob.glob(direc_path + '*.txt', recursive=True))
    # txt_list.sort(key=sortKeyFunc)
    # print('^^^^^^^^^^^^^^^^^^^^^^^^^^^',txt_list)
    final_dict = {}
    name_list = []
    for ind,txt_path in enumerate(txt_list):
        # print(txt_path)
        # f_name = os.path.splitext(os.path.basename(txt_path))[0]
        all_features = ind,txt_path[0],txt_path[1],txt_path[2],txt_path[3]
        if all_features[0] == unique_counter:
            new_fname = str(all_features[0]) + '_' + str(all_features[1]) + '_' + \
                        str(all_features[2]) + '_' + \
                        str(all_features[3]) + '_' + str(all_features[4])
            # txt_file_name = txt_path.replace(f_name, new_fname)
            img_file_name = os.path.join(direc_path, new_fname + '.jpg')

        else:
            if '.' in all_features[0]:

                all_features[0] = unique_counter
                new_fname = str(all_features[0]) + '_' + str(all_features[1]) + '_' + \
                            str(all_features[2]) + '_' + \
                            str(all_features[3]) + '_' + str(all_features[4])
                # txt_file_name = txt_path.replace(f_name, new_fname)
                img_file_name = os.path.join(direc_path,new_fname + '.jpg')

            else:
                all_features[0] = unique_counter
                new_fname = str(all_features[0]) + '_' + str(all_features[1]) + '_' + \
                            str(all_features[2]) + '_' + \
                            str(all_features[3]) + '_' + str(all_features[4])
                img_file_name =  os.path.join(direc_path,new_fname + '.jpg')
                # print('Renaming>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', txt_path, txt_file_name)
        # print('Path', img_file_name, txt_path[-1])
        # print(all_features)
        if write_flag:
            img_patch = rgb[int(all_features[2]): int(all_features[2]+all_features[4]),int(all_features[1]):int(all_features[1] + all_features[3])]

            cv2.imwrite(img_file_name,img_patch)
        name_list.append(os.path.join(settings.S3_BUCKET_NAME,
                                      img_file_name.replace('./', '')))

        unique_counter += 1
        final_dict[int(all_features[0])] = [txt_path[-1]] + [all_features[0]]  +[all_features[1]]  + [all_features[2]]  \
                                           + [all_features[3]]  + [all_features[4]]

    # print('************IMAGE_FILE_NAMe************', name_list)
    df_new = pd.DataFrame({'patch_path': name_list})
    df_new = df_new.astype(str)
    # print(df_new)
    myDict = SortedDict(final_dict)
    # print('MYDICT', myDict)
    df = pd.DataFrame.from_dict(myDict, orient='index', columns=all_columns)
    # print('$$$$$$$$$$$$$$$$$$$$$$$$', df.head())
    df[offset_features] = df[offset_features].astype(float)
    df['br_x'] = df['tl_x'] + df['width']
    df['br_y'] = df['tl_y'] + df['height']
    df['word_length'] = df['Text'].apply(lambda x: len(x))
    df['patch_path'] = df_new
    mask = np.zeros(rgb.shape[:2], dtype=np.uint8)
    font = cv2.FONT_HERSHEY_PLAIN
    # font = cv2.FONT_HERSHEY_SIMPLEX
    for ind, row in df.iterrows():
        cv2.rectangle(ref, (int(row['tl_x']), int(
            row['tl_y'] - 2)), (int(row['br_x']), int(row['br_y'] + 2)), (255, 0, 0), 2)
        cv2.putText(ref, str(row['Text']), (int(row['tl_x']), int(
            row['tl_y'] - 1)), font, 1, (0, 0, 255), 2, cv2.LINE_AA)
    # __, bw = cv2.threshold(ref, 0.0, 255.0, cv2.THRESH_BINARY_INV |
    #                        cv2.THRESH_OTSU)
    uniq_filename = (str(datetime.datetime.now())).replace(' ', '_')
    name = direc_path + uniq_filename + '.jpg'
    cv2.imwrite(name, ref)
    cv2.imwrite(direc_path + 'original_image.jpg', rgb)

    # Upload crop files to S3
    if write_flag:
        files = glob.glob(direc_path + '*.jpg')
        connect_to_s3(os.environ['S3_AWS_KEY'], os.environ['S3_AWS_SECRET'],
                      settings.S3_BUCKET_NAME, subdirectory='crop_files/' + uniq_path)
        [upload_to_s3(f, os.path.basename(f)) for f in files]
    # input()
    # df['Data_type'] = df['Text'].apply(get_type)
    # df['Data_type'] = df['Data_type'].map(type_dict)
    # df.to_csv('/home/vishnu/Desktop/sample.csv', encoding='utf-8', index=False)
    # with pd.option_context('display.max_rows', None, 'display.max_columns', None):
    # print(df['Text'])
    # print(df.head())
    # with pd.option_context('display.max_rows', None, 'display.max_columns', None):
    #     print(df)
    finalText = ' '.join(list(df['Text'].values))
    json_out = json.loads(df.to_json(orient='table', index=False))
    json_out['raw_text'] = finalText
    # print(json_out['raw_text'])
    return json_out, df, name












def get_final_json(direc_path, rgb):
    subdirectory = direc_path.replace('.', ' ')
    # print('DIREC-PATH', os.path.basename(direc_path), direc_path)
    uniq_path = os.path.splitext(direc_path)[0].split('/')[2]
    unique_counter = 0
    # rgb = cv2.pyrUp(rgb)
    image = rgb.copy()
    # __, image = cv2.threshold(image, 0.0, 255.0, cv2.THRESH_BINARY_INV |
    #                           cv2.THRESH_OTSU)

    ref = cv2.copyMakeBorder(image, 15, 15, 15, 15,
                             cv2.BORDER_CONSTANT, value=[0, 0, 0])
    ref = image
    txt_list = sorted(glob.glob(direc_path + '*.txt', recursive=True))
    txt_list.sort(key=sortKeyFunc)
    # print('^^^^^^^^^^^^^^^^^^^^^^^^^^^',txt_list)
    final_dict = {}
    name_list = []
    for txt_path in txt_list:
        f_name = os.path.splitext(os.path.basename(txt_path))[0]
        all_features = f_name.split('_')
        if all_features[0] == unique_counter:

            pass
        else:
            if '.' in all_features[0]:

                all_features[0] = unique_counter
                new_fname = str(all_features[0]) + '_' + all_features[1] + '_' + \
                    all_features[2] + '_' + \
                    all_features[3] + '_' + all_features[4]
                txt_file_name = txt_path.replace(f_name, new_fname)
                img_file_name = txt_path.replace(
                    f_name + '.txt', new_fname + '.jpg')
                # print('Renaming>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>',txt_path,txt_file_name)
                os.rename(txt_path, txt_file_name)
                os.rename(txt_path.replace('.txt', '.jpg'), img_file_name)
                txt_path = txt_file_name
            else:
                all_features[0] = unique_counter
                new_fname = str(all_features[0]) + '_' + all_features[1] + '_' + all_features[2] + '_' + all_features[
                    3] + '_' + all_features[4]
                txt_file_name = txt_path.replace(f_name, new_fname)
                img_file_name = txt_path.replace(
                    f_name + '.txt', new_fname + '.jpg')
                # print('Renaming>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', txt_path, txt_file_name)
                os.rename(txt_path, txt_file_name)
                os.rename(txt_path.replace('.txt', '.jpg'), img_file_name)
                txt_path = txt_file_name

        name_list.append(os.path.join(settings.S3_BUCKET_NAME,
                                      img_file_name.replace('./', '')))

        unique_counter += 1
        final_dict[int(all_features[0])] = [open(
            txt_path, 'r').read()] + all_features
    # print('************IMAGE_FILE_NAMe************', name_list)
    df_new = pd.DataFrame({'patch_path': name_list})
    df_new = df_new.astype(str)
    # print(df_new)
    myDict = SortedDict(final_dict)
    # print('MYDICT', myDict)
    df = pd.DataFrame.from_dict(myDict, orient='index', columns=all_columns)
    # print('$$$$$$$$$$$$$$$$$$$$$$$$', df.head())
    df[offset_features] = df[offset_features].astype(float)
    df['br_x'] = df['tl_x'] + df['width']
    df['br_y'] = df['tl_y'] + df['height']
    df['word_length'] = df['Text'].apply(lambda x: len(x))
    df['patch_path'] = df_new
    mask = np.zeros(rgb.shape[:2], dtype=np.uint8)
    font = cv2.FONT_HERSHEY_PLAIN
    # font = cv2.FONT_HERSHEY_SIMPLEX
    for ind, row in df.iterrows():
        cv2.rectangle(ref, (int(row['tl_x']), int(
            row['tl_y'] - 2)), (int(row['br_x']), int(row['br_y'] + 2)), (255, 0, 0), 2)
        cv2.putText(ref, str(row['Text']), (int(row['tl_x']), int(
            row['tl_y'] - 1)), font, 1, (0, 0, 255), 2, cv2.LINE_AA)
    # __, bw = cv2.threshold(ref, 0.0, 255.0, cv2.THRESH_BINARY_INV |
    #                        cv2.THRESH_OTSU)
    uniq_filename = (str(datetime.datetime.now())).replace(' ', '_')
    name = direc_path + uniq_filename + '.jpg'
    cv2.imwrite(name, ref)
    cv2.imwrite(direc_path + 'original_image.jpg', rgb)

    # Upload crop files to S3
    # files = glob.glob(direc_path + '*.jpg')
    # connect_to_s3(os.environ['S3_AWS_KEY'], os.environ['S3_AWS_SECRET'],
    #               settings.S3_BUCKET_NAME, subdirectory='crop_files/' + uniq_path)
    #[upload_to_s3(f, os.path.basename(f)) for f in files]
    # input()
    # df['Data_type'] = df['Text'].apply(get_type)
    # df['Data_type'] = df['Data_type'].map(type_dict)
    # df.to_csv('/home/vishnu/Desktop/sample.csv', encoding='utf-8', index=False)
    # with pd.option_context('display.max_rows', None, 'display.max_columns', None):
    # print(df['Text'])
    # print(df.head())
    # with pd.option_context('display.max_rows', None, 'display.max_columns', None):
    #     print(df)
    finalText = ' '.join(list(df['Text'].values))
    json_out = json.loads(df.to_json(orient='table', index=False))
    json_out['raw_text'] = finalText
    # print(json_out['raw_text'])
    return json_out, df, name