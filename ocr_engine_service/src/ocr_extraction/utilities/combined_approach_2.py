from .EAST.run_demo_server import main
import cv2
import numpy as np
import time
import os
from uuid import uuid4
import functools
from PIL import Image
import pytesseract

debug_mode = False
backpass=False


def cordSort(box1, box2):
    if abs(box1[5] - box2[5]) < 5:
        if box1[4] > box2[4]:
            return 1
        elif box1[4] < box2[4]:
            return -1
    else:
        if box1[5] > box2[5]:
            return 1
        elif box1[5] < box2[5]:
            return -1
    return 0


# old_w- width of image before opration
# new_w=width of image after any operation
# x,y,u =x+w, v=y+h are the four points


def scale_down(x, y, patch_w, patch_h, old_w, old_h, new_w, new_h):
    oldu1 = x + patch_w
    oldv1 = y + patch_h

    newx = int(np.round((x / old_w) * new_w))
    newy = int(np.round((y / old_h) * new_h))
    newu = int(np.round((oldu1 / old_w) * new_w))
    newv = int(np.round((oldv1 / old_h) * new_h))

    return newx, newy, newu, newv


def scale_down_v2(img, coor_list):
    print('scaling down...')
    # img=cv2.pyrUp(img)
    height, width, _ = img.shape
    img_p_down = cv2.pyrDown(img)
    height_new, width_new, _ = img_p_down.shape
    final_list = []
    for coor in coor_list:
        x1 = int((coor[0] / width) * width_new)
        y1 = int((coor[1] / height) * height_new)
        wd = coor[2] - coor[0]
        ht = coor[3] - coor[1]
        new_wd = int((wd / width) * width_new)
        new_ht = int((ht / height) * height_new)
        final_list.append([x1, y1, x1 + new_wd, y1 + new_ht,
                           int((new_wd + 2 * x1) / 2), int((new_ht + 2 * y1) / 2)])
        # break
    return final_list, img
    # img_p_down=cv2.pyrDown(img)


def non_max_suppression_fast(boxes, overlapThresh):
    # if there are no boxes, return an empty list
    if len(boxes) == 0:
        return []

    # if the bounding boxes integers, convert them to floats --
    # this is important since we'll be doing a bunch of divisions
    if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")

    # initialize the list of picked indexes
    pick = []

    # grab the coordinates of the bounding boxes
    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]

    # compute the area of the bounding boxes and sort the bounding
    # boxes by the bottom-right y-coordinate of the bounding box
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(y2)

    # keep looping while some indexes still remain in the indexes
    # list
    while len(idxs) > 0:
        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)

        # find the largest (x, y) coordinates for the start of
        # the bounding box and the smallest (x, y) coordinates
        # for the end of the bounding box
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])

        # compute the width and height of the bounding box
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)

        # compute the ratio of overlap
        overlap = (w * h) / area[idxs[:last]]

        # delete all indexes from the index list that have
        idxs = np.delete(idxs, np.concatenate(([last],
                                               np.where(overlap > overlapThresh)[0])))

    # return only the bounding boxes that were picked using the
    # integer data type
    return boxes[pick].astype("int")


def get_iou(bb1, bb2):
    """
    Calculate the Intersection over Union (IoU) of two bounding boxes.

    Parameters
    ----------
    bb1 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x1, y1) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner
    bb2 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x, y) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner

    Returns
    -------
    float
        in [0, 1]
    """
    assert bb1[0] < bb1[2]
    assert bb1[1] < bb1[3]
    assert bb2[0] < bb2[2]
    assert bb2[1] < bb2[3]

    # determine the coordinates of the intersection rectangle
    x_left = max(bb1[2], bb2[2])
    y_top = max(bb1[1], bb2[1])
    x_right = min(bb1[2], bb2[2])
    y_bottom = min(bb1[3], bb2[3])

    if x_right < x_left or y_bottom < y_top:
        return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
    bb1_area = (bb1[2] - bb1[1]) * (bb1[3] - bb1[1])
    bb2_area = (bb2[2] - bb2[0]) * (bb2[3] - bb2[1])

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
    assert iou >= 0.0
    assert iou <= 1.0
    return iou


def draw_on_black(black_canvas, x, y, w, h, padding, val):
    cv2.rectangle(black_canvas, (x - padding, y),
                  (x + w + padding, y + h), (val, val, val), -1)


avg_wd = avg_ht = 0


def mser_method_img(img):
    # print('Entering')
    img_1 = img.copy()
    padding =5
    mser = cv2.MSER_create()
    img_shape=img.shape
    if len(img_shape)>2:
      gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    else:

      gray = img
    regions, _ = mser.detectRegions(gray)
    height, width = gray.shape
    ret, thresh_img = cv2.threshold(gray, 250, 255, cv2.THRESH_BINARY_INV)
    # cv2.imwrite(ret_path,thresh_img)

    black_canvas = np.zeros((height, width), dtype=np.uint8)
    hulls = [cv2.convexHull(p.reshape(-1, 1, 2)) for p in regions]
    h_sum = 0
    w_sum = 0
    a_sum = 0
    all_rects = []
    new_rects = []
    final_rects = []
    for hull in hulls:
        x, y, w, h = cv2.boundingRect(hull)
        all_rects.append([x, y, w, h])
        h_sum += h
        w_sum += w
        a_sum += (h * w)
    # print('Number of mser rects', len(all_rects))
    if len(all_rects) > 0:
        # avg_ht = float(h_sum) / len(all_rects)
        # avg_wd = float(w_sum) / len(all_rects)

        for rect in all_rects:
            x, y, w, h = rect
            patch_binary = thresh_img[y:y + h, x:x + w]
            # n_white_pix = np.sum(patch_binary == 255)

            # print('AVG Ar3rea',avg_area,w*h)
            # if (w > 4 * avg_wd) or (h > 4 * avg_ht):
            #     continue

            final_rects.append([x - padding, y, x + w + padding, y + h])
    # final_rects = non_max_suppression_fast(np.asarray(final_rects), 0.9)

    for rect in final_rects:
        cv2.rectangle(
            black_canvas, (rect[0], rect[1]), (rect[2], rect[3]), (255, 255, 255), -1)
    _, contours, hierarchy = cv2.findContours(
        black_canvas, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        # cv2.rectangle(img_1, (x, y),
        #               (x + w, y + h), (0, 0, 255), 2)

        # if w> 0.5*width:
        #     continue
        new_rects.append([x, y, x + w, y + h])
    # cv2.imshow('RRRR', img_1)
    # cv2.waitKey(0)
    new_rects = non_max_suppression_fast(np.asarray(new_rects), 0.9)

    return new_rects


def mser_method(img_path):
    global avg_wd, avg_ht
    padding = 5
    ret_path = '/tmp/' + str(uuid4()) + '_bin.jpg'
    ret_path_1 = '/tmp/' + str(uuid4()) + '_mser_original.jpg'
    if isinstance(img_path,str):
        img = cv2.imread(img_path)
    else:
        img =img_path
    # img = cv2.imread(img_path)
    img = cv2.pyrUp(img)
    # img = cv2.pyrUp(img)
    img_2 = img.copy()
    mser = cv2.MSER_create(_delta=5, _min_area=60, _max_area=2000, _max_variation=0.25, _min_diversity=.1,
                           _max_evolution=200, _area_threshold=1.01, _min_margin=0.003, _edge_blur_size=5)
    # mser = cv2.MSER_create()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    regions, _ = mser.detectRegions(gray)
    height, width = gray.shape
    ret, thresh_img = cv2.threshold(gray, 250, 255, cv2.THRESH_BINARY_INV)
    # cv2.imwrite(ret_path,thresh_img)

    black_canvas = np.zeros((height, width), dtype=np.uint8)
    hulls = [cv2.convexHull(p.reshape(-1, 1, 2)) for p in regions]
    h_sum = 0
    w_sum = 0
    a_sum = 0
    all_rects = []
    final_rects = []
    for hull in hulls:
        x, y, w, h = cv2.boundingRect(hull)
        all_rects.append([x, y, w, h])
        h_sum += h
        w_sum += w
        a_sum += (h * w)
    # print('Number of mser rects', len(all_rects))
    if len(all_rects) > 0:
        avg_ht = float(h_sum) / len(all_rects)
        avg_wd = float(w_sum) / len(all_rects)

        for rect in all_rects:
            x, y, w, h = rect
            patch_binary = thresh_img[y:y + h, x:x + w]
            n_white_pix = np.sum(patch_binary == 255)

            # print('AVG Ar3rea',avg_area,w*h)
            if (w > 4 * avg_wd) or (h > 4 * avg_ht):
                continue
            if n_white_pix < 0.45 * (w * h) and n_white_pix > 0.9 * (w * h):
                continue
            final_rects.append([x - padding, y, x + w + padding, y + h])

            cv2.rectangle(img_2, (x - padding, y),
                          (x + w + padding, y + h), (0, 0, 255), 2)
        if debug_mode == True:
            cv2.imwrite(ret_path_1, img_2)
        final_rects = non_max_suppression_fast(np.asarray(final_rects), 0.9)
    return final_rects


def ocr_v1(img, model=None):
    # model, label_encoder, integer_encoded = grab_model(model)
    # print('TEST**************')
    # img = cv2.imread(img_path)
    # print(model.summary())
    # preds = do_Prediction(img_path, model,
    # label_encoder, integer_encoded)
    # inp_img = Image.open(img_path)
    # output_img = pillowfight.ace(inp_img)
    output_img = Image.fromarray(img)
    gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # blur = cv2.medianBlur(gray_image, 3)
    roi = img
    text1 = pytesseract.image_to_string(
        output_img, lang='eng', config='--psm 8 --oem 1')
    text2 = pytesseract.image_to_string(
        output_img, lang='eng', config='--psm 6 --oem 1')
    if len(text2) > len(text1):
        text = text2  # +'( :psm6)'
    else:
        text = text2  # +'( :psm8)'

    return text


def merge_mser_boxes(img):
    all_rects = []
    # img=cv2.imread(img_path)
    # img = cv2.pyrUp(img)
    img_1 = img.copy()
    height, width, _ = img.shape
    # ------------------------------------------------------------------------------------------------
    mser = cv2.MSER_create()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    regions, _ = mser.detectRegions(gray)
    height, width = gray.shape
    ret, thresh_img = cv2.threshold(gray, 180, 255, cv2.THRESH_BINARY_INV)
    # cv2.imwrite(ret_path,thresh_img)

    black_canvas = np.zeros((height, width), dtype=np.uint8)
    hulls = [cv2.convexHull(p.reshape(-1, 1, 2)) for p in regions]
    h_sum = 0
    w_sum = 0
    a_sum = 0
    all_rects = []
    final_rects = []
    for hull in hulls:
        x, y, w, h = cv2.boundingRect(hull)
        all_rects.append([x, y, w, h])
        h_sum += h
        w_sum += w
        a_sum += (h * w)
    # print('Number of mser rects', len(all_rects))
    if len(all_rects) > 0:
        padding = 0
        avg_ht = float(h_sum) / len(all_rects)
        avg_wd = float(w_sum) / len(all_rects)

        for rect in all_rects:
            x, y, w, h = rect
            patch_binary = thresh_img[y:y + h, x:x + w]
            n_white_pix = np.sum(patch_binary == 255)

            # print('AVG Ar3rea',avg_area,w*h)
            if (w > 4 * avg_wd) or (h > 4 * avg_ht):
                continue
            if n_white_pix < 0.25 * (w * h):
                continue
            final_rects.append([x - padding, y, x + w + padding, y + h])

            # ------------------------------------------------------------------------------------------------
            # coor_list =mser_method(img_path)
            # for coor in coor_list:
            #     cv2.rectangle(img, (int(coor[0]), int(coor[1])), (int(
            #                 coor[2]), int(coor[3])), (0, 255, 0), 1)
            final = non_max_suppression_fast(np.asarray(final_rects), 0.9)
            black_canvas_1 = np.zeros((height, width), dtype=np.uint8)
            for coor in final:
                # print('********',coor)

                cv2.rectangle(black_canvas_1, (int(coor[0]) - 5, int(coor[1])), (int(
                    coor[2]) + 5, int(coor[3])), (255, 255, 255), -1)
            # cv2.imwrite('TTTTTTT.jpg',img)
            _, contours, hierarchy = cv2.findContours(
                black_canvas_1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            for cnt in contours:
                x, y, w, h = cv2.boundingRect(cnt)
                cv2.rectangle(img_1, (x, y),
                              (x + w, y + h), (0, 0, 255), 2)

                # if w> 0.5*width:
                #     continue
                all_rects.append([x, y, x + w, y + h])
            cv2.imwrite('TTTTTTT.jpg', img_1)
            print('*****************Final mser boxes', len(all_rects))
    return all_rects


def bb_intersection_over_union(boxA, boxB):
    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    # compute the area of intersection rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)

    # return the intersection over union value
    return iou


def merge_mser_EAST_v2(final_path, mser_coords, coor_list):
    if isinstance(final_path,str):
        img = cv2.imread(final_path)
        path_1 = final_path.replace('.jpg', '_final.jpg')
        path_2 = final_path.replace('.jpg', '_combined.jpg')
        path_3 = final_path.replace('.jpg', '_EAST.jpg')
        path_4 = final_path.replace('.jpg', '_mser.jpg')
    else:
        img =final_path
    # img = cv2.imread(final_path)
    # img_3 = img.copy()
    im_gray =cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)# cv2.imread(final_path, cv2.IMREAD_GRAYSCALE)
    (thresh, im_bw) = cv2.threshold(im_gray, 128,
                                    255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    im_bw = 255 - im_bw

    img = cv2.pyrUp(img)
    # img = cv2.pyrUp(img)
    img_ret = img.copy()

    img_1 = img.copy()
    img_2 = img.copy()
    img_3 = img.copy()
    pad = 0
    height, width, _ = img.shape
    dummy_coors = []
    final_rects = []
    all_relevant_cols = []
    irrelev_box = []
    img_final = img_final_1 = img.copy()
    height, width, _ = img.shape
    for coor in coor_list:
        coor[0] = int(coor[0])
        coor[1] = int(coor[1])
        coor[2] = int(coor[2])
        coor[3] = int(coor[3])
        patch = im_bw[coor[1]:coor[3], coor[0]:coor[2]]
        cv2.rectangle(img_1, (int(coor[0]), int(coor[1])), (int(
            coor[2]), int(coor[3])), (0, 255, 0), 2)
        for ind, cord in enumerate(mser_coords):

            if ind in dummy_coors:
                continue
            iou = bb_intersection_over_union(cord, coor)
            if iou > 0.0:
                patch1 = patch[:, 0:10]
                patch2 = patch[:, -10:-1]

                all_relevant_cols.append(ind)
                irrelev_box.append(cord)
                # if np.sum(patch1 == 255) > 0 or np.sum(patch2 == 255) > 0:
                #     continue
                # if (coor[0] - cord[0] > 20) and (coor[2] - cord[2] > 20):
                #     continue
                # print(iou)
                coor[0] = min(coor[0], cord[0])
                coor[1] = coor[1]
                coor[2] = max(coor[2], cord[2])
                coor[3] = coor[3]
                # ht=y2-y1
                # wd=x2-x1

    mser_pad = 4
    black_canvas_1 = np.zeros((height, width), dtype=np.uint8)
    for ind, cord in enumerate(mser_coords):
        cv2.rectangle(img_final_1, (int(cord[0]), int(cord[1])), (int(
            cord[2]), int(cord[3])), (255, 0, 0), 2)
        if ind not in all_relevant_cols:
            cv2.rectangle(img_2, (cord[0], cord[1]),
                          (cord[2], cord[3]), (255, 0, 0), 1)
            cv2.rectangle(black_canvas_1, (int(cord[0]) - mser_pad, int(cord[1])), (int(
                cord[2]) + mser_pad, int(cord[3])), (255, 255, 255), -1)

    for coors in coor_list:
        # cv2.rectangle(black_canvas_1, (int(coors[0]), int(coors[1])), (int(
        #        coors[2]), int(coors[3])), (255, 255, 255), -1)
        cv2.rectangle(img_2, (coors[0], coors[1]),
                      (coors[2], coors[3]), (0, 0, 255), 2)
    # for coors in irrelev_box:
    #     cv2.rectangle(img,(coors[0],coors[1]),(coors[2],coors[3]),(255,0,0),1)
    if debug_mode == True:
        cv2.imwrite(path_3, img_1)
        cv2.imwrite(path_4, img_final_1)
    pad_EAST = 5
    # black_canvas_1 = np.zeros((height, width), dtype=np.uint8)
    for coor in coor_list:
        cv2.rectangle(black_canvas_1, (int(coor[0]) - pad_EAST, int(
            coor[1])), (int(coor[2]) + pad_EAST, int(coor[3])), (255, 255, 255), -1)

    _, contours, hierarchy = cv2.findContours(
        black_canvas_1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        # if w > 0.5 * width:
        #     continue
        final_rects.append(
            [x - 10, y, x + w + 10, y + h, int((w + 2 * x) / 2), int((h + 2 * y) / 2)])

    if debug_mode == True:
        # cv2.imwrite(path_2, img)
        cv2.imwrite(path_1, img_2)

    finalset = []
    if backpass == True:
            print('Enabled backpass for ocr extraction *****')
            for r in final_rects:
                patch = img_3[r[1]:r[3], r[0]:r[2]]
                p_h, p_w, _ = patch.shape
                try:

                    text = ocr_v1(patch)
                except:
                    print('Tess Error')
                    text = ''
                words_splitter = text.split(' ')
                # print('&&&&&&&&&&&&&',text,len(words_splitter))

                try:
                    if len(words_splitter) >= 1:
                        new_rect = mser_method_img(patch)
                        if len(new_rect) > len(words_splitter):
                            new_rect = []
                        elif p_w < 15:
                            new_rect = []
                    else:
                        new_rect = []
                except:
                    print('Exception', p_h, p_w)
                    continue
                # new_rect=[]
                # print('Len of rects',len(new_rect))
                if len(new_rect) > 1:
                    for r1 in new_rect:
                        # int_patch = patch[r1[1]:r1[3], r1[0]:r1[2]]
                        # text = ocr_v1(int_patch)
                        # print('************',text)
                        # print(r)
                        # print(r1)
                        cv2.rectangle(patch, (r1[0], r1[1]),
                                      (r1[2], r1[3]), (0, 255, 0), 2)
                    # cv2.imshow('img',patch)
                    # cv2.waitKey(0)
                        coor1 = r[0] + r1[0]
                        coor2 = r[1] + r1[1]
                        coor3 = coor1 + (r1[2] - r1[0])
                        coor4 = coor2 + (r1[3] - r1[1])
                        finalset.append([coor1, coor2, coor3, coor4, int(
                            (coor1 + coor3) / 2), int((coor2 + coor4) / 2)])
                        # cv2.imshow('img',patch)
                        # cv2.waitKey(0)

                else:
                    finalset.append(r)
            for coors in finalset:
                if debug_mode == True:
                    cv2.rectangle(img, (coors[0], coors[1]),
                                  (coors[2], coors[3]), (0, 255, 0), 2)
    else:
        print('Disabled backpass for ocr extraction *****')
        finalset = final_rects
    if debug_mode == True:
        cv2.imwrite(path_2, img)
    sorted_box_grouped = sorted(
        finalset, key=functools.cmp_to_key(cordSort))
    return sorted_box_grouped, img_ret


def merge_mser_EAST(final_path, mser_coords, coor_list):
    final_rects = []

    save_path = '/tmp/' + str(uuid4()) + '_seperated.jpg'
    save_path_1 = '/tmp/' + str(uuid4()) + '_unique_mser.jpg'
    save_path_2 = '/tmp/' + str(uuid4()) + '_mser_black.jpg'
    save_path_3 = '/tmp/' + str(uuid4()) + '_final.jpg'

    img = cv2.imread(final_path)
    img = cv2.pyrUp(img)
    # img = cv2.pyrUp(img)
    img_final = img_final_1 = img.copy()
    height, width, _ = img.shape
    black_canvas_1 = np.zeros((height, width), dtype=np.uint8)
    black_canvas_v2 = black_canvas_1.copy()
    for cord in mser_coords:

        cv2.rectangle(
            black_canvas_1, (cord[0], cord[1]), (cord[2], cord[3]), (255, 255, 255), -1)
        if debug_mode == True:
            cv2.rectangle(img, (cord[0], cord[1]),
                          (cord[2], cord[3]), (255, 0, 0), 2)

    for coor in coor_list:

        cv2.rectangle(black_canvas_1, (int(coor[0]), int(
            coor[1])), (int(coor[2]), int(coor[3])), (0, 0, 0), -1)

        # draw_on_black(black_canvas_1, int(coor[0]), int(coor[1]), int(wt), int(ht), 0, 0)
        if debug_mode == True:
            cv2.rectangle(img, (int(coor[0]), int(coor[1])), (int(
                coor[2]), int(coor[3])), (0, 255, 0), 2)
    for coor in coor_list:
        cv2.rectangle(black_canvas_1, (int(coor[0]), int(
            coor[1])), (int(coor[2]), int(coor[3])), (255, 255, 255), -1)
    _, contours, hierarchy = cv2.findContours(
        black_canvas_1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        cv2.rectangle(black_canvas_v2, (x - 5, y),
                      (x + w + 5, y + h), (255, 255, 255), -1)
    _, contours, hierarchy = cv2.findContours(
        black_canvas_v2, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        final_rects.append(
            [x, y, x + w, y + h, int((w + 2 * x) / 2), int((h + 2 * y) / 2)])
        if debug_mode == True:
            cv2.rectangle(img_final, (x, y), (x + w, y + h), (0, 255, 0), 2)
    if debug_mode == True:
        cv2.imwrite(save_path, img)
        cv2.imwrite(save_path_1, black_canvas_1)
        cv2.imwrite(save_path_3, img_final)
    sorted_box_grouped = sorted(
        final_rects, key=functools.cmp_to_key(cordSort))
    print('Total final rects', len(final_rects))

    return sorted_box_grouped, img_final_1


def combined_bounding_box_extraction_method_v2(img):
    final_rects = None
    if 1:
        coor_list = main(img)
        mser_cords = mser_method(img)
        final_rects, img = merge_mser_EAST_v2(img, mser_cords, coor_list)
        all_rects, img = scale_down_v2(img, final_rects)
        img = cv2.pyrDown(img)
        if debug_mode == True:
            new_img = img.copy()
            for rects in all_rects:
                cv2.rectangle(
                    new_img, (rects[0], rects[1]), (rects[2], rects[3]), (0, 0, 255), 2)
            path_1 = final_path.replace('.jpg', '_resized.jpg')
            cv2.imwrite(path_1, new_img)

    else:
        pass
    rect_and_img = [all_rects, img]
    yield rect_and_img


if __name__ == '__main__':
    import os
    import sys

    checklist = ['_seperated.jpg', '_unique_mser.jpg', '_mser_black.jpg',
                 '_mser.jpg', '_mser_original.jpg', '_original.jpg']
    folder = '/home/user2/Desktop/060118-2-/ERROR_Images/'
    for file in os.listdir(folder):
        if not file.endswith('.jpg'):
            continue

        now = time.time()
#-----------------------------------------------------------------------------------
        file_path = folder + file
        print(file_path)
        combined_bounding_box_extraction_method_v2(file_path)
        # coor_list = []
        # final_path, coor_list = main(file_path)
        # mser_cords = mser_method(file_path)
        # merge_mser_EAST_v2(file_path, mser_cords, coor_list)
        print(time.time() - now)
#-----------------------------------------------------------------------------------

        # mser_cords = mser_method(file_path)
        # img=cv2.imread(file_path)
        # scale_down_v2(img,mser_cords,scale_val=2)
        # sys.exit()
