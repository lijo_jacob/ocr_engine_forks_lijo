import boto
import boto.s3
import sys
from boto.s3.key import Key
import json


from io import StringIO
import pandas as pd
import traceback
import psycopg2
import boto3
import sys
import os



def connect_to_s3(aws_access_key_id, aws_secret_access_key, bucket, subdirectory = None, **kwargs):
    global s3, s3_bucket_var, s3_subdirectory_var, aws_1, aws_2, aws_token
    s3 = boto3.resource('s3',
                        aws_access_key_id = aws_access_key_id,
                        aws_secret_access_key = aws_secret_access_key,
                        **kwargs)
    s3_bucket_var = bucket
    if subdirectory is None:
        s3_subdirectory_var = ''
    else:
        s3_subdirectory_var = subdirectory + '/'
    aws_1 = aws_access_key_id
    aws_2 = aws_secret_access_key
    if kwargs.get('aws_session_token'):
        aws_token = kwargs.get('aws_session_token')
    else:
        aws_token = ''



#print( aws_access_key_id,aws_secret_access_key,bucket )
def upload_to_s3(filepath,filename ):
    s3.Bucket(s3_bucket_var).upload_file(filepath, s3_subdirectory_var+filename)        