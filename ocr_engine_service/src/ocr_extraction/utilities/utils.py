import os
import sys
import cv2
import json
import boto3
import random
import img2pdf
import datetime
import requests

from django.conf import settings
from .constants import spell_check_url
from PIL import Image

S3_AWS_KEY = 'AKIAJILHNXF6NS77TMRA'
S3_AWS_SECRET = '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'

print('S3_Bucket_Name: ', os.environ["S3_BUCKET_NAME"])


def s3_download_model(s3_path, bucket_name=os.environ["S3_BUCKET_NAME"]):
    # local_file_path = os.path.join('data', s3_model_path)
    check_dir_exist(os.path.dirname(s3_path))
    s3 = get_s3_client()
    s3.download_file(bucket_name, s3_path, s3_path)
    return s3_path


def get_s3_client(aws_access_key_id=S3_AWS_KEY, aws_secret_access_key=S3_AWS_SECRET):
    return boto3.client('s3', aws_access_key_id=aws_access_key_id,
                        aws_secret_access_key=aws_secret_access_key)


def check_dir_exist(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def s3_download_file(s3_path, bucket_name, file_save_path):
    check_dir_exist(os.path.dirname(file_save_path))
    s3 = get_s3_client()
    s3.download_file(bucket_name, s3_path, file_save_path)
    return file_save_path


def get_temp_fpath(data):
    rand_name = "%0.12d" % random.randint(0, 999999999999)
    extn = os.path.splitext(str(data))[1]
    return os.path.join('model_files', rand_name + extn)


def fpath_from_s3(s3_url, fpath=None, common_bucket=None):
    # if not fpath:
    #     fpath = get_temp_fpath(s3_url)
    # bucket_name = get_bucket_name(common_bucket)
    # # print(bucket_name)
    # return s3_download_file(s3_url, bucket_name, fpath)
    loc_path = None
    ctr = S3_RETRY_LIMIT

    while (ctr):
        try:
            if not fpath:
                fpath = get_temp_fpath(s3_url)
            if common_bucket == None:
                bucket_name = get_bucket_name(common_bucket)
            else:
                bucket_name = common_bucket
            print(bucket_name)
            loc_path = s3_download_file(s3_url, bucket_name, fpath)
            break
        except Exception as e:
            ctr -= 1
            sleep_time = random.randint(1, 10)
            print('S3 DOWNLOAD FAILED, RETRYING IN ', sleep_time, ' seconds...')

            time.sleep(sleep_time)
    return loc_path


def s3_upload_file(s3_path, bucket_name, file_save_path):
    check_dir_exist(os.path.dirname(file_save_path))
    s3 = get_s3_client()
    s3.download_file(bucket_name, s3_path, file_save_path)
    return file_save_path


def upload_file_s3(s3, local_file_path, s3_bucket_name, s3_file_path):
    # print('S3_Filepath', s3_file_path)
    s3.upload_file(local_file_path, s3_bucket_name,
                   s3_file_path)


def s3_upload_model(local_file_path, bucket_name):
    uniq_filename = (str(datetime.datetime.now())).replace(' ', '_')
    # s3_model_path = os.path.join(
    #     'models/', system_id, os.path.basename(local_file_path))
    # name = 'ai_data/ocr_images/' + str(uniq_filename) + '.jpg'
    upload_file_s3(get_s3_client(), local_file_path,
                   bucket_name, local_file_path)
    # print('image_path: ', os.path.join(bucket_name, local_file_path))
    return os.path.join(bucket_name, local_file_path)


def dump_to_s3(image, name):
    # img = cv2.imread(path)
    # uniq_filename = (str(datetime.datetime.now())).replace(' ', '_')
    img = image.copy()
    dire = 'ai_data/ocr_images'
    check_dir_exist(dire)
    fnl_path = os.path.join(dire, name) + '.jpg'
    cv2.imwrite(fnl_path, img)
    return upload_to_s3(fnl_path), dire


def upload_to_s3(local_file_path, common_bucket=None):
    # bucket_name = get_bucket_name(common_bucket)
    bucket_name = os.environ["S3_BUCKET_NAME"]
    return s3_upload_model(local_file_path, bucket_name)

# def run():
#     path = '/home/vishnu/Desktop/sample.jpg'
#     dump_to_s3(path)
    # img = cv2.imread(path)
    # upload_to_s3(path)
    # model_path = fpath_from_s3(
    #     'ai_data/ocr_model_files/model_file.hdf5', None, settings.S3_BUCKET_NAME)


def get_ocr_response(json_, image, url=None):
    if not url:
        url = os.path.join(settings.NLP_URL, spell_check_url)
    files = {
        'image': open(image, 'rb'),
        'data': open(json_, 'r')
    }
    return requests.post(url, files=files)


def call_ocr_response_task(responseFrmOcr):
    from job_man.tasks import get_ocr_response
    get_ocr_response.delay(responseFrmOcr)


def convert_image2pdf(img_path):
    '''
    Function to convert single page images into pdf file
    '''
    pdf_path = os.path.splitext(img_path)[0] + '.pdf'
    image = Image.open(img_path)
    pdf_bytes = img2pdf.convert(image.filename)
    file = open(pdf_path, "wb")
    file.write(pdf_bytes)
    image.close()
    file.close()
    print("Successfully made pdf file", pdf_path)
    return pdf_path


def merge_pdfs(pdflist):
    '''
    Function to merge multiple pdf pages into a single pdf file
    '''
    from PyPDF2 import PdfFileMerger
    merger = PdfFileMerger()
    for pdf in pdflist:
        merger.append(open(pdf, 'rb'))
    result_pdf = os.path.join(os.path.split(pdflist[0])[0], 'result.pdf')
    with open(result_pdf, 'wb') as fout:
        merger.write(fout)
    return result_pdf
