import os
import cv2
import keras
import numpy as np
from keras import backend as K
from keras.optimizers import SGD
from keras.models import Sequential
from __future__ import print_function
from scipy.misc import imread, imresize
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Dense, Dropout, Flatten

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from base_model_vgg import MiniVGGNet, get_model, get_seq_model


path = 'dataset_train_angle_cnn/'

data = []
labels = []

img_rows, img_cols = 64, 64

batch_size = 64

num_classes = 4

epochs = 250

for folder, subfolders, files in os.walk(path):
  for name in files:
    if name.endswith('.jpg'):
      x = cv2.imread(folder + '/' + name)
      x = imresize(x, (img_rows, img_cols))

      data.append(x)

      labels.append(os.path.basename(folder))

x_train, x_test, y_train, y_test = train_test_split(data, labels,
                                                    random_state=0,
                                                    test_size=0.2)
x_train = np.array(x_train)
x_test = np.array(x_test)
y_train = np.array(y_train)
y_test = np.array(y_test)


x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

model = get_seq_model(64, 64, 3, num_classes)
model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])

lb = LabelEncoder()
y_train = lb.fit_transform(y_train)
y_test = lb.fit_transform(y_test)

y_train = keras.utils.to_categorical(y_train)
y_test = keras.utils.to_categorical(y_test)

model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test, y_test))

score = model.evaluate(x_test, y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])


model_json = model.to_json()
with open("model.json", "w") as json_file:
  json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("model.h5")
print("Saved model to disk")
