import time
import datetime
import random
from .ocr_utils_multi_proc import (check_dir_exist, get_conf_score, draw_boxes, detect_angle_cnn, detect_angle_new,
                                   tesseract_only, getLists_new, process_ocr, extract_text_from_patch,extract_text_from_patch_all_in_memory, post_process, combined_bounding_box_extraction_method)
from .combined_approach_2 import combined_bounding_box_extraction_method_v2
import sys
from scipy import ndimage
from PIL import Image as im
from scipy.ndimage import interpolation as inter
import numpy as np
import cv2
from keras import backend as K
import sys
import billiard
import psutil
from multiprocessing.util import Finalize
from functools import partial

no_of_cores = psutil.cpu_count()

'''
Function sorts all coordinates linewise for detected coordinates in an image
coords_copy: lists of list
returns sorted coordinates
'''


def sort_line_rowwise(coords_copy):
    first_elems = [coords_copy[0]]
    thres = coords_copy[0][3]
    all_coords_selected = []
    init_val = 0
    for ind in range(1, len(coords_copy)):

        if coords_copy[ind][1] > thres:
            first_elems.append(coords_copy[ind])
            thres = coords_copy[ind][3]
            all_coords_selected.append([init_val, ind])
            init_val = ind

    final_rect = []
    for e in all_coords_selected:
        begin = e[0]
        end = e[1]
        if end > begin:

            new_coords = coords_copy[begin:end]
            new_coords = sorted(new_coords, key=lambda x: x[0])
            for coords in new_coords:
                # print ('Coords',coords)
                final_rect.append(coords)
        elif begin == end:
            new_coords = coords_copy[begin]
            # print('Elif loop',new_coords)
            final_rect.append(new_coords)
    if end <  len(coords_copy):
            new_coords = coords_copy[end:len(coords_copy)]
            new_coords = sorted(new_coords, key=lambda x: x[0])
            for ind, coord in enumerate(new_coords):
                  final_rect.append(coord)
    yield final_rect


def find_score(arr, angle):

    data = inter.rotate(arr, angle, reshape=False, order=0)
    hist = np.sum(data, axis=1)
    score = np.sum((hist[1:] - hist[:-1]) ** 2)
    # print('Angle', angle,score)
    return hist, score,angle


def skew_correction(input_file):
    _finalizers = []
    p = billiard.Pool(int(no_of_cores) - 1)
    _finalizers.append(Finalize(p, p.terminate))
    img_original = cv2.imread(input_file)
    img = im.open(input_file)

    # convert to binary
    wd, ht = img.size
    pix = np.array(img.convert('1').getdata(), np.uint8)
    bin_img = 1 - (pix.reshape((ht, wd)) / 255.0)



    delta = 1
    limit = 5
    angles = np.arange(-limit, limit + delta, delta)
    scores = []
    final_angles = []


    # tic = time.time()
    #-------------------parallelized method-----------------------
    part_func = partial(find_score, bin_img)
    try:
        for hist, score,Angle in p.map(part_func, angles):
            scores.append(score)
            final_angles.append(Angle)
            p.close()
            p.join()
    finally:
        p.terminate()
    #---------------------------------------------------------------
    #------------------------original method-------------------------
    # tic = time.time()
    # for angle in angles:
    #     hist, score = find_score(bin_img, angle)
    #     scores.append(score)

    #----------------------------------------------------------------

    best_score = max(scores)
    best_angle = final_angles[scores.index(best_score)]
    print('Best angle: {}'.format(best_angle))


    # correct skew
    data = inter.rotate(bin_img, best_angle, reshape=False, order=0)
    img = im.fromarray((255 * data).astype("uint8")).convert("RGB")
    img_new = np.array(img)
    img_new = 255 - img_new
    new_img = ndimage.rotate(img_original, best_angle, cval=255.0)
    cv2.imwrite(input_file, new_img)
    return input_file, best_angle


def getOCRText(filepath, post_processing_flag=False, method_flag='tess_only',write_flag=False):
    print('Method flag passed', method_flag)
    best_angle =0
    try:

        filepath, best_angle = skew_correction(detect_angle_new(filepath))
    except Exception as e:
        print('*** Skew COrrection Error ***', str(e))
        print('*************************\n'*10)
        print('*************Code may fail!!!*****************')
        print('*************************\n' * 10)
    # sys.exit()
    if method_flag == 'tess_only':
        print('Tess only method flag enabled')
        json_out, final_df, listt, rgb, parse_error_flag, s3_path_org = tesseract_only(
            ip_img)
        listt = sort_line_rowwise(listt)
        s3_path_box = None
        if not parse_error_flag:

            json_out, df, s3_path_box, s3_path_org = extract_text_from_patch(
                listt, rgb, delete_flag=False)
        if post_processing_flag:
            print('***Post_processing flag: {}***'.format(post_processing_flag))
            res_data = post_process(
                filepath, json_out)
            res_data['box_image_url'] = s3_path_box
            res_data['image_url'] = s3_path_org
            return res_data
        else:
            print('parse_error_flag flag: {}'.format(parse_error_flag))
            json_out['box_image_url'] = s3_path_box
            json_out['image_url'] = s3_path_org
            return json_out

    elif method_flag == 'connected_only':
        print('connected_only flag enabled')
        start_time = time.time()
        listt, rgb = getLists_new(ip_img)
        listt = sort_line_rowwise(listt)
        print("%s seconds : For finding Bounding boxes" %
              (time.time() - start_time))
        if listt != None:
            print('post_processing enabled')
            # final_json, df, s3_path = extract_text_from_patch(
            #     listt, rgb, delete_flag=False)
            final_json, df, s3_path = extract_text_from_patch(
                listt, rgb, delete_flag=False)
            if flag == True:
                res_data = post_process(filepath, final_json)
                return res_data
            else:
                return final_json
        else:
            return None

    elif method_flag == 'combined_method':
        ip_img = cv2.imread(filepath)
        K.clear_session()

        parse_error_flag = None
        print('Combined algorithms flag enabled')
        start_time = time.time()

        print('Combined Method')
        combined_bounding_box_res = combined_bounding_box_extraction_method_v2(ip_img)
        listt, rgb = next(combined_bounding_box_res)
        ht_image,wd_image,_ = rgb.shape
        print('No of boxes', len(listt))
        listt_yielded = sort_line_rowwise(listt)
        listt = next(listt_yielded)

        print("%s seconds : For finding Bounding boxes" %
              (time.time() - start_time))
        if listt != None:
            #------------------Original Flow -------------------------------------------
            # json_out, df, s3_path_box, s3_path_org = extract_text_from_patch(
            #     listt, rgb, delete_flag=True)
            #--------------------------New Flow--------------------------------------------------
            json_out, df, s3_path_box, s3_path_org = extract_text_from_patch_all_in_memory(
                listt, rgb, delete_flag=True,write_flag=False)


            if post_processing_flag:
                print('***Post_processing flag: {}***'.format(post_processing_flag))
                res_data = post_process(
                    filepath, json_out)
                res_data['box_image_url'] = s3_path_box
                res_data['image_url'] = s3_path_org
                res_data['image_height'] = str(ht_image)
                res_data['image_width'] = str(wd_image)
                res_data['skew_correction_angle'] = str(best_angle)
                return res_data
            else:
                print('parse_error_flag flag: {}'.format(parse_error_flag))
                json_out['box_image_url'] = s3_path_box
                json_out['image_url'] = s3_path_org
                json_out['image_height'] = str(ht_image)
                json_out['image_width'] = str(wd_image)
                json_out['skew_correction_angle'] = str(best_angle)
                return json_out
        else:
            return None
            # final_json, df = extract_text_from_patch(
            #     listt, rgb, delete_flag=False)
        #--------------------------------------------------------------------------------------------
        #     final_json['image_url'] = s3_path
        #     return final_json
        # else:
        #     return None
        #--------------------------------------------------------------------------------------------


if __name__ == '__main__':
    # getOCRText(
    #     '/home/vishnu/Desktop/GEC_15_patient_list/ALLEN,MARY L/ALLEN,MARY L.jpg')
    print("JSON output:\n",
          getOCRText('/home/vishnu/Downloads/ocr_sample_20.jpg'))
