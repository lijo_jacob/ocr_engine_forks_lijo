from EAST.run_demo_server import main
import cv2
import numpy as np
import time
import os
from uuid import uuid4
import functools

debug_mode = False


def cordSort(box1, box2):
    if abs(box1[5] - box2[5]) < 5:
        if box1[4] > box2[4]:
            return 1
        elif box1[4] < box2[4]:
            return -1
    else:
        if box1[5] > box2[5]:
            return 1
        elif box1[5] < box2[5]:
            return -1
    return 0


def draw_on_black(black_canvas, x, y, w, h, padding, val):
    cv2.rectangle(black_canvas, (x - padding, y),
                  (x + w + padding, y + h), (val, val, val), -1)


def mser_method(img_path):
    padding = 5
    ret_path = '/tmp/' + str(uuid4) + '_bin.jpg'
    ret_path_1 = '/tmp/' + str(uuid4) + '_mser_original.jpg'

    img = cv2.imread(img_path)
    img = cv2.pyrUp(img)
    img_2 = img.copy()
    mser = cv2.MSER_create()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    regions, _ = mser.detectRegions(gray)
    height, width = gray.shape
    ret, thresh_img = cv2.threshold(gray, 180, 255, cv2.THRESH_BINARY_INV)
    # cv2.imwrite(ret_path,thresh_img)

    black_canvas = np.zeros((height, width), dtype=np.uint8)
    hulls = [cv2.convexHull(p.reshape(-1, 1, 2)) for p in regions]
    h_sum = 0
    w_sum = 0
    a_sum = 0
    all_rects = []
    final_rects = []
    for hull in hulls:
        x, y, w, h = cv2.boundingRect(hull)
        all_rects.append([x, y, w, h])
        h_sum += h
        w_sum += w
        a_sum += (h * w)
    print('Number of mser rects', len(all_rects))
    if len(all_rects) > 0:
        avg_ht = float(h_sum) / len(all_rects)
        avg_wd = float(w_sum) / len(all_rects)

        for rect in all_rects:
            x, y, w, h = rect
            patch_binary = thresh_img[y:y + h, x:x + w]
            n_white_pix = np.sum(patch_binary == 255)

            # print('AVG Ar3rea',avg_area,w*h)
            if w > 4 * avg_wd or h > 4 * avg_ht:
                continue
            if n_white_pix < 0.25 * (w * h):
                continue
            final_rects.append([x - padding, y, x + w + padding, y + h])

            cv2.rectangle(img_2, (x - padding, y),
                          (x + w + padding, y + h), (0, 0, 255), 2)
        if debug_mode == True:
            cv2.imwrite(ret_path_1, img_2)

    return final_rects


def merge_mser_EAST(final_path, mser_coords, coor_list):
    final_rects = []

    save_path = '/tmp/' + str(uuid4) + '_seperated.jpg'
    save_path_1 = '/tmp/' + str(uuid4) + '_unique_mser.jpg'
    save_path_2 = '/tmp/' + str(uuid4) + '_mser_black.jpg'
    save_path_3 = '/tmp/' + str(uuid4) + '_final.jpg'

    img = cv2.imread(final_path)
    img = cv2.pyrUp(img)
    img_final = img.copy()
    height, width, _ = img.shape
    black_canvas_1 = np.zeros((height, width), dtype=np.uint8)
    black_canvas_v2 = black_canvas_1.copy()
    for cord in mser_coords:

        cv2.rectangle(
            black_canvas_1, (cord[0], cord[1]), (cord[2], cord[3]), (255, 255, 255), -1)
        if debug_mode == True:
            cv2.rectangle(img, (cord[0], cord[1]),
                          (cord[2], cord[3]), (255, 0, 0), 2)

    for coor in coor_list:

        cv2.rectangle(black_canvas_1, (int(coor[0]), int(
            coor[1])), (int(coor[2]), int(coor[3])), (0, 0, 0), -1)

        # draw_on_black(black_canvas_1, int(coor[0]), int(coor[1]), int(wt), int(ht), 0, 0)
        if debug_mode == True:
            cv2.rectangle(img, (int(coor[0]), int(coor[1])), (int(
                coor[2]), int(coor[3])), (0, 255, 0), 2)
    for coor in coor_list:

        cv2.rectangle(black_canvas_1, (int(coor[0]), int(
            coor[1])), (int(coor[2]), int(coor[3])), (255, 255, 255), -1)
    _, contours, hierarchy = cv2.findContours(
        black_canvas_1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        cv2.rectangle(black_canvas_v2, (x - 5, y),
                      (x + w + 5, y + h), (255, 255, 255), -1)
    _, contours, hierarchy = cv2.findContours(
        black_canvas_v2, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        final_rects.append(
            [x, y, x + w, y + h, int((w + 2 * x) / 2), int((h + 2 * y) / 2)])
        if debug_mode == True:
            cv2.rectangle(img_final, (x, y), (x + w, y + h), (0, 255, 0), 2)
    if debug_mode == True:
        cv2.imwrite(save_path, img)
        cv2.imwrite(save_path_1, black_canvas_1)
        cv2.imwrite(save_path_3, img_final)
    sorted_box_grouped = sorted(
        final_rects, key=functools.cmp_to_key(cordSort))
    print('Total final rects', len(final_rects))

    return sorted_box_grouped, img


def combined_bounding_box_extraction_method_v2(file_path):
    final_rects, img = None, None
    if 1:
        final_path, coor_list = main(file_path)
        mser_cords = mser_method(file_path)
        final_rects, img = merge_mser_EAST(file_path, mser_cords, coor_list)
    else:
        pass
    return final_rects, img


if __name__ == '__main__':
    import os
    import sys
    checklist = ['_seperated.jpg', '_unique_mser.jpg', '_mser_black.jpg',
                 '_mser.jpg', '_mser_original.jpg', '_original.jpg']
    folder = '/home/user2/Downloads/selected_formats/'
    for file in os.listdir(folder):
        if file.endswith('.txt') or not file.endswith('.jpg'):
            continue

        now = time.time()

        file_path = folder + file
        print(file_path)
        coor_list = []
        final_path, coor_list = main(file_path)
        mser_cords = mser_method(file_path)
        merge_mser_EAST(file_path, mser_cords, coor_list)
        print(time.time() - now)
        # sys.exit()
