import os
import cv2
import random
import numpy as np
import pytesseract
from .ufarray import UFarray
from itertools import product
from PIL import Image, ImageDraw


def run(img):
    data = img.load()
    width, height = img.size
    uf = UFarray()
    labels = {}
    for y, x in product(range(height), range(width)):

        #         c.append(count)
        if data[x, y] == 255:
            pass
#         count+=1
#         print(count)
        elif y > 0 and data[x, y - 1] == 0:
            labels[x, y] = labels[(x, y - 1)]

        elif x + 1 < width and y > 0 and data[x + 1, y - 1] == 0:

            c = labels[(x + 1, y - 1)]
            labels[x, y] = c

            if x > 0 and data[x - 1, y - 1] == 0:
                a = labels[(x - 1, y - 1)]
                uf.union(c, a)

            elif x > 0 and data[x - 1, y] == 0:
                d = labels[(x - 1, y)]
                uf.union(c, d)

        elif x > 0 and y > 0 and data[x - 1, y - 1] == 0:
            labels[x, y] = labels[(x - 1, y - 1)]

        elif x > 0 and data[x - 1, y] == 0:
            labels[x, y] = labels[(x - 1, y)]

        else:
            labels[x, y] = uf.makeLabel()
    uf.flatten()

    colors = {}

    output_img = Image.new("RGB", (width, height))
    outdata = output_img.load()

    for (x, y) in labels:

        component = uf.find(labels[(x, y)])
        labels[(x, y)] = component

        if component not in colors:
            # colors[component] = (255, 0, 0)

            colors[component] = (random.randint(
                0, 255), random.randint(0, 255), random.randint(0, 255))

        outdata[x, y] = colors[component]
#         print(outdata[x, y])
    return (labels, output_img)


# @profile
def req_coods(coods):
    rev_coods = [(b, a) for a, b in coods]
    min_x = min(min(coods)[0], min(rev_coods)[1])
    min_y = min(min(coods)[1], min(rev_coods)[0])
    max_x = max(max(coods)[0], max(rev_coods)[1])
    max_y = max(max(coods)[1], max(rev_coods)[0])
    return min_x, min_y, max_x, max_y


def calc_centre(points):
    width = points[2] - points[0]
    height = points[3] - points[1]
    cx = points[0] + width / 2
    cy = points[1] + height / 2
    return cx, cy


def calc_points(points):
    width = points[2] - points[0]
    height = points[3] - points[1]
    t_lx, t_ly = points[0], points[1]
    b_rx, b_ry = points[2], points[3]
    return t_lx, t_ly, b_rx, b_ry


def cordSort(box1, box2):
    if abs(box1[1] - box2[1]) > 3:
        if box1[1] > box2[1]:
            return 1
        elif box1[1] < box2[1]:
            return -1
    if abs(box1[0] - box2[0]) > 3:
        if box1[0] > box2[0]:
            return 1
        elif box1[0] < box2[0]:
            return -1
        return 0
    return 0


def get_max_tuple(a, b):
    return max(b[0], b[0]), max(a[1], b[1])


def crop_and_remove_line(bw):
    bw = cv2.bitwise_not(bw)
    __, contours, hierarchy = cv2.findContours(
        bw, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    for idx in range(len(contours)):
        x, y, w, h = cv2.boundingRect(contours[idx])
        if h < 10.0:
            cv2.rectangle(bw, (x, y), (x + w, y + h), (0, 0, 0), -2)
    return(bw)


def process_test_image(img_path):
    kernel = np.ones((2, 2), np.uint8)
    I = cv2.imread(img_path)
    # I = I[0:1300, :]
    I2 = I.copy()

    I = cv2.cvtColor(I, cv2.COLOR_RGB2GRAY)
    I2 = cv2.cvtColor(I2, cv2.COLOR_RGB2GRAY)
    I = cv2.erode(I, kernel, iterations=1)
    I = cv2.dilate(I, kernel, iterations=1)
    # I = cv2.medianBlur(I, 3)

    __, bw = cv2.threshold(I, 0.0, 255.0, cv2.THRESH_BINARY |
                           cv2.THRESH_OTSU)
    __, bw_org = cv2.threshold(I2, 0.0, 255.0, cv2.THRESH_BINARY |
                               cv2.THRESH_OTSU)

    bw = cv2.bitwise_not(crop_and_remove_line(bw))
    bw_org = cv2.bitwise_not(crop_and_remove_line(bw_org))

    fpth = os.path.splitext(img_path)[0] + '_bw.jpg'
    fpth_org = os.path.splitext(img_path)[0] + '_bw_org.jpg'
    # cv2.imwrite('/home/vishnu/Desktop/00986.jpg', bw)
    cv2.imwrite(fpth, bw)
    cv2.imwrite(fpth_org, bw_org)
#     cv2.imwrite('/home/vishnu/Desktop/00986.jpg', bw)
    return fpth, fpth_org


def get_bounding_box(mask, img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    __, contours, hierarchy = cv2.findContours(
        mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    bboxes_list = list()
    for idx in range(len(contours)):
        x, y, w, h = cv2.boundingRect(contours[idx])
        bboxes_list.append([x, y, x + w, y + h])
#         mask[y:y + h, x:x + w] = 0
        cv2.drawContours(mask, contours, idx, (255, 255, 255), -2)
        cv2.rectangle(img, (x, y), (x + w - 1, y + h - 1), (0, 255, 0), 2)
    return(img, bboxes_list)


def box_with_tess(path):
    img = cv2.imread(path)
    h, w, __ = img.shape
    mask = np.zeros(img.shape[:2], dtype=np.uint8)
    letters = pytesseract.image_to_boxes(img, config=' --oem 1')
    letters = letters.split('\n')
    letters = [letter.split() for letter in letters]

    for letter in letters:
        cv2.rectangle(mask, (int(letter[1]), h - int(letter[2])),
                      (int(letter[3]), h - int(letter[4])), (255, 255, 255), -1)
    return mask
