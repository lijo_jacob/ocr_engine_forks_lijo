import os
import cv2
import glob
import time
import pytesseract
import numpy as np
from PIL import Image
import concurrent.futures
import pillowfight
import billiard
# from .cnn_pred import grab_model, do_Prediction
import csv
import psutil
import keras
from keras.models import load_model
import importlib
from pytesseract import image_to_string
import warnings
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
from .combined_approach_2 import mser_method_img
import subprocess
import json
import pandas as pd
import warnings
from PIL import Image, ImageFilter, ImageEnhance
import uuid
import functools


from functools import partial
warnings.filterwarnings("ignore")


os.environ['OMP_THREAD_LIMIT'] = '1'
label_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R',
              'T', 'W', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
label_list = np.asarray(label_list)
delim = ':'
no_of_cores = psutil.cpu_count()
print('No of cores on system>>>>>>>>>>>>>>>>>>>>>', no_of_cores)


def sort_line_rowwise(coords_copy):
    first_elems = [coords_copy[0]]
    thres = coords_copy[0][3]
    all_coords_selected = []
    init_val = 0
    for ind in range(1, len(coords_copy)):

        if coords_copy[ind][1] > thres:
            first_elems.append(coords_copy[ind])
            thres = coords_copy[ind][3]
            all_coords_selected.append([init_val, ind])
            init_val = ind

    final_rect = []
    for e in all_coords_selected:
        begin = e[0]
        end = e[1]
        if end > begin:

            new_coords = coords_copy[begin:end]
            new_coords = sorted(new_coords, key=lambda x: x[0])
            for coords in new_coords:
                # print ('Coords',coords)
                final_rect.append(coords)
        elif begin == end:
            new_coords = coords_copy[begin]
            # print('Elif loop',new_coords)
            final_rect.append(new_coords)
    if end <  len(coords_copy):
            new_coords = coords_copy[end:len(coords_copy)]
            new_coords = sorted(new_coords, key=lambda x: x[0])
            for ind, coord in enumerate(new_coords):
                  final_rect.append(coord)
    yield final_rect

def change_contrast(img, level):
    factor = (259 * (level + 255)) / (255 * (259 - level))

    def contrast(c):
        return 128 + factor * (c - 128)
    return img.point(contrast)

def process_ocr_v2(box, rgb):
    top, bottom, left, right = [50] * 4
    color = [255, 255, 255]
    # print(box)
    x, y = box[:2]
    w, h = box[2] - box[0], box[3] - box[1]
    mx, my = box[4:6]
    if y - 2 != 0 and x - 2 != 0:
        roi = rgb[y - 2:y + h + 2, x - 2:x + w + 2]
    else:
        roi = rgb[y:y + h, x:x + w]
    if roi.shape[0] != 0 and roi.shape[1] != 0:
        # cr_name = str(i) + '_' + str(x) + '_' + \
        #     str(y) + '_' + str(w) + '_' + str(h)
        roi = cv2.pyrUp(roi)
        roi = cv2.copyMakeBorder(
            roi, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)
        roi = Image.fromarray(roi)
        roi = roi.filter(ImageFilter.SHARPEN)
        # roi = ImageEnhance.Brightness(roi).enhance(1.5)
        roi = change_contrast(roi, 25)
    return np.array(roi)



def grab_model(model_file):
    label_encoder = LabelEncoder()
    integer_encoded = label_encoder.fit_transform(label_list)
    return (model_file, label_encoder, integer_encoded)


def do_Prediction(wordimg_path, model, label_encoder, integer_encoded):
    img = cv2.imread(wordimg_path, 0)

    __, thresh = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY_INV)
    ret, msk = cv2.connectedComponents(thresh)
    words = []
    for label in range(0, ret):
        mask = np.array(msk, dtype=np.uint8)
        mask[msk == label] = 0
        mask[msk != label] = 255
        active_px = np.argwhere(mask == 0)
        active_px = active_px[:, [1, 0]]
        x, y, w, h = cv2.boundingRect(active_px)
        words.append((x, y, w, h, label))
    sort_words = sorted(words[1:len(words)], key=lambda x: x[0])
    ans = ""

    for i in range(len(sort_words)):
        try:
            # print('A')
            x, y, w, h, label = sort_words[i]
            mask[msk == label] = 0
            mask[msk != label] = 255
            ima = mask[y - 2:y + h + 3, x - 2:x + w + 3]
            letter = cv2.resize(ima, (20, 20)) / 255
            X_test = np.asarray(letter)
            X_test = np.reshape(X_test, [-1, 20, 20, 1])
            # print('AA########')
            # print('MODEL_SUMMARY', model.summary())

            pred = model.predict(X_test, verbose=0)
            # print('MMMMM')
            top = pred[0].argsort()[-5:][::-1]
            ind = [top[i] for i in range(top.size)]
            val = [pred[0][i] for i in ind]
            output = np.argmax(pred)
            output = label_encoder.inverse_transform([output])
            top_output = label_encoder.inverse_transform(ind)
            # print(top_output)
            # print(val)
            ans += output[0]
            # print(ans)
        except:
            print('SKIPPED')
            ans = ' '
    return(ans)


def cordSort(box1, box2):
    if abs(box1[1] - box2[1]) < 5:
        if box1[0] > box2[0]:
            return 1
        elif box1[0] < box2[0]:
            return -1
    else:
        if box1[1] > box2[1]:
            return 1
        elif box1[1] < box2[1]:
            return -1
    return 0


def get_conf_score(DIR_PATH):
    parse_error_flag = False
    image = cv2.imread(DIR_PATH)
    OUTPUT_FILE = os.path.splitext(DIR_PATH)[0] + '_conf'
    func_tesseract = 'tesseract ' + DIR_PATH + " " + \
        OUTPUT_FILE + ' --oem 1 -l eng --psm 6 tsv'
    # os.system(func_tesseract)
    subprocess.check_output(func_tesseract, shell=True)
    f_df = pd.DataFrame()
    # tsv_file = OUTPUT_FILE + '.tsv'
    # csv_table = pd.read_table(tsv_file, sep='\t')
    # csv_table.to_csv(OUTPUT_FILE + '.csv')
    OUTPUT_FILE = os.path.join('./', OUTPUT_FILE)
    main_file = OUTPUT_FILE + '.tsv'
    try:
        # raise pd.errors.ParserError
        df = pd.read_csv(main_file, sep="\t", error_bad_lines=False)

    except pd.errors.ParserError:
        # elif 1:
        print('*******Handling parse error********')
        parse_error_flag = True
        with open(main_file, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            list_reader = list(reader)
            main_list = [list(dict(row).values())[0].split('\t')
                         for row in list_reader]
            columns = list(dict(list_reader[0]).keys())[0].split('\t')
            df = pd.DataFrame(main_list, columns=columns)
    # print('DF_EROOR', df.head())
    # print(df.head())
    # df = pd.read_csv(OUTPUT_FILE + '.tsv', sep='\t', error_bad_lines=False, engine = 'python')
    df1 = df.dropna()
    conf_df = pd.concat([df1, f_df], ignore_index=True)
    # os.remove(file)
    conf_df = conf_df.rename(
        index=str, columns={"left": "tl_x", "top": "tl_y", "text": "Text"})
    try:
        conf_df['br_x'] = conf_df['tl_x'].astype(float) + conf_df['width'].astype(float)
        conf_df['br_y'] = conf_df['tl_y'].astype(float) + conf_df['height'].astype(float)
    except:
        conf_df['br_x'] = conf_df['tl_x'] + conf_df['width']
        conf_df['br_y'] = conf_df['tl_y'] + conf_df['height']
    offset_features = ['tl_x', 'tl_y', 'br_x', 'br_y', 'width', 'height']
    req_cols = offset_features + ['Text']
    conf_df[offset_features] = conf_df[offset_features].astype(float)
    new_df = conf_df[req_cols]
    new_df['word_length'] = new_df['Text'].apply(lambda x: len(x))
    new_df['index'] = list(range(len(new_df)))
    finalText = ' '.join(list(new_df['Text'].values))
    json_out = json.loads(new_df.to_json(orient='table', index=False))
    json_out['raw_text'] = finalText
    # print(new_df.head())

    return json_out, new_df, parse_error_flag


def draw_boxes(df, img_path):
    coordinates = []
    img = cv2.imread(img_path)
    org = img.copy()
    out = os.path.splitext(img_path)[0] + '_boxes.jpg'
    for (i, row) in df.iterrows():

        tl_x = row['tl_x']
        tl_y = row['tl_y']
        br_x = row['br_x']
        br_y = row['br_y']
        mx = (int(br_x) - int(tl_x)) / 2
        my = (int(br_y) - int(tl_y)) / 2
        coordinates.append([int(tl_x), int(tl_y),
                            int(br_x), int(br_y), int(mx), int(my)])
    #     cv2.rectangle(img, (int(tl_x), int(tl_y)),
    #                   (int(br_x), int(br_y)), (0, 255, 0), 2)
    # cv2.imwrite(out, img)
    return coordinates


def tesseract_only(filepath):
    # filepath = detect_angle(filepath)
    json_out, final_df, parse_error_flag = get_conf_score(filepath)
    # s3_path_org = None
    # Write image boxes got from tesseract
    coordinates = draw_boxes(final_df, filepath)

    return coordinates


def tesseract_backpass(img, text, coord,dir_p):
    # print(coord)
    # f_name = os.path.splitext(os.path.basename(img_path))[0]
    img_from_img = img
    x1, y1, x2, y2,_,_ = coord
    no_of_words = len(text.split(' '))
    patchwise_info = []
    flag_is_multiple = [False,[]]
    method_flag = False
    if no_of_words > 1:

        all_split_words = text.split(' ')

        # ----------------------------------------------------------------------

        # rects = mser_method_img(img)
        try:
            img_path =os.path.join(dir_p,str(uuid.uuid4())+'.jpg')
            # cv2.imwrite(img_path,img)
            roi = Image.fromarray(img)
            roi.save(img_path, dpi=(300.0, 300.0))
            rects = tesseract_only(img_path)
        except:
            method_flag = True
            rects = mser_method_img(img)
            if len(all_split_words) != len(rects):
                return flag_is_multiple
        rects = sorted(rects, key=lambda x1: x1[0])

        if len(all_split_words) > 1:
            # print('Detection Correction for patch...')
            flag_is_multiple[0] = True

            for rect_index, rect in enumerate(rects):

                if 1:
                    x_new = int(x1) + (rect[0]/2 - 55)/2
                    x_new_1 = int(x1) + (rect[2]/2 - 55) / 2
                    y_new = int(y1) + (rect[1]/2 - 55) / 2
                    y_new_1 = int(y1) + (rect[3]/2 - 55) / 2
                else:
                    x_new = int(x1) + (rect[0] / 2 - 25)
                    x_new_1 = int(x1) + (rect[2] / 2 - 25)
                    y_new = int(y1) + (rect[1] / 2 - 25)
                    y_new_1 = int(y1) + (rect[3] / 2 - 25)
                w_new = x_new_1 - x_new
                h_new = y_new_1 - y_new
                # file_name_img = str(img_ind) + '.' + str(rect_index) + '_' + str(x_new) + '_' + str(y_new) + '_' + str(
                #     w_new) + '_' + str(h_new) + '.jpg'
                # file_name_txt = file_name_img.replace('jpg', 'txt')
                # final_img_path = img_path.replace(
                #     f_name + '.jpg', file_name_img)
                #
                # final_text_path = img_path.replace(
                #     f_name + '.jpg', file_name_txt)

                patch = img_from_img[rect[1]:rect[3], rect[0]:rect[2]]
                top, bottom, left, right = [50] * 4
                color = [255, 255, 255]
                roi = cv2.copyMakeBorder(
                    patch, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)
                # cv2.imwrite(final_img_path, roi)
                try:
                    text2 = pytesseract.image_to_string(
                        roi, lang='eng', config='--psm 6 --oem 3')
                except:
                    text2 = ' '
                patchwise_info.append([x_new,y_new,w_new,h_new, int((w_new + 2 * x_new) / 2), int((h_new + 2 * y_new) / 2),text2])
                # print('Backpass >>>>>>>>>',[x_new,y_new,w_new,h_new, int((w_new + 2 * x_new) / 2), int((h_new + 2 * y_new) / 2),text2])
                # file = open(final_text_path, "w")
                # file.write(text2)
    flag_is_multiple[1] = patchwise_info
    # print ('>>>>>>>>>>>>>>>>>>>>>>>>',flag_is_multiple)
    return flag_is_multiple


def multi_split(value, split_chars):
    last_index = -1
    current_index = 0
    final_list = []
    value_len = len(value)
    for ch in value:
        if(split_chars.find(ch) >= 0):
            if last_index + 1 != current_index:
                final_list.append(value[last_index + 1:current_index])
            final_list.append(ch)
            last_index = current_index
        current_index = current_index + 1
    if last_index == -1:
        final_list.append(value[last_index + 1:current_index])
    elif current_index > 0 and split_chars.find(value[current_index - 1]) == -1:
        final_list.append(value[last_index + 1:current_index])
    if value != "".join(final_list):
        raise Exception("Error : multi_split string do not match '%s' != '%s'" % (
            value, "".join(final_list)))
    return final_list


def delimeter_splitter(gray_image, text, ind_coords, delim=':, '):
    ret_val = [False, []]
    final_list = []
    # print('Entering delimeter func..')
    h, w = gray_image.shape
    patch = gray_image[50:h - 50, 55:w - 55]
    word_data = []
    # f_name = os.path.splitext(os.path.basename(img_path))[0]
    # ind_coords = img_path.split('/')[-1].split('.')[0].split('_')
    # file_index = int(ind_coords[0])
    xmin = int(ind_coords[0])
    ymin = int(ind_coords[1])
    xmax = int(ind_coords[2]) #+ xmin
    ymax = int(ind_coords[3]) #+ ymin
    xmid = int((xmin + xmax) / 2)
    ymid = int((ymin + ymax) / 2)
    value = text
    split_entries = multi_split(value, delim)
    # print("split_entries=%s" % (split_entries))
    if len(split_entries) > 1:
        width_per_char = int((xmax - xmin) / len(value))
        last_xmin = xmin
        # print("width_per_char=%s,last_xmin=%s,%s" %(width_per_char,last_xmin,str(new_entry)))
        for split_entry in split_entries:
            new_entry = [
                last_xmin, ymin, last_xmin +
                (width_per_char * len(split_entry)), ymax, xmid, ymid, split_entry,
                split_entry]  # entry
            last_xmin = last_xmin + (width_per_char * len(split_entry)) + 1
            word_data.append(new_entry)
    else:
        new_entry = [
            xmin, ymin, xmax, ymax, xmid, ymid]
        word_data.append(new_entry)
    # print('&&&&&&&&&&&',word_data)
    if len(word_data) > 1:
        ret_val[0] = True
    else:
        ret_val[0] = False
    if ret_val[0]:
        for ind, coord in enumerate(word_data):
            x = coord[0]
            y = coord[1]
            w = coord[2] - coord[0]
            h = coord[3] - coord[1]
            # print(coord)
            # file_name_img = str(file_index) + '.' + str(ind) + '_' + str(coord[0]) + '_' + str(coord[1]) + '_' + str(
            #     coord[2] - coord[0]) + '_' + str(coord[3] - coord[1]) + '.jpg'
            # file_name_txt = file_name_img.replace('jpg', 'txt')
            # final_img_path = img_path.replace(f_name + '.jpg', file_name_img)
            #
            # final_text_path = img_path.replace(f_name + '.jpg', file_name_txt)

            # roi = gray_image
            # top, bottom, left, right = [50] * 4
            # color = [255, 255, 255]
            # roi = cv2.copyMakeBorder(
            #     patch, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)
            # cv2.imwrite(final_img_path, roi)
            #
            # file = open(final_text_path, "w")
            # file.write(coord[-1])
            final_list.append([x,y,w,h,coord[4],coord[5],coord[-1]])
            # print('Delim Splitter<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<',[x,y,w,h,coord[4],coord[5],coord[-1]])
    ret_val[1] = final_list

    # print('Return func',ret_val,len(word_data))
    return ret_val


def ocr(rgb, coord):
    # model, label_encoder, integer_encoded = grab_model(model)
    # print('&&&&&&&&&&&',img_path)
    img = cv2.imread(img_path)
    img = cv2.resize(img, None, fx=2, fy=2)

    gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # blur = cv2.medianBlur(gray_image, 3)
    roi = img
    try:
        text1 = pytesseract.image_to_string(
            gray_image, lang='eng', config='--psm 9 --oem 3')
        text2 = pytesseract.image_to_string(
            gray_image, lang='eng', config='--psm 6 --oem 3')
    except:
        text1 = text2 = ' '

    # print('TEST**************',text2)

    # text2=text1='error'
    if len(text2) > len(text1):
        text = text2  # +'( :psm6)'
    else:
        text = text2  # +'( :psm8)'
    flag_is_multiple = False
    if len(text.split(' ')) > 1:
        flag_is_multiple = tesseract_backpass(gray_image, text, img_path)
    else:
        for d in delim:

            if len(text.split(d)) > 1 and d != ' ':
                flag_is_multiple = delimeter_splitter(
                    gray_image, text, img_path, delim)
                break

    # flag_is_multiple=tesseract_backpass_v2(text, img_path, rgb)
    # if roi.shape[1] > 1000:
    #     text = pytesseract.image_to_string(output_img, lang='eng')
    #     if len(text) < 50:
    #         text = pytesseract.image_to_string(
    #             output_img, lang='eng', config='--psm 7 --oem 1')
    # else:
    #     text = pytesseract.image_to_string(
    #         output_img, lang='eng', config='--psm 7 --oem 1')
    # print(preds, text)
    f_name = os.path.splitext(os.path.basename(img_path))[0]

    dir_name = os.path.dirname(img_path)
    file_to_delete = dir_name + '/' + f_name + ".txt"
    file = open(dir_name + '/' + f_name + ".txt", "w")
    file.write(text)
    if flag_is_multiple:
        # print('Deleting ',file_to_delete,img_path)
        os.remove(file_to_delete)
        os.remove(img_path)
    return text




def ocr_v2(rgb_dir_p, coord):
    flag_is_multiple = [False, []]
    # print('coord dirp',coord_dir_p)
    rgb,dir_p = rgb_dir_p
    # model, label_encoder, integer_encoded = grab_model(model)
    # print('&&&&&&&&&&&',img_path)
    # print(coord)
    img = process_ocr_v2(coord,rgb)
    # print('Img shape',img.shape)
    try:
        img = cv2.resize(img, None, fx=2, fy=2)

        gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # blur = cv2.medianBlur(gray_image, 3)
        roi = img
        try:
            text1 = pytesseract.image_to_string(
                gray_image, lang='eng', config='--psm 9 --oem 3')
            text2 = pytesseract.image_to_string(
                gray_image, lang='eng', config='--psm 6 --oem 3')
        except:
            text1 = text2 = ' '

        # print('TEST**************',text1)

        # text2=text1='error'
        if len(text2) > len(text1):
            text = text2  # +'( :psm6)'
        else:
            text = text2  # +'( :psm8)'

        if len(text.split(' ')) > 1:
            flag_is_multiple = tesseract_backpass(gray_image, text, coord,dir_p)
        else:

            for d in delim:

                if len(text.split(d)) > 1 and d != ' ':
                    flag_is_multiple = delimeter_splitter(
                        gray_image, text, coord, delim)
                    break
        if not flag_is_multiple[0]:
            flag_is_multiple[1] = [[coord[0],coord[1],coord[2] - coord[0],coord[3] - coord[1],coord[4],coord[5],text]]


        return flag_is_multiple
    except:
        print('Skipped ... ')
        return flag_is_multiple









def ocr_with_vallidation(img_path, model=None):
    model, label_encoder, integer_encoded = grab_model(model)
    # print('TEST**************')
    img = cv2.imread(img_path)
    # print(model.summary())
    preds = do_Prediction(img_path, model,
                          label_encoder, integer_encoded)
    inp_img = Image.open(img_path)
    gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # blur = cv2.medianBlur(gray_image, 3)
    roi = img
    if roi.shape[1] > 1000:
        text = pytesseract.image_to_string(roi, lang='eng')
        if len(text) < 50:
            text = pytesseract.image_to_string(
                roi, lang='eng', config='--psm 8 --oem 1')
    else:
        text = pytesseract.image_to_string(
            roi, lang='eng', config='--psm 8 --oem 1')
    # print(preds, text)
    f_name = os.path.splitext(os.path.basename(img_path))[0]
    f_name_preds = os.path.splitext(os.path.basename(img_path))[0] + '_preds_'
    dir_name = os.path.dirname(img_path)
    file = open(dir_name + '/' + f_name + ".txt", "w")
    file.write(text)
    file_p = open(dir_name + '/' + f_name_preds + ".txt", "w")
    file_p.write(preds)
    return text


def ocr_old(img_path):

    img = cv2.imread(img_path)
    inp_img = Image.open(img_path)
    # output_img = pillowfight.ace(inp_img)
    # text2 = pytesseract.image_to_string(
    #     output_img, lang='eng', config='--psm 8')
    gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur = cv2.medianBlur(gray_image, 3)
    roi = img
    # ret, th = cv2.threshold(gray_image, 127, 255, cv2.THRESH_BINARY)
    # roi = cv2.adaptiveThreshold(gray_image, 255, cv2.ADAPTIVE_THRESH_MEAN_C,
    #                            cv2.THRESH_BINARY, 11, 2)
    # roi = cv2.resize(roi, None, fx=2, fy=2,
    #                  interpolation=cv2.INTER_LINEAR)
    # roi = cv2.GaussianBlur(roi, (5, 5), 0)
    # roi = cv2.copyMakeBorder(
    # roi, 10, 10, 10, 10, cv2.BORDER_CONSTANT, value=[255, 255, 255])
    # roi = cv2.erode(roi, kernel=np.ones((2, 2), np.uint8), iterations=1)
    if roi.shape[1] > 1000:
        text = pytesseract.image_to_string(roi, lang='eng')
        if len(text) < 50:
            text = pytesseract.image_to_string(
                roi, lang='eng', config='--psm 8 --oem 1')
            # words = [pytesseract.image_to_string(
            #     img, lang='eng', config='--psm ' + str(i)) for i in [6, 7, 8, 9, 11]]
            # text = Counter(words).most_common(1)[0][0]
        # if not text:
        #     # text = pytesseract.image_to_string(
        #     #     roi, lang='eng', config='--psm 8')
        #     words = [pytesseract.image_to_string(
        #         img, lang='eng', config='--psm ' + str(i)) for i in [6, 7, 8, 9, 11]]
        #     text = Counter(words).most_common(1)[0][0]
    else:
        text = pytesseract.image_to_string(
            roi, lang='eng', config='--psm 7 --oem 1')
        # words = [pytesseract.image_to_string(
        #     img, lang='eng', config='--psm ' + str(i)) for i in [6, 7, 8, 9, 11]]
        # text = Counter(words).most_common(1)[0][0]
    types = ['tuple', 'float', 'date_time', 'date']
    if get_type(text) in types[2:4]:

        roi = cv2.resize(roi, None, fx=2, fy=2,
                         interpolation=cv2.INTER_LINEAR)
        kernel = np.ones((2, 2), np.uint8)
        # t = text
        # print(text)
        roi = cv2.copyMakeBorder(
            roi, 10, 10, 10, 10, cv2.BORDER_CONSTANT, value=[255, 255, 255])
        roi = cv2.erode(roi, kernel, iterations=1)
        # text = pytesseract.image_to_string(roi, lang='eng', config='--psm 8')
        words = [pytesseract.image_to_string(
            roi, lang='eng', config='--psm ' + str(i) + '--oem 1') for i in [6, 7, 8, 9, 10, 11]]
        text = max(set(words), key=words.count)
    if get_type(text) in types[0:2]:
        roi = cv2.resize(roi, None, fx=2, fy=2,
                         interpolation=cv2.INTER_LINEAR)
        words = [pytesseract.image_to_string(
            roi, lang='eng', config='--psm ' + str(i) + '--oem 1') for i in [6, 7, 8, 9, 10, 11]]
        text = max(set(words), key=words.count)
    if get_type == 'currency':
        roi = cv2.resize(roi, None, fx=3, fy=3,
                         interpolation=cv2.INTER_LINEAR)
        roi = cv2.copyMakeBorder(
            roi, 10, 10, 10, 10, cv2.BORDER_CONSTANT, value=[255, 255, 255])
        roi = cv2.erode(roi, kernel, iterations=1)
        words = [pytesseract.image_to_string(
            roi, lang='eng', config='--psm ' + str(i) + '--oem 1') for i in [6, 7, 8, 9, 10, 11]]
        text = max(set(words), key=words.count)
    # print(text, get_type(text))
    # print(t, text)
    f_name = os.path.splitext(os.path.basename(img_path))[0]
    dir_name = os.path.dirname(img_path)
    # print(dir_name + '/' + f_name + ".txt")
    # print(text)
    # text = str(text).encode('ascii', errors='ignore')
    file = open(dir_name + '/' + f_name + ".txt", "w")
    file.write(text)
    return text


def text_extract(dir_path):
    orig_image = None

    from billiard import Pool
    from multiprocessing.util import Finalize
    _finalizers = []
    p = billiard.Pool(int(no_of_cores) - 1)
    _finalizers.append(Finalize(p, p.terminate))
    image_list = glob.glob(dir_path + '*.jpg', recursive=True)

    try:
        # p.map_async(time.sleep, [1000, 1000, 1000])
        c = 0
        for img_path, out_file in zip(image_list, p.map(ocr, image_list)):
            c += 1
        p.close()
        p.join()
    finally:
        p.terminate()
    # image_list = glob.glob(dir_path + '*.jpg', recursive=True)
    # #with concurrent.futures.ProcessPoolExecutor(max_workers=4) as executor:
    # with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
    #     c = 0
    #     for img_path, out_file in zip(image_list, executor.map(ocr, image_list)):
    #         c += 1
    #         # ocr_.append(out_file)
def cordSort(box1, box2):
    if abs(box1[5] - box2[5]) < 5:
        if box1[4] > box2[4]:
            return 1
        elif box1[4] < box2[4]:
            return -1
    else:
        if box1[5] > box2[5]:
            return 1
        elif box1[5] < box2[5]:
            return -1
    return 0


def text_extract_v2(listt,img):
    orig_image = None
    part_func = partial(ocr_v2, img)
#-----------------------------Using billaiard pool-----------------------------------------------
    from billiard import Pool
    from multiprocessing.util import Finalize
    _finalizers = []
    p = billiard.Pool(int(no_of_cores) - 1)
    _finalizers.append(Finalize(p, p.terminate))
    all_rec = []
    # image_list = glob.glob(dir_path + '*.jpg', recursive=True)

    try:
        # p.map_async(time.sleep, [1000, 1000, 1000])
        # c = 0
        for  coord, res  in zip(listt, p.map(part_func, listt)):
            for r in res[1]:
                if r:
                  all_rec.append(r)
        p.close()
        p.join()
    finally:
        p.terminate()

#------------------------------------------------------------------------------------------------------

#-------------------------------Using Threadpool-----------------------------------------
    #with concurrent.futures.ProcessPoolExecutor(max_workers=4) as executor:
    # with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
    #     c = 0
    #     all_rec = []
    #     for   coord, res in zip(listt,executor.map(part_func, listt)):
    #         # print('>>>>>>>>>>>>>>>>>>>>################',res[1])
    #         for r in res[1]:
    #             if r:
    #               all_rec.append(r)
                # print(all_rec)

            # ocr_.append(out_file)
# ----------------------------------------------------------------------------------------------

    sorted_box_grouped = sorted(
        all_rec, key=functools.cmp_to_key(cordSort))
    sorted_final = sort_line_rowwise(sorted_box_grouped)
    # print(next(sorted_final))
    return sorted_final




def text_extract_with_validation(dir_path, model):
    image_list = glob.glob(dir_path + '*.jpg', recursive=True)
    for img in image_list:
        ocr_with_vallidation(img, model)


if __name__ == '__main__':
    dir_path = '/home/user2/Desktop/ip_ocr_engine/crop_files/26767/'
    text_extract(dir_path)