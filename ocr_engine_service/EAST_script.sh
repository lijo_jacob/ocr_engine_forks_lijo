pip3 install tensorflow==1.7.0
pip3 install tensorflow-tensorboard==1.5.1
pip3 install https://github.com/mind/wheels/releases/download/tf1.6-cpu/tensorflow-1.6.0-cp36-cp36m-linux_x86_64.whl


cd src/ocr_extraction/utilities/
git clone https://github.com/argman/EAST.git
apt-get install zip  -y
cd EAST/lanms
make
cd ../
fileid="0B3APw5BZJ67ETHNPaU9xUkVoV0U"
filename="east_model_files.zip"
rm ./cookie
curl -c ./cookie -s -L "https://drive.google.com/uc?export=download&id=${fileid}" > /dev/null
curl -Lb ./cookie "https://drive.google.com/uc?export=download&confirm=`awk '/download/ {print $NF}' ./cookie`&id=${fileid}" -o ${filename}
unzip -n east_model_files.zip
rm east_model_files.zip
rm -rf  nets
rm run_demo_server.py
rm model.py
rm icdar.py
rm data_util.py
rm eval.py
cp ../east_dependencies/run_demo_server.py ../EAST/
cp ../east_dependencies/model.py ../EAST/
cp ../east_dependencies/icdar.py ../EAST/
cp ../east_dependencies/data_util.py ../EAST/
cp ../east_dependencies/eval.py ../EAST/
cp -r  ../east_dependencies/nets_EAST ../EAST/nets


