"""rcm_ai_service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('ocr_extraction/', include('src.ocr_extraction.urls')),
    path('spell_check/', include('src.spell_check.urls')),
    path('status', views.GetStatus.as_view(), name='get_status'),
    path('connection_status', views.GetConnectionStatus.as_view(), name='get_status'),
    path('git_status', views.GitStatus.as_view(), name='git_status'),
    path('skew_correct', views.ImgSkewcorrectApi.as_view(), name='skew_correct')
]
