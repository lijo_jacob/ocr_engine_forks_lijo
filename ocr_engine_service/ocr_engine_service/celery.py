from __future__ import absolute_import, unicode_literals
import os
import tensorflow as tf
from celery import Celery
from django.conf import settings


#print('pyamqp://' + os.environ['RABBITMQ_USERNAME'] +':' + os.environ['RABBITMQ_PASSWORD'] + '@' + os.environ['RABBITMQ_HOSTNAME'] + '//')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
app = Celery('rcmb_job_api',
                broker='pyamqp://' + os.environ.get('RABBITMQ_USERNAME','') +':' + os.environ.get('RABBITMQ_PASSWORD','') + '@' + os.environ.get('RABBITMQ_HOSTNAME','') + '//',
                include=['job_man.tasks'])


app.config_from_object(settings, namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

#def startOCRWorker():
# argv = [
        
#         'worker',
            
#         '--loglevel=INFO',
#         '-n {}[%i]@%h'.format("ocr_worker"),
#         '-Qocr_queue'

# ]
# app.worker_main(argv) 

#import concurrent.futures
#executor = concurrent.futures.ProcessPoolExecutor(1)#max_workers=2
#ocrTask      = executor.submit(startOCRWorker)

@app.task
def nlp_dummy_task():
    print("nlp_dummy_task")
    return 5