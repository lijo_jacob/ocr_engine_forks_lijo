import os
import boto3
import shutil

from django.conf import settings
from rest_framework.views import APIView
from rest_framework.response import Response
from common.rcm_connections import RcmbDbConnection
from src.ocr_extraction.utilities.ocr_utils_multi_proc import skew_correction, detect_angle_new
from src.ocr_extraction.utilities.utils import convert_image2pdf, merge_pdfs


class GetStatus(APIView):
    def post(self, request):
        d = {"status": "success"}
        return Response(d)

    def get(self, request):
        d = {"status": "success"}
        return Response(d)


class GetConnectionStatus(APIView):

    def post(self, request):
        from pymongo import MongoClient
        response = {"status": "success"}
        try:
            s3 = boto3.client(
                's3', aws_access_key_id=os.environ["S3_AWS_KEY"], aws_secret_access_key=os.environ["S3_AWS_SECRET"])
            s3BucketLocationRes = s3.get_bucket_location(
                Bucket=os.environ["S3_BUCKET_NAME"])

            response["s3Response"] = s3BucketLocationRes
        except:
            response["s3Exception"] = "S3Error"
            response["status"] = "ERROR"

        try:
            obj = RcmbDbConnection()
            mresponse = obj.get_rcmb_schema()
            response["mongoSchemaLength"] = len(mresponse["resources"])
        except:
            response["mongoError"] = "MongoDB Error"
            response["status"] = "ERROR"

        return Response(response)


class GitStatus(APIView):

    def post(self, request):
        d = {"status": "success"}
        return Response(d)

    def get(self, request):
        d = {"status": "success"}
        with open(os.path.join(settings.BASE_DIR, 'ocr_engine_service', 'git_status.txt')) as f:
            data = f.read()
        d['data'] = data
        return Response(d)


class ImgSkewcorrectApi(APIView):

    def post(self, request):
        from scripts.page_rotation_init_train import download_img_fpath_from_s3
        from src.ocr_extraction.utilities.s3_upload import connect_to_s3, upload_to_s3
        dict_out = {"status": "success"}
        data = request.data
        dParam = data.dict()
        base_name = dParam['file_url']
        print('*****', base_name)
        # fpath = skew_correction_only(download_img_fpath_from_s3(base_name))
        extension = os.path.splitext((base_name))[-1]
        out_name = os.path.join(os.path.split(base_name)[0], os.path.splitext(
            os.path.basename(base_name))[0] + '_out' + extension)

        fpath = download_img_fpath_from_s3(base_name)
        print('Local File Path: ', fpath)
        if extension == '.pdf':
            from src.ocr_extraction.utilities.non_layered_pdf_util import convert_pdf_img
            filename_list = convert_pdf_img(fpath)
            print('\n' * 1, '*** FILENAMELIST ***: ', filename_list, '\n' * 1)

            for fp in filename_list:
                try:
                    fp_corr = skew_correction(detect_angle_new(fp))
                except Exception as e:
                    print('*** Skew COrrection Error ***',
                          str(e), 'Filepath:', fp)
                list_pdfs = [convert_image2pdf(fp_corr)
                             for fp_corr in filename_list]
            print('\n' * 1, 'LIST_PDFS', list_pdfs, '\n' * 1)
            result = merge_pdfs(list_pdfs)

        else:
            result = download_img_fpath_from_s3(base_name)
            try:
                result = skew_correction(detect_angle_new(result))
            except Exception as e:
                print('*** Skew COrrection Error ***', str(e))

        connect_to_s3(os.environ['S3_AWS_KEY'], os.environ['S3_AWS_SECRET'],
                      settings.S3_BUCKET_NAME_IMG, subdirectory=os.path.split(base_name)[0])
        upload_to_s3(result, os.path.basename(out_name))

        dict_out['S3_url'] = os.path.join(
            settings.S3_BUCKET_NAME_IMG, out_name)

        # remove directory
        try:
            if extension == '.pdf':
                shutil.rmtree(os.path.split(list_pdfs[0])[0])
            else:
                shutil.rmtree(os.path.split(result)[0])
        except OSError as e:
            print ("Error: %s - %s." % (e.filename, e.strerror))

        return Response(dict_out)

    def get(self, request):
        dict_out = {"status": "success"}
        with open(os.path.join(settings.BASE_DIR, 'ocr_engine_service', 'git_status.txt')) as f:
            data = f.read()
        dict_out['data'] = data
        return Response(dict_out)
