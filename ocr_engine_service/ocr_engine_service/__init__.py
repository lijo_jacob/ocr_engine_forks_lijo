from __future__ import absolute_import
from common.initializers import set_environment_vars_from_kms
import os
if os.environ.get("INIT_OVER") != "true" :
    set_environment_vars_from_kms()
from .celery import app as celery_app
__all__ = ('celery_app',)
print("-------------- init.py ------------------")
