import os
import json
import hashlib
import pymongo
import requests
from pymongo import MongoClient
from common.variables import dml_log_status_code_url


class DmlService:
    def __init__(self):
        # self.dml_url = os.environ['DML_URL']
        self.uri = os.environ['RCMB_DB_URI']
        self.con = MongoClient(self.uri)
        self.db = self.con[os.environ['RCMB_DB']]  # master db - connection
        self.client_db = None
        # set_iam_token()

    def make_http_post(self, sUrl, data=None, dHeaders={}):
        oResponse = requests.post(sUrl, json=data, headers=dHeaders)
        return oResponse

    def set_iam_token(self):
        dData = {
            "userName": os.environ["JOB_MANAGER_USER"],
            "userPassword": os.environ["JOB_MANAGER_PASSWORD"]
        }
        sUrl = os.environ["IAM_URL"]
        oResponse = self.make_http_post(sUrl, dData)
        os.environ['TOKEN'] = oResponse.json().get('token')

    def get_dml_response(self, sUrl, dData=None, dHeaders={}):
        if not dHeaders.get('Authorization'):
            if not os.environ.get('TOKEN'):
                self.set_iam_token()
            dHeaders['Authorization'] = os.environ['TOKEN']
        if os.environ.get('DML_URL') not in sUrl:
            sUrl = os.environ.get('DML_URL') + sUrl
        oResponse = self.make_http_post(sUrl, dData, dHeaders)
        if oResponse.status_code != 200:
            print("broken api : ", sUrl, dData)
        dResult = oResponse.json()
        print('dResult', dResult)
        return dResult

    def get_hash(self, key):
        return hashlib.md5(key.encode('utf8')).hexdigest()

    def filter_dFileInfo(self, dFileInfo, status, msg, page_count,ocr_start_time,ocr_end_time,ocr_duration):
        if dFileInfo:
            data_dict = {"client_id": dFileInfo.get('client_id'),
                         "iExtractedJsonLogId": dFileInfo.get('iExtractedJsonLogId'),
                         "file_url": dFileInfo.get('file_url'),
                         "dump_status": status,
                         "dump_msg": msg,
                         }
            if not [x for x in data_dict.values() if not x]:
                data_dict['page_count'] = page_count
                data_dict['ocr_start_time'] = ocr_start_time
                data_dict['ocr_end_time'] = ocr_end_time
                data_dict['ocr_duration'] = ocr_duration


                return data_dict
        print('\n\nNo enough information to log status! \n#dFileInfo:{} \n#Status:{} \n#Msg:{}'.format(
            dFileInfo, status, msg))
        return {}


def update_status_code(dFileInfo=None, status=0, msg='', page_count=None,ocr_start_time = None,ocr_end_time = None,ocr_duration = None):
    dmlObj = DmlService()
    return dmlObj.get_dml_response(dml_log_status_code_url, dmlObj.filter_dFileInfo(dFileInfo, status, msg, page_count,ocr_start_time,ocr_end_time,ocr_duration))
