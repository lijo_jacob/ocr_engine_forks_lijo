import boto3
import botocore

import os


def s3_call(MY_ACCESS_KEY_ID, MY_SECRET_ACCESS_KEY, BUCKET):
    s3 = boto3.resource('s3',
                        aws_access_key_id=MY_ACCESS_KEY_ID,
                        aws_secret_access_key=MY_SECRET_ACCESS_KEY
                        # region_name=AWS_REGION
                        )

    # Bucket to use
    bucket = s3.Bucket(BUCKET)

    # List objects within a given prefix
    for obj in bucket.objects.filter(Delimiter='/', Prefix='ai_data/new_training/hunspell_dic/'):
        path, filename = os.path.split(obj.key)
        print('working')
        print(obj.key)
        s3.meta.client.download_file(BUCKET, obj.key, os.getcwd(
        ) + '/src/spell_check/static_data/gazetters/' + filename)
