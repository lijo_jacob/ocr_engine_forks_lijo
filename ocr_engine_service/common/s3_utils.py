import os
import boto3

from common.s3_constants import (
    S3_AWS_KEY, S3_AWS_SECRET, S3_BUCKET_NAME
)
from src.general.utils import check_dir_exist


def upload_file_s3(s3, local_file_path, s3_bucket_name, s3_file_path):
    s3.upload_file(local_file_path, s3_bucket_name, s3_file_path)


def get_s3_client(aws_access_key_id=S3_AWS_KEY, aws_secret_access_key=S3_AWS_SECRET):
    return boto3.client('s3', aws_access_key_id=aws_access_key_id,
                        aws_secret_access_key=aws_secret_access_key)


def s3_upload_model(local_file_path, bucket_name=S3_BUCKET_NAME):
    # s3_model_path = os.path.join(
    #     'models/', system_id, os.path.basename(local_file_path))
    upload_file_s3(get_s3_client(), local_file_path,
                   bucket_name, local_file_path)
    return local_file_path


def s3_download_model(s3_path, bucket_name=S3_BUCKET_NAME):
    # local_file_path = os.path.join('data', s3_model_path)
    check_dir_exist(os.path.dirname(s3_path))
    s3 = get_s3_client()
    s3.download_file(bucket_name, s3_path, s3_path)
    return s3_path


def s3_download_file(s3_path, bucket_name, file_save_path):
    check_dir_exist(os.path.dirname(file_save_path))
    s3 = get_s3_client()
    s3.download_file(bucket_name, s3_path, file_save_path)
    return file_save_path


def s3_list_filepaths(s3_path, bucket_name):
    print(s3_path)
    s3 = get_s3_client()
    content_list = s3.list_objects_v2(
        Bucket=bucket_name, Prefix=s3_path).get('Contents')
    return content_list
