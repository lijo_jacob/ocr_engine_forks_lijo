class RcmbConnections(object):
    '''docstring for RcmbConnections'''

    def __init__(self):
        super(RcmbConnections, self).__init__()
        # self.arg = arg

    def get_kms_connection(self):
        from common.rcmb_constants import MY_ACCESS_KEY_ID, MY_SECRET_ACCESS_KEY, AWS_REGION
        import os
        import boto3
        import json
        client = boto3.client('ssm',
                              aws_access_key_id=MY_ACCESS_KEY_ID,
                              aws_secret_access_key=MY_SECRET_ACCESS_KEY,
                              region_name=AWS_REGION)
        response = client.get_parameters(
            #Names=['rcmb-dml-%s' % (os.environ.get('App_Env','dev'))],
            Names=['rcmb-dml-%s' % ( os.environ.get('App_Env','dev2') ) ],
            WithDecryption=True
        )
        credentials = response['Parameters'][0]['Value']
        data = json.loads(credentials)
        # print(data)
        return data

    def set_params_from_kms(self):
        import os
        from common.initializers import set_environment_vars_from_kms
        if os.environ.get("INIT_OVER") != "true" :
            set_environment_vars_from_kms()
        # print(dKms)

        self.MONGO_HOST = os.environ['MONGO_HOST']
        self.MONGO_USERNAME = os.environ['MONGO_USERNAME']
        self.MONGO_PASSWORD = os.environ['MONGO_PASSWORD']
        self.MONGO_DB = os.environ['MONGO_DB']

        self.RCMB_DB_URI = os.environ["RCMB_DB_URI"]
        self.RCMB_DB = os.environ["RCMB_DB"]
        self.RCMB_DB_HOST = os.environ["RCMB_DB_HOST"]
        self.RCMB_DB_PORT = os.environ["RCMB_DB_PORT"]
        self.RCMB_DB_USERNAME = os.environ["RCMB_DB_USERNAME"]
        self.RCMB_DB_PASSWORD = os.environ["RCMB_DB_PASSWORD"]

        self.MONGODB_SCHEDULER_DB = os.environ["MONGODB_SCHEDULER_DB"] 

        self.S3_AWS_KEY = os.environ["S3_AWS_KEY"]
        self.S3_AWS_SECRET = os.environ["S3_AWS_SECRET"]
        self.S3_BUCKET_NAME = os.environ["S3_BUCKET_NAME"]
        self.S3_BUCKET_COMMON = os.environ["S3_BUCKET_COMMON"]

        self.DRILL_HOSTNAME = os.environ["DRILL_HOSTNAME"]
        self.DRILL_PORT = os.environ["DRILL_PORT"] 
        self.DRILL_USERNAME = os.environ["DRILL_USERNAME"]
        self.DRILL_PASSWORD = os.environ["DRILL_PASSWORD"]
        self.DRILL_STORENAME = os.environ["DRILL_STORENAME"]  

        self.KAFKA_HOSTNAME = os.environ["KAFKA_HOSTNAME"]
        self.KAFKA_PORT = os.environ["KAFKA_PORT"]
        self.KAFKA_BROKER_LIST = os.environ["KAFKA_BROKER_LIST"]
        self.KAFKA_AGENT_REQUEST_TOPIC = os.environ["KAFKA_AGENT_REQUEST_TOPIC"]
        self.KAFKA_AGENT_RESPONSE_TOPIC = os.environ["KAFKA_AGENT_RESPONSE_TOPIC"]

        self.FLOWER_USERNAME = os.environ["FLOWER_USERNAME"]
        self.FLOWER_PASSWORD = os.environ["FLOWER_PASSWORD"]

        self.RABBITMQ_HOSTNAME = os.environ["RABBITMQ_HOSTNAME"]
        self.RABBITMQ_USERNAME = os.environ["RABBITMQ_USERNAME"]
        self.RABBITMQ_PASSWORD = os.environ["RABBITMQ_PASSWORD"]


class RcmbDbConnection(RcmbConnections):
    """docstring for RembDbConnection"""

    def __init__(self, arg=None):
        super(RcmbDbConnection, self).__init__()
        # self.arg = arg
        self.set_params_from_kms()
        self.oMasterDb = self.get_master_db_connection()

    def get_master_db_connection(self):
        from pymongo import MongoClient
        return MongoClient(self.RCMB_DB_URI)[self.RCMB_DB]

    def get_rcmb_schema(self):
        dTables = self.oMasterDb['rcmb_settings'].find_one({'name':'rcmb_schema'})['value']
        dSchema = {
                      "name": "rcmb_schema",
                      "resources": dTables
                      }
        return dSchema

def get_rcmb_schema_as_json():
    oConn = RcmbDbConnection()
    return oConn.get_rcmb_schema()


def get_rcmb_constant_json():
    oConn = RcmbConnections()
    return oConn.get_kms_connection()    