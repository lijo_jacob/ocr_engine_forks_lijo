from rest_framework.response import Response
from common.rcmb_constants import ERROR, FAILED, SUCCESS


def get_formatted_response(sStatus, data=None, sMessage=None, sDetails=None):
    dResponse = {"status" 	: sStatus}
    if data:
        dResponse["data"] = data
    if sMessage:
        dResponse["message"] = sMessage
    if sDetails:
        dResponse["details"] = sDetails
    return Response(dResponse)


def check_for_keys_in_data(dData, lRequiredKeys):
    lDataKeys = list(dData.keys())
    for sKey in lRequiredKeys:
        if sKey not in lDataKeys:
            return get_formatted_response(ERROR, "key '{key}' not found in request data".format(key=sKey))
        if not dData[sKey] and dData[sKey] != False:
            return get_formatted_response(ERROR, "key '{key}' found null request data".format(key=sKey))
    return False
