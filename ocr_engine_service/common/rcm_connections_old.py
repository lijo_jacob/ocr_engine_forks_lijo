class RcmbConnections(object):
    '''docstring for RcmbConnections'''

    def __init__(self):
        super(RcmbConnections, self).__init__()
        # self.arg = arg

    def get_kms_connection(self):
        from common.rcmb_constants import MY_ACCESS_KEY_ID, MY_SECRET_ACCESS_KEY, AWS_REGION
        import os
        import boto3
        import json
        client = boto3.client('ssm',
                              aws_access_key_id=MY_ACCESS_KEY_ID,
                              aws_secret_access_key=MY_SECRET_ACCESS_KEY,
                              region_name=AWS_REGION)
        response = client.get_parameters(
            Names=['rcmb-dml-%s' % (os.environ['App_Env'])],
            WithDecryption=True
        )
        credentials = response['Parameters'][0]['Value']
        data = json.loads(credentials)
        # print(data)
        return data

    def set_params_from_kms(self):
        import os
        dKms = self.get_kms_connection()
        # print(dKms)
        self.MONGO_HOST = dKms['mongo_rcmb']['host']
        self.MONGO_USERNAME = dKms['mongo_rcmb']['user']
        self.MONGO_PASSWORD = dKms['mongo_rcmb']['passwd']
        self.MONGO_DB = dKms['mongo_rcmb']['db']

        self.RCMB_DB_URI = dKms['mongo_rcmb']['db_uri']
        self.RCMB_DB = dKms['mongo_rcmb']['db']
        self.RCMB_DB_HOST = dKms['mongo_rcmb']['db_host']
        self.RCMB_DB_PORT = dKms['mongo_rcmb']['port']
        self.RCMB_DB_USERNAME = dKms['mongo_rcmb']['user']
        self.RCMB_DB_PASSWORD = dKms['mongo_rcmb']['passwd']

        self.MONGODB_SCHEDULER_DB = dKms['mongo_rcmb']['db']

        self.S3_AWS_KEY = dKms['s3']['aws_key']
        self.S3_AWS_SECRET = dKms['s3']['aws_secret']
        self.S3_BUCKET_NAME = dKms['s3']['bucket_name']
        self.S3_BUCKET_COMMON = dKms['s3']['bucket_common']

        self.DRILL_HOSTNAME = dKms['drill']['hostname']
        self.DRILL_PORT = dKms['drill']['port']
        self.DRILL_USERNAME = dKms['drill']['username']
        self.DRILL_PASSWORD = dKms['drill']['password']
        self.DRILL_STORENAME = dKms['drill']['storename']

        self.KAFKA_HOSTNAME = dKms['kafka']['consumer_hostname']
        self.KAFKA_PORT = dKms['kafka']['kafka_port_number']
        self.KAFKA_BROKER_LIST = dKms['kafka']['kafka_broker_list']
        self.KAFKA_AGENT_REQUEST_TOPIC = dKms['kafka']['kafka_topic_producer']
        self.KAFKA_AGENT_RESPONSE_TOPIC = dKms['kafka']['kafka_topic_consumer']

        self.FLOWER_USERNAME = dKms['flower']['username']
        self.FLOWER_PASSWORD = dKms['flower']['password']

        self.RABBITMQ_HOSTNAME = dKms['rabbit_mq']['host_name']
        self.RABBITMQ_USERNAME = dKms['rabbit_mq']['username']
        self.RABBITMQ_PASSWORD = dKms['rabbit_mq']['password']


class RcmbDbConnection(RcmbConnections):
    """docstring for RembDbConnection"""

    def __init__(self, arg=None):
        super(RcmbDbConnection, self).__init__()
        # self.arg = arg
        self.set_params_from_kms()
        self.oMasterDb = self.get_master_db_connection()

    def get_master_db_connection(self):
        from pymongo import MongoClient
        return MongoClient(self.RCMB_DB_URI)[self.RCMB_DB]

    def get_rcmb_schema(self):
        dTables = self.oMasterDb['rcmb_settings'].find_one({'name':'rcmb_schema'})['value']
        dSchema = {
                      "name": "rcmb_schema",
                      "resources": dTables
                      }
        return dSchema

def get_rcmb_schema_as_json():
    oConn = RcmbDbConnection()
    return oConn.get_rcmb_schema()