# Extraction errors
ERR_001 = 'ERR_001---PDF Extraction Error---{desp}'
ERR_002 = 'ERR_002---OCR Extraction Error---{desp}'
ERR_003 = 'ERR_003---HTML Extraction Error---{desp}'

# Prediction errors
ERR_101 = 'ERR_101---Prediction Error---{desp}'

# Training Error
ERR_201 = 'ERR_201---Training Error---{desp}'

# Conversion error
ERR_301 = 'ERR_301---Conversion Error---{desp}'
ERR_311 = 'ERR_311---Update Data Conversion Error---{desp}'

# NLP Spell check error
ERR_401 = 'ERR_401---NLP Spell check error---{desp}'