import os
S3_AWS_KEY = os.environ["S3_AWS_KEY"]
S3_AWS_SECRET = os.environ["S3_AWS_SECRET"]
S3_BUCKET_NAME = os.environ["S3_BUCKET_NAME"] 
S3_SYSTEM_MAPPING_BUCKET = os.environ["S3_BUCKET_COMMON"] 