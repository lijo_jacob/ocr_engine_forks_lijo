import os
import time
import random
import logging
from ast import literal_eval
from dateutil.parser import parse
from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage

from common.rcm_connections import RcmbDbConnection
from common.s3_utils import (
    s3_download_file, s3_upload_model, s3_list_filepaths
)
from .variables import S3_RETRY_LIMIT
logger = logging.getLogger(__name__)


def check_dir_exist(directory):
    '''
    Function to check whether a directory exists
    If not exists, creates a new one
    '''
    if not os.path.exists(directory):
        os.makedirs(directory)


def get_temp_fpath(data):
    rand_name = "%0.12d" % random.randint(0, 999999999999)
    extn = os.path.splitext(str(data))[1]
    return os.path.join(settings.MEDIA_ROOT, 'tmp', extn[1:], rand_name + extn)


def save_file_locally(data):
    '''
    saves the received file to a directory and
    returns the full path of the saved file
    '''
    fpath = get_temp_fpath(data)
    path = default_storage.save(fpath, ContentFile(data.read()))
    return path


def get_bucket_name(common_bucket):
    oRcmbDbConnection = RcmbDbConnection()
    oRcmbDbConnection.set_params_from_kms()
    if common_bucket:
        return oRcmbDbConnection.S3_BUCKET_COMMON
    return oRcmbDbConnection.S3_BUCKET_NAME


def fpath_from_s3(s3_url, fpath=None, common_bucket=None):
    # if not fpath:
    #     fpath = get_temp_fpath(s3_url)
    # bucket_name = get_bucket_name(common_bucket)
    # # print(bucket_name)
    # return s3_download_file(s3_url, bucket_name, fpath)
    loc_path = None
    ctr = S3_RETRY_LIMIT

    while (ctr):
        try:
            if not fpath:
                 fpath = get_temp_fpath(s3_url)
            if common_bucket == None:
                bucket_name = get_bucket_name(common_bucket)
            else:
                bucket_name = common_bucket
            print(bucket_name)
            loc_path = s3_download_file(s3_url, bucket_name, fpath)
            break
        except Exception as e:
            ctr -= 1
            sleep_time = random.randint(1, 10)
            print('S3 DOWNLOAD FAILED, RETRYING IN ', sleep_time, ' seconds...')

            time.sleep(sleep_time)
    return loc_path


def s3_list_contents(s3_url):
    bucket_name = get_bucket_name(common_bucket=True)
    contents = s3_list_filepaths(s3_url, bucket_name)
    file_list = [item['Key'] for item in contents if item['Key'][-1] != '/']
    return file_list


def upload_to_s3(local_file_path, common_bucket=None):
    bucket_name = get_bucket_name(common_bucket)
    return s3_upload_model(local_file_path, bucket_name)


def is_date(string):
    try:
        parse(string)
        return True
    except ValueError:
        return False


def any_currency(s, curr="¥$€£"):
    return any(c in s for c in curr)


def get_type(s):
    try:
        k = literal_eval(s)
        val_type = type(k).__name__
    except Exception as e:
        if e.args[0] == 'invalid token':
            try:
                s = int(s)
            except:
                pass
        val_type = type(s).__name__

    if(val_type == 'str'):
        if(str(s).isalpha()):
            str_category = 'alpha'
        elif(is_date(str(s))):
            str_category = 'date_time'
        elif(str(s).isalnum()):
            str_category = 'alpha_numeric'
        elif(any_currency(s)):
            str_category = 'currency'
        else:
            str_category = 'string'

        return str_category
    elif val_type == 'tuple':
        return 'string'
    else:
        return val_type


def log_error(err_code):
    def decorate(f):
        def applicator(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except Exception as e:
                logger.exception(err_code.format(desp=str(e)))
                raise e
        return applicator
    return decorate


def get_extn(fpath):
    return os.path.splitext(fpath)[1][1:].lower()


def get_file_hash(file_path):
    print('ORG FILEPATH', file_path)
    file_path = os.path.join(os.getcwd(), file_path)
    print('FILEPATHIS*************', file_path)
    import hashlib
    BLOCKSIZE = 65536
    hasher = hashlib.md5()
    with open(file_path, 'rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(BLOCKSIZE)
    return hasher.hexdigest()
