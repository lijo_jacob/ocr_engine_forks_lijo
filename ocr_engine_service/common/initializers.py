AWS_REGION = Region = 'us-west-2'
import os
from .rcmb_constants import MY_ACCESS_KEY_ID,MY_SECRET_ACCESS_KEY
# os.environ['App_Env'] = 'dev2'
def kms_connection():
    import boto3
    import json
    client = boto3.client('ssm',
                          aws_access_key_id=MY_ACCESS_KEY_ID,
                          aws_secret_access_key=MY_SECRET_ACCESS_KEY,
                          region_name=Region)
    response = client.get_parameters(
        Names=['rcmb-dml-%s' % (os.environ['NODE_ENV'])],
        WithDecryption=True
    )
    print("App_Env value is =="+os.environ['NODE_ENV'])
    print(response['Parameters'])
    credentials = response['Parameters'][0]['Value']
    data = json.loads(credentials)
    # print(data)
    return data


def set_environment_vars_from_kms():
    import os
    dKms                                     = kms_connection()
    os.environ['MONGO_HOST']                 = dKms['mongo_rcmb']['host']
    os.environ['MONGO_USERNAME']             = dKms['mongo_rcmb']['user']
    os.environ['MONGO_PASSWORD']             = dKms['mongo_rcmb']['passwd']
    os.environ['MONGO_DB']                   = dKms['mongo_rcmb']['db']

    os.environ["RCMB_DB_URI"]                = dKms['mongo_rcmb']['db_uri']
    os.environ["RCMB_DB"]                    = dKms['mongo_rcmb']['db']
    os.environ["RCMB_DB_HOST"]               = dKms['mongo_rcmb']['db_host']
    os.environ["RCMB_DB_PORT"]               = dKms['mongo_rcmb']['port']
    os.environ["RCMB_DB_USERNAME"]           = dKms['mongo_rcmb']['user']
    os.environ["RCMB_DB_PASSWORD"]           = dKms['mongo_rcmb']['passwd']

    os.environ["MONGODB_SCHEDULER_DB"]       = dKms['mongo_rcmb']['db']

    os.environ["S3_AWS_KEY"]                 = dKms['s3']['aws_key']
    os.environ["S3_AWS_SECRET"]              = dKms['s3']['aws_secret']
    os.environ["S3_BUCKET_NAME"]             = dKms['s3']['bucket_name']
    os.environ["S3_BUCKET_COMMON"]           = dKms['s3']['bucket_common']

    os.environ["DRILL_HOSTNAME"]             = dKms['drill']['hostname']
    os.environ["DRILL_PORT"]                 = dKms['drill']['port']
    os.environ["DRILL_USERNAME"]             = dKms['drill']['username']
    os.environ["DRILL_PASSWORD"]             = dKms['drill']['password']
    os.environ["DRILL_STORENAME"]            = dKms['drill']['storename']

    os.environ["KAFKA_HOSTNAME"]             = dKms['kafka']['consumer_hostname']
    os.environ["KAFKA_PORT"]                 = dKms['kafka']['kafka_port_number']
    os.environ["KAFKA_BROKER_LIST"]          = dKms['kafka']['kafka_broker_list']
    os.environ["KAFKA_AGENT_REQUEST_TOPIC"]  = dKms['kafka']['kafka_topic_producer']
    os.environ["KAFKA_AGENT_RESPONSE_TOPIC"] = dKms['kafka']['kafka_topic_consumer']

    os.environ["FLOWER_USERNAME"]            = dKms['flower']['username']
    os.environ["FLOWER_PASSWORD"]            = dKms['flower']['password']

    os.environ["RABBITMQ_HOSTNAME"]          = dKms['rabbit_mq']['host_name']
    os.environ["RABBITMQ_USERNAME"]          = dKms['rabbit_mq']['username']
    os.environ["RABBITMQ_PASSWORD"]          = dKms['rabbit_mq']['password']

    os.environ["STATIC_URL"]                 = dKms['STATIC_URL']
    # os.environ["MEDIA_ROOT"]                 = dKms['MEDIA_ROOT']
    #----------------------------------> To fix---------------------------------------
    os.environ["MEDIA_ROOT"]                 = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))+'/media'
    os.environ["MEDIA_URL"]                  = dKms['MEDIA_URL']
    os.environ["S3_BUCKET_NAME_IMG"]         = dKms['S3_BUCKET_NAME_IMG']
    os.environ["S3_BUCKET_NAME"]             = dKms['S3_BUCKET_NAME']
    os.environ["model_path"]                 = dKms['model_path']
    os.environ["model_json_path"]            = dKms['model_json_path']
    os.environ["OCR_UPLOAD_URL"]             = dKms['OCR_UPLOAD_URL']
    # os.environ["DOC_PRED_TRAINING_FLASK_URL"]= dKms['DOC_PRED_TRAINING_FLASK_URL']
    # os.environ["MASTER_DB_URI"]              = dKms['MASTER_DB_URI']
    os.environ["MASTER_COLLECTION"]          = dKms['MASTER_COLLECTION']
    os.environ["INIT_OVER"] = "true"


#os.path.dirname(os.path.dirna    me(os.path.dirname(os.path.abspath(__file__))))+'/media'