import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '*%w$sm=)0t3(alyu&0kyepm1wj-$ziinwk@5s16c#ytcn+wq_-'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # external
    'rest_framework',
    'django_extensions',
    # internal

]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'ocr_engine_service.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

if os.environ['NODE_ENV'] == 'dev2':
    os.environ["DML_URL"] = "https://dev2-datamgmt.iinerds.com/"
    os.environ["JOB_MANAGER_USER"] = "sreejith.sp@iinerds.com"
    os.environ["JOB_MANAGER_PASSWORD"] = "123456"
    os.environ["IAM_URL"] = "https://dev-iam.iinerds.com/reLogin"

    os.environ['App_Env'] = 'dev2'
    os.environ['S3_AWS_KEY'] = 'AKIAJILHNXF6NS77TMRA'
    os.environ['S3_AWS_SECRET'] = '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'
    os.environ['S3_BUCKET_NAME'] = 'data-rcmbrain-dev2'
    SECRET_KEY = '*%w$sm=)0t3(alyu&0kyepm1wj-$ziinwk@5s16c#ytcn+wq_-'
    # ocr urls
    # OCR_UPLOAD_URL = 'https://dev2-ocrengine.iinerds.com/img_to_json/'
    OCR_UPLOAD_URL = 'https://dev2-ocrengine.rcmbrain.com/img_to_json/'
    NLP_URL = 'https://dev2-nlpengine.rcmbrain.com/git_status'
    S3_BUCKET_NAME = 'rcmb-ai-data-dev2'
    # Image upload bucket. This has been added here because of the code merge of image deskew
    S3_BUCKET_NAME_IMG = 'data-rcmbrain-dev2'
    DOC_PRED_TRAINING_FLASK_URL = 'http://dev2-rcmbai-training.iinerds.com'
    MASTER_DB_URI = 'mongodb://admin:T3QXVUbK4VHh@60.10.20.190:27017/rcmb_local?authSource=admin&authMechanism=SCRAM-SHA-1'
    MASTER_COLLECTION = 'rcmb_local'

elif os.environ['NODE_ENV'] == 'dev':

    os.environ["DML_URL"] = "https://dev-datamgmt.iinerds.com/"
    os.environ["JOB_MANAGER_USER"] = "sreejith.sp@iinerds.com"
    os.environ["JOB_MANAGER_PASSWORD"] = "123456"
    os.environ["IAM_URL"] = "https://dev-iam.iinerds.com/reLogin"

    os.environ['App_Env'] = 'dev'
    os.environ['S3_AWS_KEY'] = 'AKIAJILHNXF6NS77TMRA'
    os.environ['S3_AWS_SECRET'] = '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'
    os.environ['S3_BUCKET_NAME'] = 'data-rcmbrain-dev'
    SECRET_KEY = '*%w$sm=)0t3(alyu&0kyepm1wj-$ziinwk@5s16c#ytcn+wq_-'
    # ocr urls
    OCR_UPLOAD_URL = 'https://dev-ocrengine.iinerds.com/img_to_json/'
    NLP_URL = 'https://dev-nlpengine.rcmbrain.com/git_status'
    S3_BUCKET_NAME = 'rcmb-ai-data-dev'
    # Image upload bucket. This has been added here because of the code merge of image deskew
    S3_BUCKET_NAME_IMG = 'data-rcmbrain-dev'
    DOC_PRED_TRAINING_FLASK_URL = 'http://dev-rcmbai-training.iinerds.com'
    MASTER_DB_URI = 'mongodb://admin:T3QXVUbK4VHh@60.10.20.190:27017/rcmb_local?authSource=admin&authMechanism=SCRAM-SHA-1'
    MASTER_COLLECTION = 'rcmb_local'

elif os.environ['NODE_ENV'] == 'qa':

    os.environ["DML_URL"] = "https://qa-datamgmt.iinerds.com/"
    os.environ["JOB_MANAGER_USER"] = "sreejith.sp@iinerds.com"
    os.environ["JOB_MANAGER_PASSWORD"] = "1234567"
    os.environ["IAM_URL"] = "https://qa-iam.iinerds.com/reLogin"

    os.environ['App_Env'] = 'qa'
    os.environ['S3_AWS_KEY'] = 'AKIAJILHNXF6NS77TMRA'
    os.environ['S3_AWS_SECRET'] = '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'
    os.environ['S3_BUCKET_NAME'] = 'data-rcmbrain-qa'
    SECRET_KEY = '*%w$sm=)0t3(alyu&0kyepm1wj-$ziinwk@5s16c#ytcn+wq_-'
    # ocr urls
    OCR_UPLOAD_URL = 'https://qa-ocrengine.iinerds.com/img_to_json/'
    NLP_URL = 'https://qa-nlpengine.iinerds.com/git_status'
    S3_BUCKET_NAME = 'rcmb-ai-data-qa'
    # Image upload bucket. This has been added here because of the code merge of image deskew
    S3_BUCKET_NAME_IMG = 'data-rcmbrain-qa'
    DOC_PRED_TRAINING_FLASK_URL = 'http://qa-rcmbai-training.iinerds.com'
    MASTER_DB_URI = 'mongodb://mongo-admin:4jz24meWykJ4NZ2N@60.20.0.208:38109,60.20.40.149:38109,60.20.0.223:38109/rcmb_qa?replicaSet=rcmbrain&authSource=admin'
    MASTER_COLLECTION = 'rcmb_local'

elif os.environ['NODE_ENV'] == 'staging':

    os.environ["DML_URL"] = "https://rcmbstaging-datamgmt.iinerds.com/"
    os.environ["JOB_MANAGER_USER"] = "sreejith.sp@iinerds.com"
    os.environ["JOB_MANAGER_PASSWORD"] = "123456"
    os.environ["IAM_URL"] = "https://staging-iam.iinerds.com/reLogin"

    os.environ['App_Env'] = 'staging'
    os.environ['S3_AWS_KEY'] = 'AKIAJILHNXF6NS77TMRA'
    os.environ['S3_AWS_SECRET'] = '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'
    os.environ['S3_BUCKET_NAME'] = 'data-rcmbrain-staging'
    SECRET_KEY = '*%w$sm=)0t3(alyu&0kyepm1wj-$ziinwk@5s16c#ytcn+wq_-'

    OCR_UPLOAD_URL = 'https://rcmbstaging-ocrengine.iinerds.com/img_to_json/'
    NLP_URL = 'https://rcmbstaging-nlpengine.iinerds.com/git_status'
    S3_BUCKET_NAME = 'rcmb-ai-data-staging'
    # Image upload bucket. This has been added here because of the code merge of image deskew
    S3_BUCKET_NAME_IMG = 'data-rcmbrain-staging'
    DOC_PRED_TRAINING_FLASK_URL = 'http://staging-rcmbai-training.iinerds.com'
    MASTER_DB_URI = 'mongodb://mongo-admin:4jz24meWykJ4NZ2N@60.30.20.204:48536,60.30.10.130:48536,60.30.10.154:48536/rcmbrain_staging?replicaSet=rcmbrain&authSource=admin'
    MASTER_COLLECTION = 'rcmbrain_staging'

elif os.environ['NODE_ENV'] == 'prod':

    os.environ["DML_URL"] = "https://datamgmt.rcmbrain.com/"
    os.environ["JOB_MANAGER_USER"] = "rcmbrain@gmail.com"
    os.environ["JOB_MANAGER_PASSWORD"] = "rcmbrain@123"
    os.environ["IAM_URL"] = "https://iam.rcmbrain.com/reLogin"

    os.environ['App_Env'] = 'prod'
    os.environ['S3_AWS_KEY'] = 'AKIAJILHNXF6NS77TMRA'
    os.environ['S3_AWS_SECRET'] = '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'
    os.environ['S3_BUCKET_NAME'] = 'data-rcmbrain-prod'
    # ocr urls
    OCR_UPLOAD_URL = 'https://ocrengine.rcmbrain.com/img_to_json/'
    NLP_URL = 'https://nlpengine.rcmbrain.com/git_status'
    S3_BUCKET_NAME = 'rcmb-ai-data-prod'
    # Image upload bucket. This has been added here because of the code merge of image deskew
    S3_BUCKET_NAME_IMG = 'data-rcmbrain-prod'
    DOC_PRED_TRAINING_FLASK_URL = 'http://prod-rcmbai-training.iinerds.com'
    MASTER_DB_URI = 'mongodb://mongo-admin:hzO7qFI0d0TivIGo@60.40.20.112:59658,60.40.20.149:59658,60.40.10.70:59658/admin?replicaSet=rcmbrain&authSource=admin'
    MASTER_COLLECTION = 'rcmb_prod'


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_URL = '/static/'

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
from kombu import Exchange, Queue
print("=++++++++++ Celery Config in Settings=========================")
# worker_prefetch_multiplier  = 1
# worker_max_tasks_per_child = 15
# worker_lost_wait = 30
# task_acks_late = True
CELERYD_PREFETCH_MULTIPLIER = 6
CELERYD_POOL_RESTARTS = True
CELERY_ACKS_LATE = True
BROKER_HEARTBEAT=60.0
#BROKER_POOL_LIMIT = 0
# celery queues setup
CELERY_TASK_DEFAULT_QUEUE = 'default'
CELERY_DEFAULT_EXCHANGE_TYPE = 'topic'
CELERY_DEFAULT_ROUTING_KEY = 'default'
CELERY_QUEUES = (
    Queue('low_priority', Exchange('low_priority'), routing_key='low_priority'),
    Queue('medium_priority', Exchange('medium_priority'),
          routing_key='medium_priority'),
    Queue('high_priority', Exchange('high_priority'),
          routing_key='high_priority'),
    Queue('agent_sequential', Exchange('agent_sequential'),
          routing_key='agent_sequential'),
    Queue('validator_worker', Exchange('validator_worker'),
          routing_key='validator_worker'),
    Queue('database_insert', Exchange('database_insert'),
          routing_key='database_insert'),
    Queue('nlp_pdf_queue', Exchange('nlp_pdf_queue'),
          routing_key='nlp_pdf_queue'),
    Queue('rules_queue', Exchange('rules_queue'), routing_key='rules_queue'),
    Queue('ocr_queue', Exchange('ocr_queue'), routing_key='ocr_queue'),
    Queue('ocr_img_queue', Exchange('ocr_img_queue'),
          routing_key='ocr_img_queue'),
    Queue('ocr_res_queue', Exchange('ocr_res_queue'),
          routing_key='ocr_res_queue'),
    Queue('system_mapping_ocr_queue', Exchange('system_mapping_ocr_queue'),
          routing_key='system_mapping_ocr_queue'),
)
# celery route setup
CELERY_TASK_ROUTES = {
    'job_man.tasks.nlp_pdf_to_json': {
        'queue': 'nlp_pdf_queue',
        'routing_key': 'nlp_pdf_queue'
    },
    'job_man.tasks.split_and_process_data': {
        'queue': 'rules_queue',
        'routing_key': 'rules_queue'
    },
    'job_man.tasks.nlp_ocr_to_json': {
        'queue': 'nlp_ocr_queue',
        'routing_key': 'nlp_ocr_queue'
    },
    'job_man.tasks.ocr_img_to_json': {
        'queue': 'ocr_queue',
        'routing_key': 'ocr_queue'
    },
    'job_man.tasks.get_ocr_response': {
        'queue': 'ocr_res_queue',
        'routing_key': 'ocr_res_queue'
    },
    'job_man.tasks.ocr_img_to_json_system_mapping': {
        'queue': 'system_mapping_ocr_queue',
        'routing_key': 'system_mapping_ocr_queue'
    }
}
from common.initializers import set_environment_vars_from_kms
import os
if os.environ.get("INIT_OVER") != "true":
    set_environment_vars_from_kms()
CELERY_BROKER_URL = 'pyamqp://' + os.environ['RABBITMQ_USERNAME'] + ':' + \
    os.environ['RABBITMQ_PASSWORD'] + '@' + \
    os.environ['RABBITMQ_HOSTNAME'] + '//'
# CELERY_MONGODB_SCHEDULER_DB         = os.environ['MONGODB_SCHEDULER_DB']
# CELERY_MONGODB_SCHEDULER_COLLECTION = os.environ['MONGODB_SCHEDULER_COLLECTION']
# CELERY_MONGODB_SCHEDULER_URL        = os.environ['RCMB_DB_URI']
