#/bin/bash
crontab -l | sed '/\/bin\/bash/s!^!#!' | crontab -
cd /root/ocr_engine/ocr_engine_service
mkdir /root/ocr_engine/ocr_engine_service/crop_files/
sh EAST_script.sh
#mount -t tmpfs -o rw,size=3G tmpfs /root/ocr_engine/ocr_engine_service/crop_files/
echo "Installing required packages via pip"
pip3 install -r /root/ocr_engine/ocr_engine_service/requirement.txt
crontab -l | sed "/^#./s/^#//" | crontab -