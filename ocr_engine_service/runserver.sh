#/bin/bash
pkill tail
echo "Stopping uwsgi"
kill -9 $(ps aux | grep uwsgi | grep -v grep | awk '{print $2}' | tr '\n' ' ') > /dev/null 2>&1
kill -9 $(ps aux | grep python | grep -v grep | awk '{print $2}' | tr '\n' ' ') > /dev/null 2>&1
kill -9 $(ps aux | grep celery | grep -v grep | awk '{print $2}' | tr '\n' ' ') > /dev/null 2>&1
cd /root/ocr_engine/ocr_engine_service
pip3 install -r requirement.txt
sh initial_setup_prod.sh
echo "Starting uwsgi"
pkill tail
if [ "$DEPLOYMENT_GROUP_NAME" = "Dev" ]; then
export NODE_ENV=dev
nohup sh run_server.sh > log1.txt 2>&1 &
elif [ "$DEPLOYMENT_GROUP_NAME" = "Staging" ]; then
export NODE_ENV=staging
nohup sh run_server.sh > log1.txt 2>&1 &
elif [ "$DEPLOYMENT_GROUP_NAME" = "Qa" ]; then
export NODE_ENV=qa
nohup sh run_server.sh > log1.txt 2>&1 &
elif [ "$DEPLOYMENT_GROUP_NAME" = "Prod" ]; then
export NODE_ENV=prod
nohup sh run_server.sh > log1.txt 2>&1 &
else
export NODE_ENV=dev2
nohup sh run_server.sh > log1.txt 2>&1 &
fi
sh logs.sh > /dev/null 2> /dev/null < /dev/null &
