import os
import sys
import time
import json
import boto3
import random

from django.conf import settings
from common.variables import (
    S3_AWS_KEY, S3_AWS_SECRET, model_path, model_json_path)

print('S3_Bucket_Name: ', os.environ["S3_BUCKET_NAME"])


def s3_download_model(s3_path, bucket_name=os.environ["S3_BUCKET_NAME"]):
    # local_file_path = os.path.join('data', s3_model_path)
    check_dir_exist(os.path.dirname(s3_path))
    s3 = get_s3_client()
    s3.download_file(bucket_name, s3_path, s3_path)
    return s3_path


def get_s3_client(aws_access_key_id=S3_AWS_KEY, aws_secret_access_key=S3_AWS_SECRET):
    return boto3.client('s3', aws_access_key_id=aws_access_key_id,
                        aws_secret_access_key=aws_secret_access_key)


def check_dir_exist(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def s3_download_file(s3_path, bucket_name, file_save_path):
    check_dir_exist(os.path.dirname(file_save_path))
    s3 = get_s3_client()
    print("File save Location   " + os.path.join(os.getcwd(), file_save_path))
    s3.download_file(bucket_name, s3_path, os.path.join(
        os.getcwd(), file_save_path))
    return file_save_path


def get_temp_fpath(data):
    # print('DATA**', os.path.basename(data))
    rand_name = "%0.12d" % random.randint(0, 999999999999)
    extn = os.path.splitext(str(data))[1]
    return os.path.join('temp_files', os.path.basename(data))
    # return os.path.join('model_files', rand_name + extn)


def fpath_from_s3(s3_url, fpath=None, common_bucket=None):
    # if not fpath:
    #     fpath = get_temp_fpath(s3_url)
    # bucket_name = get_bucket_name(common_bucket)
    # # print(bucket_name)
    # return s3_download_file(s3_url, bucket_name, fpath)
    loc_path = None
    ctr = S3_RETRY_LIMIT

    while (ctr):
        try:
            if not fpath:
                 fpath = get_temp_fpath(s3_url)
            if common_bucket == None:
                bucket_name = get_bucket_name(common_bucket)
            else:
                bucket_name = common_bucket
            print(bucket_name)
            loc_path = s3_download_file(s3_url, bucket_name, fpath)
            break
        except Exception as e:
            ctr -= 1
            sleep_time = random.randint(1, 10)
            print('S3 DOWNLOAD FAILED, RETRYING IN ', sleep_time, ' seconds...')

            time.sleep(sleep_time)
    return loc_path

def download_img_fpath_from_s3(s3_url, fpath=None, common_bucket=None):
    if not fpath:
        fpath = get_temp_fpath(s3_url)
    bucket_name = os.environ["S3_BUCKET_NAME_IMG"]
    print('Bucket_Name: ', bucket_name)
    return s3_download_file(s3_url, bucket_name, fpath)
def download_img_fpath_from_s3_system_mapping(s3_url, fpath=None, common_bucket=None):
    print("*********    ",common_bucket)
    if not fpath:
        fpath = get_temp_fpath(s3_url)
    return s3_download_file(s3_url, common_bucket, fpath)

def run():
    model_folder=os.path.join(os.getcwd(),'ai_data/ocr_model_files/')
    if not os.path.isdir(model_folder):

        print('***Running page rotation initial training***')
        model1 = fpath_from_s3(
            model_path, model_path, os.environ["S3_BUCKET_NAME"])
        model2 = fpath_from_s3(
            model_json_path, model_json_path, os.environ["S3_BUCKET_NAME"])


def get_bucket_name(common_bucket):
    from common.rcm_connections import RcmbDbConnection
    oRcmbDbConnection = RcmbDbConnection()
    oRcmbDbConnection.set_params_from_kms()
    if common_bucket:
        return oRcmbDbConnection.S3_BUCKET_COMMON
    return oRcmbDbConnection.S3_BUCKET_NAME
