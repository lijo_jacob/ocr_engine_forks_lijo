'''
Profiling code to know  time consumption per file
img_p should contain path to image
Method to do profiling:
pip install pyinstrument
python -m pyinstrument code_profiling.py

'''

from argparse import ArgumentParser
import os

parser = ArgumentParser()
parser.add_argument("--file", dest="filename",
                    help="input img file")
parser.add_argument("--env", dest="environment",
                    help="environment the code should run in")
# set_environment_vars_from_kms()
args = parser.parse_args()
file_p = args.filename
env_var = args.environment
os.environ['NODE_ENV'] = env_var
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
from common.initializers import set_environment_vars_from_kms
import os

if os.environ.get("INIT_OVER") != "true":
    set_environment_vars_from_kms()

from src.ocr_extraction.utilities.ocr_multi_proc import getOCRText

res = getOCRText(file_p,post_processing_flag=True, method_flag='combined_method')
