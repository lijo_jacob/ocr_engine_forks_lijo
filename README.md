# RCMB OCR extraction Service -Latest

## Setup to run locally
1. Setup and activate virtual environment
2. Install required dependencies `pip install -r ocr_engine_service/requirements.txt`
3. Run the initial setup shell script `chmod +x ocr_engine_service/initial_setup_dev.sh` and `sudo sh ocr_engine_service/initial_setup_dev.sh`
4. Run the server `python ocr_engine_service/manage.py runserver`


#Run code profiling
pip install pyinstrument (not included in requirements.txt)
python -m pyinstrument code_profiling.py --file <filepath>
